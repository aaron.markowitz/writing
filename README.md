# Writing

[![pipeline status](https://git.ligo.org/aaron.markowitz/writing/badges/main/pipeline.svg)](https://git.ligo.org/aaron.markowitz/writing/commits/main)

Thesis, talks, etc


## Thesis:
[Markowitz_Aaron_2023.pdf](https://git.ligo.org/aaron.markowitz/writing/-/jobs/artifacts/main/file/Thesis/Markowitz_Aaron_2023.pdf?job=thesis)

## CV
[main.pdf](https://git.ligo.org/aaron.markowitz/writing/-/jobs/artifacts/main/file/Applications/cv/main.pdf?job=cv)
