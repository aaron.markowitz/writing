
%\begin{refsection}

  \chapter{Silicon Fabrication} \label{SiFab}
  
  High quality mechanical oscillators are at the heart of the optomechanics
  experiments described in this work. Silicon is a promising material for mirror substrates due
  to its extremely low mechanical loss near $123$ K and low optical absorption
  for short infrared light. This chapter introduces the much larger body of literature on silicon
  fabrication, and describes a few small experiments undertaken to improve our
  group's application of old techniques.

  \section{Surface Cleaning and Passivation}

  For the Q measurement apparatus described in Chapter \ref{cryoQ}, we require 2'' or
  3'' substrate wafers with extremely low mechanical loss. Imperfections in the
  Si crystal structure due to surface defects can contribute mechanical loss, as
  can the presence of contaminents on the Si surface. Surface contaminants can
  also affect the properties of thin films deposited on the Si wafers, which may
  be relevant for the film's optical or mechanical properties.

  \subsection{RCA Clean}\label{RCA}

  To prepare a silicon wafer for further processing, we first remove any surface
  contaminants with an ``RCA clean,'' a standard cleaning procedure first
  developed by Radio Corporation of America \cite{waferClean}. Our procedure is as follows:

  \begin{enumerate}
    \item Bath in solvent to remove oils and organic materials. We use a room
      temperature bath of acetone for 3 minutes, followed by methanol for 3
      minutes. After soaking, rinse the wafer in running DI water for 3 minutes.
    \item Bath in a 1:5:1 solution of ammonium hydroxide, water, and hydrogen
      peroxide at $70\pm 5^\circ$ C for 15 minutes. The basic solution
      removes remaining organic residues and oxidizes the silicon surface. After
      soaking, immerse the solution under DI water for several water changes and
      remove the wafer from the solution while running water to avoid depositing
      contaminants on the wafer as it passes through the unbroken water surface.
      The wafer should be hydrophilic, indicating the presence of a silicon
      dioxide layer. 
    \item Bath in a 1:5:1 solution of hydrochloric acid, water, and hydrogen
      peroxide at $70 \pm 5^\circ$ C for 15 minutes. The acid solution removes
      metallic contaminants and some ammonium-insoluble hydroxides. Rinse the
      wafer under running DI water as in the previous step, and again verify
      that the surface is hydrophilic.
    \item Dry the wafer with dry filtered nitrogen gas.
  \end{enumerate}

  \subsection{HF Etch}
  
  Bare silicon quickly develops a silicon dioxide surface
  layer by reacting with water in the air or bath. The $\mathrm{SiO}_2$ layer introduces
  excess mechanical loss.

  To remove the oxide passivation layer following RCA
  clean, we etch the wafer in a dilute solution of buffered hydrofluoric acid (0.75$\%$)
  for 2 minutes. The HF etch removes the oxide and etches the Si crystal along
  highly specular surfaces.

  \subsection{Plasma-Enhanced Chemical Vapor Deposition}

  Immediately after HF etch and before a thermal oxide layer forms on the Si
  surface, we deposit a passivating layer of silicon nitride using
  plasma-enhanced chemical vapor deposition (PECVD). The SiNx layer prevents the
  silicon from reacting with atmospheric water, and has much lower mechanical
  loss than $\mathrm{SiO}_2$.

  \section{Cantilever Fabrication}

  \subsection{Rectangular Cantilevers}

  The simplest method of reducing Si wafers to pieces with the desired
  mechanical resonances is by scribing and breaking the $\langle 100 \rangle$
  wafer along its crystal lattice. We use a Dynatek GST-150 Scriber-Breaker to
  produce rectanuglar wafers with several mm to several cm lengths with good reproducability.

  One drawback of simple rectangular wafers is their high mechanical loss due to
  significant coupling to the clamp that holds one end of the cantilever. Also,
  bonding the cantilever directly to an optic will introduce an additional uncoated Si
  surface that scatters light from the beam transmitted through the
  optic and cantilever.

  The cantilevers utilized in Part \ref{Preamp} were fabricated by Korth
  \cite{KorthThesis} using a process developed by the Chao group at National
  Tsing Hua University \cite{Chao_fab}. The process uses optical lithography
  followed by deep reactive ion etching to form a thick SiNx mask on both sides
  of the cantilever. The mask selectively protects the silicon during a KOH etch
  process such that the resulting cantilever is thick at either end (where the
  clamp or optic touch the cantilever) but thin in the middle. We also etch a
  through-hole that sits behind the optic to allow beam clearance.

  The resulting cantilevers have mechanical losses around $2.6\times 10^{-6}$ at
  100 K, consistent with clamping and surface quality-related losses
  \cite{KorthThesis}. The
  thinned central region of the cantilever contains most of the strain-energy
  associated with low frequency eigenmodes, thus diluting clamping losses. 
  
  \section{Potassium Hydroxide Etch Characterization}

  Surface roughness can create slip-stick losses at clamping interfaces, as well
  as increase the area available for relatively lossy in SiNx or thermal oxide
  surface layers. A wet KOH etch process sets the surface roughness of our Si
  cantilevers. The literature on Si wet etching
  suggests higher KOH molarity reduces both surface roughness and etch rate,
  while elevated bath temperature increases etch rate but reduces surface
  roughness \cite{KOHroughness}. 

  We used atomic force microscopy (AFM) to characterize the
  evolution of surface
  roughness of B-doped $\langle  100 \rangle$ Si samples during a wet etch with
  50\% w/v (13.4 M) KOH held at 50 C and continually agitated. The duration of
  KOH etch varied by sample.

  Samples were prepared by breaking a 500 $\mu$m thick Si wafer into 10 mm
  $\times$ 20 mm rectangles with a Dynatek scriber-breaker. Each sample was
  RCA cleaned with the recipe in Sec \ref{RCA}. Samples were etched for 2 min
  in 2.5\% buffered HF, then rinsed for 2 min in running DI water, immediately
  prior to KOH etching. Following KOH etching, samples were washed in DI water
  for another 2 minutes. 

  We used a Bruker Dimension Icon AFM in PeakForce Tapping mode to measure
  surface height on a $256\times 256$ nm square grid with 512 points in each
  direction. We used ScanAsyst to automatically adjust probe feedback
  gain and setpoint, but we manually set z-range to a relatively low 1.27 $\mu$m
  to improve vertical resolution over our featureless scan regions.

  A control sample that was only
  RCA cleaned had mean roughness $R_a=0.439$ nm and RMS roughness $R_q=0.546$ nm,
  indicating HF etch alone improved surface roughness by 40-60\%. Fig.
  \ref{fig:roughness} shows that mean surface
  roughness increased then stabilized to $1.6$nm for long duration etches. RMS
  roughness continued increasing even for long duration etches, though this may
  have been due to pitting visible in the photos of Fig. \ref{fig:AFM_photos} and
  could be mitigated by increasing KOH bath temperature. The etch rate for the
  first 60 minutes of etching was 0.42 $\mu$m/min, while the average rate
  between 60 and 382 minutes of etching was 0.32 $\mu$m/min. The decrease in
  etch rate over time is consistent with the observation of \cite{KOHroughness} that
  increasing roughness exposes more slow-etching $\langle 111 \rangle$ planes of
  the Si crystal. 

  \begin{figure}[h!]
    \centering
    \includegraphics[width=\textwidth]{Figures/KOH_roughness.pdf}
    \caption[KOH Etched Surface Roughness]{Average and RMS surface roughness of
      Si wafers after KOH wet etch followed by water rinse, measured with AFM.
      Roughness at 0 min was measured on samples that were not KOH
      etched.}
    \label{fig:roughness}
    \index{figures}
  \end{figure}

  \begin{figure}[h!]
    \centering
    \includegraphics[width=\textwidth]{Figures/AFM_photos.png}
    \caption[AFM Optical Images]{The stills captured from the AFM's video
      microscope are ordered from top to bottom by increasing KOH etch time,
      starting with RCA-cleaned samples with no HF or KOH etching. The left-most
      column are taken near the location of the AFM scan quoted in the
      roughness measurements. The other columns show pitting or contamination
      elsewhere on the sample.}
    \label{fig:AFM_photos}
    \index{figures}
  \end{figure}

  \section{Process Refinement}

  We recommend that future Si cantilever fabrication based on \cite{KorthThesis} use 50\% w/v KOH but return
  to an 80 C bath temperature. It would be useful to repeat surface roughness
  measurements following post-etch HF rinse, water wash, and SiNX deposition, to
  identify to what extent these post-etching processes impact the final surface
  roughness. Future measurements would also benefit from decreasing AFM scan
  resolution and increasing scanned area to capture the $\mu$m-sized features
  visible in Fig. \ref{fig:AFM_photos} especially for long-duration etches.

  Note that in this study, we decreased the bath temperature relative to that used by \cite{KorthThesis} in order to slow the etch
  rate and more precisely control etch depth. The rate implied by measurements in \cite{KorthThesis}
  underestimated the rate observed over short-duration etches on thinner ($<300$
  $\mu$m) wafers. To precisely tune the ultimate thickness (and resonant
  frequency) of our cantilevers, it would be useful to repeat etch rate measurements over short
  intervals up to 2 hours.

  Alternatively, one could try to avoid entirely the surface roughness
  associated with KOH etching. Rather than thinning the cantilever midsection,
  one could instead thicken the clamped region by optically contacting Si
  rectangles on either side of the cantilever underneath the steel clamp.
  Further study is needed to determine the clamping and interface losses of
  optically contacted Si, but we would expect reduced surface losses along the
  length of the cantilever. Furthermore, alternative anisotropic etching
  techniques like deep reactive ion etching could be explored for creating
  vertical etches through the entire Si wafer for laser beam transmission. 
 
%  \printbibliography[heading=subbibliography]

%\end{refsection}