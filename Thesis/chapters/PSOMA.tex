
%\begin{refsection}

  \chapter{Phase-Sensitive Optomechanical Amplifier}\label{PSOMA}

 
  Many figures and derivations in this chapter can be found in our proposal for a
  phase-sensitive optomechanical amplifier (PSOMA) to realize
  single quadrature optical preamplification in an entirely optomechanical
  system \cite{PSOMA}. The proposed PSOMA can achieve sufficiently low noise, high gain, and
  high isolation to benefit next generation GW IFOs limited by optical readout losses. 

  \section{Phase-Sensitive Amplification}

  The nonlinearity of optomechanical radiation pressure interactions can be
  utilized in many photonic signal processing applications
  \cite{optomech_phononPhoton}.

  The fundamental noises for optomechanical sensors like gravitational
  wave interferometers can be thermal and quantization noises of the mechanical or optical
  degrees of freedom \cite{Whittle_unifyingNoises}. One challenge for next
  generation gravitational wave interferometers will be quantum noise
  due to the low quantum efficiency of existing photodetection technologies at
  wavelengths beyond 1064 nm \cite{Voyager}. Photodetection optical losses are
  generally important for optical sensors utilizing highly squeezed optical
  states.

  Caves \cite{PSOMA_CavesOriginal} and more recently Knyazev \emph{et al.}
  \cite{Knyazev_absorption} \cite{Knyazev_tomography} proposed overcoming photodetection
  losses by pre-amplifying the signal-carrying optical quadrature, such that
  unsqueezed vacuum fields introduced by downstream optical losses do not limit
  the sensor's input-referred noise performance. The general principles of
  quantum noise in linear amplifiers are described in \cite{Caves_linearAmps}. 

  

  \section{Traveling Wave Cavity}

  PSOMA (the ``Amplifier'' in Figure \ref{fig:PSOMAvoy}) uses a traveling wave cavity to enhance the radiation pressure coupling
  between a pumping laser and a mechanical oscillator. The circulating power
  (and optomechanical gain)
  in the cavity is enhanced by the cavity finesse, with a tradeoff in the
  amplifier's bandwidth due to a finite cavity pole. Because the radiation
  pressure force falls off like $1/f^2$, ponderomotive amplification is most
  effective at low frequencies and we can choose an appropriate cavity finesse
  and length to optimize for high optomechanical gain over the measurement band.

  We choose a traveling wave cavity, rather than a Fabry-Perot cavity, to ensure
  that the reverse propagating vacuum field from the photodetection port is not
  amplified on its way to the low noise measurement device. Only
  the optical field copropagating and in-phase with the pump drives the
  mechanical oscillator with nonnegligible coupling.

  

  \section{Mach-Zehnder Interferometery}

  With only a single traveling wave cavity, PSOMA's input referred noise can be
  limited by relative intensity noise (RIN) of the pump laser. Even if pump RIN
  is prestabilized to the quantum shot noise level, this noise would limit the
  utility of PSOMA for sub-SQL measurement devices. To mitigate the contribution
  from pump RIN, PSOMA uses an interferometric technique analogous to the long-tailed
  pair in electronic differential amplifiers \cite{LongTailedPair}.

  The pump and probe (signal-carrying) beams enter PSOMA at
  the input beamsplitter of a Mach-Zehnder interferometer (MZI). Each arm of the
  MZI contains a nominally identical traveling wave cavity, and the cavity
  reflections are recombined at the MZI's output beamsplitter. Due to the $\pi$
  phase shift experienced on reflection from one side of the input beamsplitter,
  one cavity is driven by the sum of pump and probe fields, while the other is
  driven by the difference of pump and probe fields. With an appropriate choice
  of MZI detuning, the output BS of the MZI can separate the symmetric and
  antisymmetric contributions such that one MZI output port contains only the pump
  and amplified pump noise, while the other MZI output port contains only the
  probe and amplified probe signals.

  In this way, the MZI is the 2-port version
  of the Michelson interferometer (MI). The ideal MI operated on the dark fringe
  perfectly reflects all fields incident on its antisymmetric (dark) port. The
  ideal MZI operated on the dark fringe perfectly transmits all fields incident
  on its antisymmetric ports.

  With identical traveling wave cavities in each arm of the MZI, pump RIN is
  rejected out the common mode port of PSOMA. On the other hand, the optical
  field entering the probe port is differentially amplified by the cavities and
  exits out the differential (antisymmetric, probe, dark) port of PSOMA.
  This allows the SNR of signals much smaller than pump RIN to be preserved, up
  to limits imposed by the contrast defect of the MZI or gain imbalance of the
  arm cavities.
  
  Mach-Zehnder interferometers are further described in \cite{MZ_limits_vacIn}
  and elsewhere.
  %TODO: cite some SU11 IFOs and other


  \subsection{Input-Output Relations}

  The derivation of the input-output relations of PSOMA is in \cite{PSOMA}.
  
  Let $R_A, T_A$ denote the power reflectivity and transmissivity, respectively,
  of the M1L and M1R mirrors, and $L_A$ denotes the round-trip length of each
  ring. In the limit of a high finesse cavity with low frequency signals
  compared to the cavity length, $\Omega L_A/c<<1$ and $T_A<<1$, giving
  
  \begin{equation}\label{eq:PSOMA_IO}
    \begin{pmatrix} b_{out,1} \\ b_{out,2} \end{pmatrix} =
    e^{2i\eta}\begin{pmatrix} 1 & 0 \\ -\mathcal{K}_A & 1 \end{pmatrix}
    \begin{pmatrix} b_{in,1} \\ b_{in,2} \end{pmatrix}
    + \sqrt{\frac{32 \omega_0 P_\mathrm{circ}}{\hbar c^2}} \frac{1}{T_A}
    \begin{pmatrix} 0 \\ 1 \end{pmatrix} \xi
  \end{equation}

  where

  \begin{equation}
    \begin{split}
      \mathcal{K}_A & =\frac{4}{T_A[1+(\Omega/\gamma_A)^2]}\kappa_A \\
      \kappa_A & =-\frac{18\omega_0P_\mathrm{circ}}{c^2}\chi_A \\
      \eta & =\arctan(\Omega/\gamma_A) \\
      \gamma_A & =\frac{c T_A}{2L_A}
    \end{split}
  \end{equation}

  with $\gamma_A$ the cavity pole frequency, $P_\mathrm{circ}$ the power
  circulating in each ring, $\chi_A$ the mechanical susceptibility of the
  movable mirrors, and $c$ the speed of light. Noise may couple in to PSOMA
  through $\xi$, the motion of mirrors in the ring cavity not due to quantum
  radiation-pressure noise (for example seismic or thermal noise). 

  \section{For LIGO Voyager}

  \subsection{Sensitivity Improvement}
  
  In \cite{PSOMA}, we proposed using PSOMA to preamplify optical signals
  exiting a next-generation GW IFO before they encounter optically lossy
  elements like photodiodes. The proposed configuration is in Fig.
  \ref{fig:PSOMAvoy}. 

  \begin{figure}[h!]
    \centering
    \includegraphics[width=0.5\textwidth]{Figures/PSOMA_voy_layout.pdf}
    \caption[GWIFO With PSOMA]{Optical configuration of PSOMA as applied to a LIGO-like
      gravitational wave interferometer.}
    \label{fig:PSOMAvoy}
    \index{figures}
  \end{figure}

  The amplifier has a modest benefit in the observatory's most sensitive band
  between 40-300 Hz under assumptions consistent with current optics
  technology. However, the improvement is more significant for
  configurations using higher levels of squeezing. More than ~15 dB of injected
  squeezing does not improve sensitivity under the assumption of high
  photodetection losses without preamplification, and some kind of low-loss preamplification like PSOMA
  becomes necessary to take advantage of higher levels of squeezing. We show the
  noise improvement for the most likely and optimistic parameter choices in Figures
  \ref{fig:VoyImprovement15} and \ref{fig:VoyImprovement20}. The expected
  noise budget for the pragmatic configuration is in \ref{fig:VoyBudget15}.

  \begin{figure}[h!]
    \centering
    \includegraphics[width=0.5\textwidth]{Figures/VoyBudget15.pdf}
    \caption[Voyager with PSOMA Noise Budget]{Noise budget for PSOMA applied to LIGO Voyager under a pragmatic
      choice of parameters and technical noises.}
    \label{fig:VoyBudget15}
    \index{figures}
  \end{figure}
  

  \begin{figure}[h!]
    \centering
    \includegraphics[width=0.5\textwidth]{Figures/VoyImprovement15.pdf}
    \caption[Voyager with PSOMA Sensitivity Improvement]{Strain noise referred sensitivity improvement for PSOMA applied to
      LIGO Voyager under a pragmatic choice of parameters and technical noises,
      including 15 dB of frequency dependent squeezing and 30 g amplifier mirrors.}
    \label{fig:VoyImprovement15}
    \index{figures}
  \end{figure}

  \begin{figure}[h!]
    \centering
    \includegraphics[width=0.5\textwidth]{Figures/VoyImprovement20.pdf}
    \caption[Voyager with PSOMA Optimistic Sensitivity Improvement]{Strain noise referred sensitivity improvement for PSOMA applied to
      LIGO Voyager under an optimistic choice of parameters and technical
      noises, including 20 dB of frequency dependent squeezing and 10 g
      amplifier mirrors. }
    \label{fig:VoyImprovement20}
    \index{figures}
  \end{figure}

  \subsection{Amplifier Gain}

  The ponderomotive force, and therefore also PSOMA's gain, falls off like
  $1/f^2$. However, because the optimal homodyne readout quadrature varies with
  frequency, we do not always sense the maximally amplified quadrature.
  Therefore, the effective signal amplification also varies with frequency, as
  shown in Fig. \ref{fig:VoyGain}.

  \begin{figure}[h!]
    \centering
    \includegraphics[width=0.5\textwidth]{Figures/VoyGain.pdf}
    \caption[PSOMA Projected Gain]{Amplifier gain with and without output filter cavity implementing
      frequency-dependent homodyne detection.}
    \label{fig:VoyGain}
    \index{figures}
  \end{figure}

%  \subsection{Mass Scaling}
%
%  TODO: include mass scaling
  
%  \printbibliography[heading=subbibliography]

%\end{refsection}