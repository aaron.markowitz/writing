
%\begin{refsection}

  \chapter{Cryo Q Experiment} \label{cryoQ}

  Due to the fluctuation dissipation theorem, mechanical loss is a reasonable
  way of estimating the expected Brownian thermal noise contribution of a
  particular material or optomechanical element. Brownian thermal noise is one of
  the limiting noises of many cavity-stabilized frequency measurements \cite{noise_thermal_frequencyStabilization}.

  For next generation gravitational wave interferometers, we expect to use
  cryogenic silicon optomechanics cooled to 123 K to allow for longer carrier
  wavelength (2 um interferometry) with low absorption, and lower thermal noise due to
  both reduced thermal bath temperature and coupling of acoustic phonons to that bath \cite{TN_Levin}. We
  also need to enhance the natural emissivity of Si \cite{Si_emissivity_review} \cite{Si_emissivity}
  to radiatively cool the test masses.
  
  This series of experiments aimed to develop a testbed for measuring the
  temperature-dependent mechanical loss of thin films such as a-Si (which exhibits incredibly low
  mechanical loss for reasons discussed in \cite{Q_aSiThesis}) to explore the
  parameter space of materials and deposition processes
  \cite{IBS_Ta2O5_stress_optical} \cite{anneal_aSi} for optical coatings in
  cryogenic GW observatories. We also want to characterize the thermal noise
  contribution of high emissivity coatings that could be applied to the barrel
  of GWIFO test masses to increase their radiative coupling for temperature control.

  Many experiments have measured the quality factor of Si, typically by
  observing ringdowns of mechanical eigenmodes of clamped \cite{Q_thinSi} or nodally suspended
  \cite{GeNS} \cite{QThesis} wafers or cantilevers. The ringdown measurements are then be used
  to extract mechanical propertise of thin films deposited on the Si oscillators
  \cite{Q_Ta2O5}, \cite{Q_OHcatalysis}, \cite{Penn:19}, \cite{Q_In},
  \cite{Q_Acktar}, \cite{Q_Y_SiO2_Ta2O5}, \cite{Q_aSi_nanoSi} \cite{Q_blackNanotube}
  \cite{Q_ampDependence}.

  Our considerations of mechanical and thermal design are similar to and
  informed by experiments
  characterizing other properties of Si bonds such as breaking strength and
  thermal conductivity \cite{Q_bondStrength} \cite{stable_bonds}, \cite{Q_OHcatalysisStrength},
  \cite{Q_bondConductivity}, \cite{Q_OHorientation}.

  %\begin{figure}[hbt!]
  %  \centering
  %  % \includegraphics[width=.3\textwidth]{}
  %  \caption[High Emissivity Coating SEM]{High emissivity nanotube coating under
  %    consideration}\label{fig:nanotubePhoto}
  %  \index{figures}
  %\end{figure}
  
  \section{Target Testbed Quality Factor}
  
  Optimal experimental design requires clearly defining the target measurement
  sensitivity from the outset. As described above, we want to measure the
  mechanical loss $\phi_\mathrm{coating}$ of a thin film deposited on a wafer of
  single crystalline silicon. We first develop a detailed physical model of
  mechanical loss $\phi_\mathrm{Si}(\vec{\mu})$ of the silicon wafers in our experiment by measuring the quality
  factor $\{Q_i\}$ and eigenfrequencies $\{f_i\}$ of several mechanical
  eigenmodes of the wafer. The $Q_i$ and $f_i$ are together determined by a set
  of material and experimental parameters $\vec{\mu}$ (in which the parameter
  $i$ indexing mode number could be included, if desired).

  We then measure eigenfrequencies $\{f_j\}$ and quality factors
  $\{Q_j\}$ under some experimental parameters $\vec{\mu}'$ after depositing a thin coating that modifies the mechanical response
  of the oscillator. The contribution of the coating and environment to the $Q_j$ is
  proportional to dilution factors $\{D_j^\mathrm{coating}\}$ and
  $\{D_j^\mathrm{env}\}$, representing the fraction of the
  eigenmode's integrated strain-energy contained in the coating and environment
  (anything other than the Si and coating), respectively. For any
  particular eigenmode, the total mechanical loss of the system is due to the
  weighted sum of the diluted losses in each subsystem:
  
  \begin{equation} \label{eq:dilution}
    \frac{1}{Q_j(\vec{\mu}')} = (1-D_j^\mathrm{coating} - D_j^\mathrm{env})\phi_\mathrm{Si}(\vec{\mu}') +
    D_j^\mathrm{coating}\phi_\mathrm{coating}(\vec{\mu}') + D_j^\mathrm{env}\phi_\mathrm{env}(\vec{\mu}').
  \end{equation}
  
  If we control the experimental parameters to remain nearly the same before and after
  coating deposition, we can estimate that
  
  \begin{equation}
    \begin{split}
      \phi_\mathrm{Si}(\vec{\mu}') \approx \phi_\mathrm{Si}(\vec{\mu}) + \frac{\partial \phi_\mathrm{Si}}{\partial \vec{\mu}}\delta\vec{\mu} \\
      \phi_\mathrm{env}(\vec{\mu}') \approx \phi_\mathrm{env}(\vec{\mu}) + \frac{\partial \phi_\mathrm{env}}{\partial \vec{\mu}}\delta\vec{\mu}.
    \end{split}
  \end{equation}
  
  We use a COMSOL finite element analysis to model the expected dilution factors for a
  given $\vec{\mu}$, which lets us solve for the coating loss in Eq
  \ref{eq:dilution}.

  % TODO: incomplete, flesh out
  % TODO: what goes here vs in tomography chapter?

  \subsection{Statistical Analysis}
  
  Frequentist statistical analysis is useful for posing questions of the type,
  ``What is the probability $P(\vec{x}|\vec{\mu})$ of some experimental outcome $\vec{x}_0$ given an
  assumption that the true fiducial model parameters $\vec{\mu}_0$ lie inside the confidence
  interval $(\vec{\mu}_1, \vec{\mu}_2)$?'' Physics defines the map $P$, while the
  canonical application of Neyman's method \cite{Neyman} due to Feldman and
  Cousins ensures a formally optimal choice of bounds $(\vec{\mu}_1, \vec{\mu}_2)$ that neither under- nor
  over- covers the parameter space given the observed measurement outcomes \cite{FeldmanCousins}.
  
  The parameters $\vec{\mu}$ include physically interesting but
  unconstrained quantities, such as $\phi_\mathrm{coating}$. They may also contain
  a set of nuisance parameters that are less interesting, but nonetheless affect the
  measurement outcome. Some nuisance parameters are selectable or controllable
  experimental parameters, such as the system
  temperature $T$ and wafer geometry characterized by a radius $r$ and thickness
  $t$. Some nuisance parameters may be constrained by other experiments but not
  perfectly known or easily varied, like the Young's modulus $Y$ and
  coefficient of thermal expansion $\alpha$ of crystalline silicon and our thin
  film. Heuristically, we aim to design an experiment such that
  $P(\vec{x}|\vec{\mu})$ is mostly influenced by the parameters we wish to measure
  ($\phi_\mathrm{coating}$), rather than by our uncertainty about the nuisance
  parameters. Formally, we can construct $(\vec{\mu}_1, \vec{\mu}_2)$ for many simulated
  realizations of an experiment drawing from $P(\vec{x}|\vec{\mu})$ under a
  particular choice of nuisance parameters.
  
  % TODO: One could also maximize the Fisher information
  % TODO: in reality this section is just vanity, should be cut or fleshed out
  
  \subsection{Achievable Constraints}
  
  It would be interesting to precisely measure the mechanical
  loss of the coatings, but we will be satisfied if we can constrain the coating
  loss to be below a particular value $\phi_\mathrm{threshold}$. Fig.
  \ref{fig:coatConstraint} shows the minimum coating loss that can be
  distinguished by the background of mechanical loss due to the silicon substrate
  and measurement apparatus. We can see that for larger dilution factors, we are
  able to detect smaller coating mechanical losses regardless of a fixed 25\%
  uncertainty in dilution factor. However, note that the shaded regions reflect
  our uncertainty in the minimum detectable mechanical loss, not our projected
  uncertainty in our estimated coating mechanical loss.
  
  % TODO: See elog 1761
  
  % TODO: Flesh out. Based on Qryo/Loss\_Uncertainty/Loss\_Constraint.ipynb. Also a
  % Fisher calculation in this folder?
  
  \begin{figure}[h!]
    \centering
    \includegraphics[width=\textwidth]{Figures/Coating_constraint.pdf}
    \caption[Testbed Q Requirements]{A larger dilution factor corresponds to more strain-energy contained
      in the coating, and improves the ability to distinguish the true coating
      mechanical loss from 0. The shaded regions correspond to 25\% uncertainty in
      the true dilution factor.}
    \label{fig:coatConstraint}
    \index{figures}
  \end{figure}
  
  \section{Experimental Design}
  
  \subsection{Gentle Nodal Suspension}
  
  We updated the Gentle Nodal Suspension (GeNS) system developed by \cite{GeNS} for cryogenic operation
  with an inverted cold plate. The primary benefit of this suspension system is
  excellent mechanical isolation from clamping losses that limit other common
  designs; the downside is inability to control sample temperature with high
  bandwidth due to weak thermal coupling.
  
  The design used for measurements throughout this chapter is in Fig.
  \ref{fig:GeNSdrawing}. An updated design compatible with inverted or
  normal-orientation cold plates is in \ref{fig:futureGeNS}.
  
  
  % TODO: add subsections on
  % Some questions I considered:
  % \begin{itemize}
  % \item thermal link for initial cooldown
  % \item radiative shielding, cold windows, etc
  % \item optical lever alignment
  % \item vacuum compatibility and cleanliness
  % \item electrical feedthrough
  % \item conductive cooling path
  % \item ESD actuation
  % \end{itemize}
  
  
  \begin{figure}[h!]
    \centering
    \includegraphics[width=.45\textwidth]{Figures/Disk_Assy.pdf} \hfill
    \includegraphics[width=0.45\textwidth]{Figures/Disk_Assy_inCryo.pdf}
    \caption[GeNS Solid Model]{Solid model of Gentle Nodal Suspension system. The
      right view shows the suspension system inside the cryostat as used for the
      measurements presented in this chapter.}\label{fig:GeNSdrawing}
    \index{figures}
  \end{figure}
  
  % TODO: label drawings for ease of understanding

  For the suspension point, we wanted a hard material that ideally is thermally
  conductive near 123 K. A sharper radius of curvature may slightly increase the
  surface area in contact with the sample, but also increases the maximum
  acceptable radial error for placing the disk on the sphere. We chose an
  uncoated sapphire plano convex lens with 200 mm focal length.

  % TODO: check lens focal length / ROC against purchase. This was for the quote
  % request only. Guild optics. 

  During pumpdown and cooldown, we rest the outer radius of the sample on an aluminum ring
  with ramped edges to maintain the sample's position relative to the suspension
  point. The ring also increases thermal contact of the sample with a cold
  ($\approx 85 K$) surface. After cooldown, the vacuum pump can be turned off to
  reduce vibrations. Then, we use a manual linear actuator mounted on the bottom
  of the cryostat to lower the holding ring and place the sample on the
  suspension point.
  
  \begin{figure}[h!]
    \centering
    \includegraphics[width=0.5\textwidth]{Figures/GeNS_3.pdf}
    \caption[Upgraded GeNS Solid Model]{Solid model of next generation Gentle
      Nodal Suspension. Rather than lowering the disk onto a suspension point, the
      disk is clamped during pumpdown and cooldown then the clamp is released for
      measurements.}
    \label{fig:futureGeNS}
    \index{figures}
  \end{figure}


  % TODO: would be good to include some math showing position tolerance for
  % stable suspension, also surface area and deformation at contact point. 

  % TODO: include some references on cryo design from elog 2472
  % some other drawings in elog 2471, 


% TODO: section on ESD coupling, TFs in elog 2184
  \subsection{Optical lever sensor}
  
  We used a HeNe laser and QPD to measure the motion of silicon wafers with an
  optical lever. We want the beam spot position on the QPD to be maximally
  sensitive to angular displacement of the Si wafer, without saturating the range
  defined by the QPD radius.
  
  %Working in a coordinate system centered on the beam axis with no deflection at
  %the Si wafer, the 
  
  We used an analytic ray tracing calculation (ABCD matrices) to optimize the
  position of a pair of telescoping lenses subject to the constraints on our
  optics table.
  
  % \begin{figure}[hbt!]
  %   \centering
  %    %   \includegraph3ics[width=.3\textwidth]{}
  %   \caption[Optical Lever Layout]{Optical lever layout}\label{fig:OpticalLeverLayout}
  %   \index{figures}
  % \end{figure}
  
  % \begin{figure}[hbt!]
  %   \centering
  %    %   \includegraphics[width=.3\textwidth]{}
  %   \caption[Optical Lever Lens Placement]{Optimal lens placement for optical lever}\label{fig:LeverLenses}
  %   \index{figures}
  % \end{figure}
  
  % TODO: details in elog 2156
  % TODO: add optimal op lev layout?
  
  \subsection{Temperature Sensing and Control}
  
  We instrumented the cryostat with Pt RTDs and read them out with 4-lead resistive
  measurements using a custom current driver and instrumentation amplifier.
  However, due to low thermal coupling of the sample to its environment, we need
  to independently estimate the sample temperature.
  
  Maintaining a high Q mechanical system is incompatible with traditional
  temperature sensing based on strong thermally conductive coupling between a
  sample and a thermometer like an RTD. There are, however, a variety of remote or
  noncontact temperature sensing techniques \cite{temp_remote_filmThickness}
  \cite{temp_SiC} \cite{temp_nonContact_IFO} \cite{temp_nonContact_IFO_fiber}.
  
  Silicon has a number of well-characterized temperature-dependent material
  properties, including dielectric function \cite{Si_dielectric_temp} and Young's
  modulus \cite{Si_Youngs} \cite{Si_Youngs_beam} \cite{temp_Si_cantilevers_Y}.
  Likewise, some of the thin films under study have temperature dependent
  properties \cite{capacitance_aSi}. Following \cite{Q_NawrodtApparatus}'s work
  in crystalline Si, we took advantage of the temperature dependent Young's
  modulus of silicon to predict the temperature-dependent eigenfrequency of the
  Si wafer's acoustic modes.

  During measurement, we continuously excite at least one eigenmode of the Si
  sample and track its eigenfrequency. We implemented both a phase-locked loop
  and zero crossing counter to track the eigenfrequency, but found the latter
  more robust and less expensive for our realtime model.

  The change in Young's modulus causes a fractional change in the wafer's
  eigenfrequency. The eigenmode and eigenfrequency are set by the wafer
  geometry, but the change in eigenfrequency is only set by the change in
  material properties. We can achieve a similar measurement
  bandwidth for measuring any mode frequency up to the Nyquist frequency of our
  digital control system. Therefore, higher frequency eigenmodes typically
  provide better temperature sensitivity. We estimate that the sensitivity of a
  30 kHz eigenmode near 123 K is 0.38 Hz/K.

  We could reduce the systematic error of eigenfrequency-based temperature
  estimates by using the temperature dependence of
  many eigenmodes, and include the effect of the temperature dependence of
  additional material properties (especially Poisson's ratio). The analysis in
  Chapter \ref{tomo} shows one way to implement this in post-processing.

  However, even a single eigenfrequency is a sufficient sensor for realtime
  temperature sensing and control. Our ability to control wafer temperature is
  limited by actuation, not sensing. We apply heat by flowing current through a nichrome
  heating wire located above the wafer. However, Si has a low emissivity, so
  this is not a strong actuator. Modulating HeNe power can also change the
  ultimate disk temperature, but also creates temperature gradients across the
  wafer.
  
  % Some small items to consider including
  % \begin{itemize}
  % \item calculated thermal load due to lead resistance for RTDs
  % \item RTD calibration
  % \item heat straps
  % \end{itemize}
  
  % sensitivity calculation around 2130
  
  
  % \begin{figure}[hbt!]
  %   \centering
  %    %   \includegraphics[width=.3\textwidth]{}
  %   \caption[GeNS Cooldown]{GeNS cooldown curves}\label{fig:GeNSCooldown}
  %   \index{figures}
  % \end{figure}
  
  % \begin{figure}[hbt!]
  %   \centering
  %   \caption[Eigenfrequency Temperature Dependence]{Eigenfrequency temperature calibration}\label{fig:freqToTemp}
  %   \index{figures}
  % \end{figure}
  
  % \begin{figure}[hbt!]
  %   \centering
  %   \caption[Temperature Readout Circuit]{RTD circuit diagram and liso noise budget}\label{fig:RTDcircuit}
  %   \index{figures}
  % \end{figure}
  
  % TODO: section on moderinger, loop shaping. See eg elog 2361 and thread
  
  \subsection{Vacuum and Cryogenics}
  
  We operated several liquid nitrogen cryostats from IR Labs. For
  electronic and mechanical design, we consulted the excellent reference by White
  \cite{cryo_White}.
  
  Unfortunately, several features of the cryostat proved limiting for GeNS
  measurements. First, the cryostat's inverted cold plate complicated the process
  of loading and suspending samples. Without access from above the GeNS apparatus
  inside the cryostat,
  we needed to remove the entire apparatus each time we installed a Si wafer.
  Rather than fixing the GeNS apparatus relative to the cold plate then closing the cryostat, our mechanical design fixed the sample
  relative to the cryostat's bottom lid and the entire experimental apparatus,
  sample, and lid were lifted to close the cryostat. This complicated the process
  of sample installation and optical lever alignment, and led to undue stress on
  the electromechanical connections requiring frequent maintenance.
  
  Furthermore, the aluminum thermal shield was not in thermal contact with the
  liquid nitrogen reservoir by design. Instead, we needed to connect the cold
  plate to the thermal shield with thermally conductive copper straps. Because
  they were not part of the initial design, we had trouble fitting enough straps
  into the limited cryogenic volume to achieve good thermal coupling.
  
  Finally, due to the low emissivity of Si, weak thermal coupling provided by
  GeNS, and absorptive heat load of the optical lever laser, the GeNS-suspended Si
  wafer temperature stabilizes well above ($\Delta T\approx 60 K$) the temperature of the nodal suspension
  point.
  
  % TODO: photo from 2326 of cold straps
  % bunch of notes in 2320
  % bunch of photos in 2316... most embarrassing
  % better photos in 2283
  % photos in elog 2166, 2162
  
  \begin{figure}[h!]
    \centering
    \includegraphics[width=\textwidth]{Figures/IRlabs_pumpdown.png}
    \caption[Cryostat Pumpdown Curve]{Pumpdown curve showing vacuum pressure
     inside cryostat containing GeNS experiment.}\label{fig:IRlabsPumpdown}
    \index{figures}
  \end{figure}
  
  % \begin{figure}[hbt!]
  %   \centering
  %  %   \includegraphics[width=.3\textwidth]{}
  %   \caption[Cryostat Leak Rate]{Leak rate}\label{fig:IRLabsLeakRate}
  %   \index{figures}
  % \end{figure}

  % \begin{figure}[hbt!]
  %   \centering
  %  %   \includegraphics[width=.3\textwidth]{}
  %   \caption[Cryostat Photos]{IR Labs Cryostat Photos}\label{fig:IRLabsPhotos}
  %   \index{figures}
  % \end{figure}
  
  % \begin{figure}[hbt!]
  %   \centering
  %  %   \includegraphics[width=.3\textwidth]{}
  %   \caption[Cryostat Cooldown]{Cooldown}\label{fig:CryostatCooldown}
  %   \index{figures}
  % \end{figure}

  % \subsection{Cooldown Simulations}
  
  % TODO: COMSOL model of temp gradients along post elog 2389
  % The wafer's thermal equilibrium is set by a combination of radiative
  % cooling (very minimal due to low emissivity of thin Si wafers), conductive cooling to the mount,
  % and radiative load from laser absorption.
  % \section{Mechanical Loss Measurements}


%  \subsection{Frequency tracking}

%  %  TODO: described in 2344,

  \section{Measurements}

  \subsection{Moderinger}
%  \subsection{Eigenfrequencies}

  % TODO: add subsection on matching COMSOL eigenmodes to measurements,
  % including degeneracies

  % TODO: see elog 2431 for eigenmodes (from anna)
  % TODO: elog 2374 documents temperature increasing during measurements due to
  % HeNe heating
  % lots of elogs... 2334 has moderinger and ambient RTD temp measurements
  
  We used a moderinger model (\cite{Moderinger}) to make mechanical loss
  measurements. The measurement consists of exciting a particular eigenmode at a
  fixed amplitude using an amplitude locked loop. The loop unity gain frequency
  and excitation amplitude provides an estimate of the instantaneous
  Q of the excited eigenmode.

  In Fig. \ref{fig:moderinger}, we compare measured quality factors of three
  eigenmodes of a 2'' Si wafer held near 150 K over a couple hours.

  \begin{figure}[h!]
    \centering
    \includegraphics[width=\textwidth]{Figures/moderinger_measurements.png}
    \caption[Moderinger Measurements]{Comparison of measurements made with
      moderinger and traditional ringdown. From top to bottom, we show mode
      amplitude, mode excitation, and estimated quality factor over time.
      Eigenmode shapes calculated from COMSOL are displayed next to their
      respective traces. The quality factor for modes 0 and 1 are estimated with
      a rolling average of the decay rate, with these modes repeatedly excited for
      short durations when their amplitudes fell below a fixed value. The quality
      factor for mode 2 is estimated from the amplitude locked loop drive.}
    \label{fig:moderinger}
    \index{figures}
  \end{figure}

  We made several
  temperature dependent mechanical loss measurements of Si wafers, shown in Fig.
  \ref{fig:Qs}.
  
  We also developed a loss budget analogous to
  \cite{Q_lossBudget}, taking into account both temperature \cite{Q_Ge_Si_ofT}
  and frequency- or geometry- dependent effects. The loss budget and details on the physical
  interpretation of quality factor measurements are in Chapter \ref{tomo}. The
  quality factors in \ref{fig:Qs} are consistent with thermoelastic losses, as
  expected for measurements well above 123 K. Still, based on typical dilution
  factors $\approx 10^{-3}$ for thin films deposited on Si wafers, we expect to be able to
  constrain the mechanical loss of test films to $\approx 3\times 10^{-6}$. 
  
  \begin{figure}[h!]
    \centering
    \includegraphics[width=\textwidth]{Figures/Q_measurements.pdf}
    \caption[Quality Factor Measurements]{Collection of quality factor
      measurements made on similar 2'' Si wafers with SiNx surface passivation.}
    \label{fig:Qs}
    \index{figures}
  \end{figure}


%  The limiting mechanical loss in amorphous systems like silica glass are two
%  level system transitions \cite{Q_glass_TLS}.
  % TODO: move this to tomo chapter
  
%  \begin{figure}[hbt!]
%    \centering
%    % \includegraphics[width=.3\textwidth]{}
%    \caption[Moderinger Control Loop]{moderinger control loop diagram}\label{fig:moderingerLoop}
%    \index{figures}
%  \end{figure}
  
%  \begin{figure}[hbt!]
%    \centering
%    % \includegraphics[width=.3\textwidth]{}
%    \caption[Si Wafer Loss Budget]{loss budget for bare wafer Q measurements}\label{fig:LossBudget}
%    \index{figures}
%  \end{figure}
  
  % include some high Q measurements
  % elog 2477 and 2481
  % 2456
  % 2368, also talks about using PLL phase to separate degenerate modes
  % 2367, this is also mostly about mode shapes
  % mode splittings 2366
  % 2359
  % 2356
  % 2355
  % 2349
  % 2348
  % 2341
  % 2340
  % 2201 -- first Q of >1e6 (3 million)
  % elog 1726, 1707, 

  \section{Lessons for Future Work}

  Aside from cryogenic design, the more fundamental challenge in cryogenic
  mechanical loss measurements with gentle nodal suspension is maintaining a
  stable temperature throughout a quality factor measurement. As described in
  Fig. \ref{fig:TE_vsT}, the Si substrate experiences a sharp increase in
  thermoelastic loss away from Si's zero thermal expansion temperature.
  Measurements of time-varying mechanical loss are systematically biased
  towards the highest loss experienced in any sub-volume of the oscillator
  during the measurement time.

  To achieve temperature control over the entire Si oscillator, we require tight
  thermal coupling of the oscillator to an actuator. However, this coupling must
  reflect low frequency acoustic waves that would spoil mechanical Q. The field
  of metamaterials for structural and nanomechanical design offers promising
  examples for achieving this. %, and a selection of examples from the
                               %literature are in Appendix \ref{Meta}.

  In brief, one could start by considering mechanical oscillators consisting of a cantilever
  clamped at one end or wafer clamped on the outer edge. Then, introduce
  exclusions to the substrate material to increase the effective cantilever
  length or wafer radius and decrease the fundamental eigenfrequency of the
  suspension. The higher frequency acoustic eigenmodes of the remaining material
  farthest from the clamp point are thus acoustically isolated from the clamp,
  but heat can still flow along the suspension. Designing the exclusions that
  the dominant coupling between exclusion layers is torsional (as in the double
  paddle oscillator) can provide further isolation.

  A more clever approach explored by Abe \cite{Mahiro_report} is to design a
  cantilever (or wafer) such that acoustic waves encounter several effectively
  independent mechanical
  oscillators before reaching an isolated oscillator. Each oscillator introduces
  a phase shift for acoustic waves near its resonance, so together they can act
  as a mechanical Bragg grating to enhance isolation in a particular frequency
  band.

  One can also consider changing the geometry of the coating to enhance its
  coupling (and contribution to mechanical loss) for specific eigenmodes of the
  substrate. For example, if the coating is only deposited along lines of high
  stress for a 3rd order butterfly mode of the substrate, it will preferentially
  introduce loss to the 3rd order butterfly mode and less to other eigenmode.
  This can improve the ability to distinguish coating from substrate or
  environmental mechanical
  loss. 

%  \printbibliography[heading=subbibliography]
  
%\end{refsection}