
%\begin{refsection}

  \chapter{Optomechanical Sensing}\label{optomech}

  The purpose of this chapter is to define the mathematical framework employed
  throughout the rest of the work. For the most part, we follow the approach of
  \cite{IFO_math}. Other good reviews of cavity optomechanics include
  \cite{optomech_review}.
  
  \section{Optical Sensing}

  The basic componenents of optomechanical systems are lasers, beam blocks
  or photodetectors, free space propagation, and beamsplitters. Nonlinear media
  with complicated dispersion relations can be used to produce various types of
  correlators, but we will mostly not consider those in this work.
  
  \subsection{Lasers, Vacuum, and Strain}
  
  The operator of the quantized electric field is \cite{Haixing_thesis}

  \begin{equation}
    \hat{E} = u(x,y,z)\int_0^{+\infty}\frac{d\omega}{2\pi}\sqrt{\frac{2\pi\hbar\omega}{\mathcal{A}c}}
   [\hat{a}_\omega e^{ikz-i\omega t} + \hat{a}_\omega^\dagger e^{i\omega t-ikz}].
  \end{equation}

  We typically work within the two-photon formalism of Caves and Schumaker
  \cite{CavesSchumaker_1} \cite{CavesSchumaker_2}, which in the frame rotating
  at $\omega_0$ (typically some laser's carrier frequency) substitutes pairs of operators that
  create or annihilate photons at $\omega_0\pm \Omega$ for quadrature operators
  that describe the amplitude and phase of the carrier field at frequency
  $\Omega$.

  \begin{equation}
    \hat{a} \equiv \begin{pmatrix} \hat{a}_c \\ \hat{a}_s \end{pmatrix}
  \end{equation}

  Following the framework of Corbitt, Chen, and Mavalvala \cite{IFO_math}, an
  optomechanical system consists of a set of elementary subsystems represented
  by $N \times N$ matrices $\mathcal{M}_{N,N}$ relating $N$ incident quadrature fields
  to $N$ outgoing quadrature fields. Each element of $\mathcal{M}_{N,N}$ is itself a
  $2\times 2$ matrix describing how the two quadrature fields transform between
  the corresponding nodes in the system. The input nodes $u^{(i)}$ generically consist of
  contributions from vacuum fluctuations $v^{(i)}$, the laser $I^{(i)}$, and
  spacetime strain modulation $H^{(i)}h$.

  \begin{equation}\label{eq:FS}
    \hat{u}^{(i)} = \hat{v}^{(i)} + \hat{I}^{(i)} + \hat{H}^{(i)}h
  \end{equation}

  A beam block or photodiode is an element whose input-output relation has only the vacuum term, with no
  laser or strain coupled in. Classical (unsqueezed) vacuum sources have
  uncorrelated white noise with unit amplitude spectral density and zero mean
  in both directions, while squeezed or other nonclassical vacuum states
  conserve total noise power but can otherwise distribute the noise
  arbitrarily between the two quadratures.
  
  A laser may couple in a bright field at $\Omega=0$
  Hz, sideband fields at $\Omega \neq 0$, and classical noises associated
  with the laser, in addition to a possibly squeezed vacuum field. A carrier
  field with intensity $I_j$ and phase $\theta_j$ is represented by

  \begin{equation}
    \hat{D}_j\equiv \sqrt{2I_j}\begin{pmatrix} \cos\theta_j\\\sin\theta_j\end{pmatrix}.
  \end{equation}
    
  The contribution of spacetime strain is typically small enough to ignore except
  between nodes separated by a large propagation length containing a high power
  carrier field. Classical gravitational radiation's modification to a locally
  Minkovski metric $\eta_{\mu\nu}$, first described in a
  gauge-invariant form by Pirani \cite{Pirani1957}, can be expressed in the
  transverse-traceless gauge as

  \begin{equation}\label{hTT}
    \begin{split}
      g_{\mu\nu} & = \eta_{\mu\nu} + h_{\mu\nu} \\
      h_{\mu\nu} & = \cos(\Omega t - k z) \begin{pmatrix}
                                            0 & 0 & 0 & 0 \\
                                            0 & h_{+} & h_{\times} & 0 \\
                                            0 & h_{\times} & h_{+} & 0 \\
                                            0 & 0 & 0 & 0
                                          \end{pmatrix}
    \end{split}
  \end{equation}

  where $h_{+}$ and $h_{\times}$ are field amplitudes of the two radiation
  polarizations traveling at frequency $\Omega$ along $\hat{z}$. Eq \ref{hTT}
  can be rotated to find the strain due to gravitational radiation in an
  arbitrary direction with wavevector $k(k_x\hat{x} + k_y\hat{y}+ k_z\hat{z})$,
  which following \cite{SiggCal} modifies the total phase $\Phi(t_0)$ accumulated by light
  traveling length $L$ along $\hat{x}$ to

  \begin{equation}
    \Phi^x(t_0) = \frac{\omega}{c}\int_0^L\sqrt{1+h_{xx}\cos(\Omega t_0 + k(1-k_x)x)} dx.
  \end{equation}
  
  We can see that both the strain amplitude and effective wave vector along the
  propagation length depend on the orientation of the GW to the light.
  We can get an intuitive expression relevant to the optomechanical sensor by
  assuming an infinitesimal propagation length $dL$ such that the strain is
  uniform across the propagation length, and call $h$ the Fourier transform of
  the GW amplitude projected along $h_{xx}$. If the propagation length contains a
  carrier field
  $\hat{D}$ at $\omega$, the strain contribution to \ref{eq:FS} is given by \cite{IFO_math}

  \begin{equation}
    \hat{H} \equiv \sqrt{\frac{\omega}{4c^2\hbar}}\hat{D}^* dL.
  \end{equation}
  
  The character \textsuperscript{*} refers to quadrature rotation by $\pi/2$, for
  example

  \begin{equation}
    \begin{pmatrix} v_1 \\ v_2 \end{pmatrix}^* = \begin{pmatrix} v_2 \\ -v_1 \end{pmatrix}.
  \end{equation}

  In the expression for strain coupling, \textsuperscript{*} indicates the pump laser
  acquires phase fluctuations proportional to its field amplitude. 

  \subsection{Free Space Propagation}

  Perhaps the most remarkable and useful behavior of photons is that their phase evolution keeps
  track of their proper distance travelled. The matrix
  for the free space propagator is \cite{IFO_math}
  \begin{equation}
    M_\mathrm{prop} \equiv e^{i\phi}\begin{pmatrix} 0 & R_\Theta \\ R_\Theta & 0 \end{pmatrix}
  \end{equation}

  where $M_\mathrm{prop}$ relates
  $\begin{pmatrix} \hat{a}_1 \\
     \hat{a}_2 \end{pmatrix}$ to
   $\begin{pmatrix} \hat{b}_1 \\
      \hat{b}_2 \end{pmatrix}$
    as described in
    \ref{fig:basicPorts}
    and
    
  \begin{equation}
    \Theta \equiv \frac{\omega_0 L}{c},
    \phi\equiv \frac{\Omega L}{c},
    R_\Theta \equiv \begin{pmatrix} \cos \Theta & -\sin \Theta \\ \sin\Theta & \cos\Theta \end{pmatrix}.
  \end{equation}

  \subsection{Beamsplitters}
  
  Beamsplitters are the basic units that perform linear operations on
  propagating electric fields. Beamsplitters have four input and four output
  ports, which we label in \ref{fig:basicPorts}. Neglecting briefly the dynamics of the
  beamsplitter itself, a beamsplitter with reflectivity $\rho$,
  transmissivity $\tau$, and optical loss $\eta^2 \equiv 1-\rho^2-\tau^2$
  transforms input fields as Eq \ref{eq:BS}.

  \begin{figure}[h!]
    \centering
    \includegraphics[width=\textwidth]{Figures/beamsplitter.pdf}
    % \\[\bigskipamount]
    % \includegraphics[width=\textwidth]{Figures/freeSpace.pdf}
    \caption[Basic Optomechanics Ports]{Port labelling for basic optomechanical
      elements. Right: Beamsplitter labelled
      with four input and four output ports. Left: Port labels for free space propagation.}
    \label{fig:basicPorts}
    \index{figures}
  \end{figure}


  \begin{equation}\label{eq:BS}
    \begin{pmatrix} \hat{b}_1 \\ \hat{b}_2 \\ \hat{b}_3 \\ \hat{b}_4 \end{pmatrix} = M_\mathrm{BS}
    \begin{pmatrix} \hat{a}_1 \\ \hat{a}_2 \\ \hat{a}_3 \\ \hat{a}_4 \end{pmatrix} -\eta 
    \begin{pmatrix} \hat{v}^{(1)} \\ \hat{v}^{(2)} \\ \hat{v}^{(3)} \\ \hat{v}^{(4)} \end{pmatrix}
  \end{equation}

  We can adapt the expression of \cite{IFO_math} to write
  
  \begin{equation}
    M_\mathrm{BS} \equiv
    \begin{pmatrix}
      -\rho & 0 & 0 & \tau \\
      0 & -\rho & \tau & 0 \\
      0 & \tau & \rho & 0 \\
      \tau & 0 & 0 & \rho
    \end{pmatrix}.
  \end{equation}

  ``Mirrors'' are simply beamsplitters operated at normal incidence, such that
  we can identify
  $\hat{a}_1\leftrightarrow \hat{a}_2$, $\hat{b}_1\leftrightarrow \hat{b}_2$,
  $\hat{a}_3\leftrightarrow \hat{a}_4$, $\hat{b}_3\leftrightarrow\hat{b}_4$. 
  
  \subsection{Cavities}

  Cavities are a collection of $N\geq 2$ beamsplitters (or other 4-port optical
  elements) arranged such that for some choice of labels,
  \begin{itemize}
    \item on one side of beamsplitter $1$, at least one of the incident beams
      arrives from beamsplitter $N$ and is reflected to beamsplitter $2$,
    \item for $n>1$, at least one beam incident on beamsplitter $n$ comes from
      beamsplitter $n-1$.
  \end{itemize}
    

  Optical cavities are used in many sensing problems, including gyroscopes
  \cite{microresonator_gryo} and gravitational wave detection.

  %(TODO, add some citations to applications of
  %cavities for sensing, LIGO, etc. Or move to intro).

  The cavity's free spectral range (FSR) is the separation (in frequency units)
  of the cavity resonances. For a cavity in free space, it is set by the round
  trip cavity length alone:
 
  \begin{equation}\label{eq:FSR}
    \Delta\nu_{FSR} = \frac{c}{L_\mathrm{roundtrip}}.
  \end{equation}


  %TODO: define finesse

  %TODO: move cavity input-output derivation here from QIFO?


  \subsection{Radiation Pressure}

  In the two-quadrature picture, the momentum flow carried by an optical field
  with power $I_j$ at node $j$ is

  \begin{equation}
    \dot{P}_j(\Omega) = \sqrt{\frac{\hbar \omega}{c}}\hat{D}_j^T\hat{j}(\Omega)
  \end{equation}

  where $\hat{j}(\Omega)$ is the sideband component at $\Omega$.


 
  Here again we have relabelled expressions in \cite{IFO_math}, which can
  be used to 
  derive the input-output relation for the beamsplitter of mass $M$ with
  radiation pressure and optical losses.

  \begin{equation} \label{eq:BS_IO}
    \begin{split}
      [\mathbb{I} + \frac{\Pi}{2}
      \begin{pmatrix}
        D_{\hat{a}_1}^* \\ D_{\hat{a}_2}^* \\ D_{\hat{a}_3}^* \\ D_{\hat{a}_4}^*
      \end{pmatrix}
      \begin{pmatrix}
        D_{\hat{b}_1}^T & D_{\hat{b}_2}^T & -D_{\hat{b}_3}^T & -D_{\hat{b}_4}^T
      \end{pmatrix}
      ]
      \begin{pmatrix}
        \hat{b}_1 \\ \hat{b}_2 \\ \hat{b}_3 \\ \hat{b}_4
      \end{pmatrix} \\
      =
      [M_\mathrm{BS} - \frac{\Pi}{2}
      \begin{pmatrix}
        D_{\hat{a}_1}^* \\ D_{\hat{a}_2}^* \\ D_{\hat{a}_3}^* \\ D_{\hat{a}_4}^*
      \end{pmatrix}
      \begin{pmatrix}
        D_{\hat{a}_1}^T & D_{\hat{a}_2}^T & -D_{\hat{a}_3}^T -D_{\hat{a}_4}^T
      \end{pmatrix}
      ]
      \begin{pmatrix}
        \hat{a}_1 \\ \hat{a}_2 \\ \hat{a}_3 \\ \hat{a}_4
      \end{pmatrix} -
      \eta \begin{pmatrix} \hat{v}^{(1)} \\ \hat{v}^{(2)} \\ \hat{v}^{(3)} \\ \hat{v}^{(4)} \end{pmatrix}
    \end{split}
  \end{equation}

  Roughly, the new terms account for additional phase accumulated by the
  incident fields due to the effect of radiation pressure of the incident and
  outgoing fields on the mirror position.
  
  A beamsplitter operated at angle of incidence $\alpha$ with mechanical
  responsivity $\chi(\Omega)$ has

  \begin{equation}\label{eq:Pi}
    \Pi\equiv \frac{2\rho \omega}{c^2}\cos^2(\alpha)\chi(\Omega)
  \end{equation}

  which for a beamsplitter that can be treated as a free mass $M$ becomes
  \begin{equation}
    \Pi= \frac{2\rho \omega}{M\Omega^2c^2}\cos^2(\alpha).
  \end{equation}
  
  \section{Optomechanical Systems with Radiation Pressure}

  The ponderomotive (radiation pressure) force leads to parametric instability in optomechanical
  systems \cite{parametricInstability}, but also is a useful resource for sensing.

  \subsection{Optomechanical Cooling}

  Using optical fields to cool mechanical oscillators can significantly enhance
  the sensitivity of mechanical sensors, since the effective Q of optomechanical
  systems dominated by optical forces can be significantly enhanced relative to
  the Q of the bare mechanical oscillator.

  Recent advances in quantum optics have enabled quantum-enhanced cooling of
  mechanical systems \cite{cooling_squeezed} \cite{Corbitt_rigidity} or cooling to
  the mechanical oscillator's ground state \cite{Corbitt_groundStateCool}. Even the
  remaining displacement noise of an optomechanically cooled oscillator can be
  suppressed with appropriate sensing \cite{TN_suppression_optomech}. 

  \section{Gravitational Wave Interferometers}
  Gravitational wave interferometers are sensitive force and displacement
  senesors used to detect gravitational waves and place limits on
  fundamental physics \cite{GWIFO_constants_DM}. 
  Interferometric gravitational wave observatories (GWIFO) like LIGO
  \cite{LIGO_instrument} are thoroughly discussed in the classic book by Saulson
  \cite{Saulson_book} and in \cite{Hall_thesis} \cite{GWIFO_review}
  \cite{IFO_review} \cite{GWIFO_review_Giazotto}. TGWIFO consist of freely
  falling test masses (TMs) of mass $m$, mechanically isolated by pendula such
  that Earth's (mostly dissipation-free) gravitational potential provides their
  dominant restoring force. Two pairs of TMs in LIGO-like GWIFOs form two
  orthogonally oriented Fabry-Perot (FP) cavities, which sense the phase
  accumulated by light propagating in each FP cavity. The relative phase noise
  of each FP is separated from common-mode laser noise by combining the FP
  outputs on a beamsplitter, forming a Fabry-Perot Michelson Interferometer
  (FPMI). And, normal-incidence recycling mirrors on each input port of the FPMI
  set the impedance matching condition of light entering or leaving the
  dual-recycled FPMI (DRFPMI).

  \subsection{Mechanics}

  When considering quantum measurement with a GWIFO, we must treat both the free
  test masses and laser field quantum mechanically. The relative position
  $\hat{x}$ and momentum $\hat{p}$ operators of the TMs obey equations of motion
  \cite{Haixing_thesis}:

  \begin{equation}
    \begin{split}
      \dot{\hat{x}}(t) &= \frac{\hat{p}}{m} \\
      \dot{\hat{p}} &= \frac{\hat{I}}{c} + mL\ddot{h}(t).
    \end{split}
  \end{equation}

  \subsection{Some noises}

  The limiting noises of the current generation of GW interferometers are
  discussed in \cite{GWIFO_sensitivity}. The most fundamental limiting noises are
  quantum noise due to quantization of the sensing light field (introduced as $\hat{v}$ in
  Eq \ref{eq:FS}), and thermal
  noise due to Brownian motion of various parts of the mechanical system
  (coatings, suspension, test mass substrates) \cite{Whittle_unifyingNoises}.

  Thermal noise can be attributed to loss in the mechanical susceptibility
  of the test mass to forces applied by the laser \cite{TN_Levin} \cite{TN_Gonzalez}.

  The first complete treatment of quantum noise in second generation GW
  interferometers are the
  classic works by Buonnano and Chen \cite{BnC_SRC} \cite{BnC_opticalSpring}. An
  excellent review on the topic is \cite{Danilishin_review}.
  
%  \subsection{Ponderomotive Squeezing in a 40m Interferometer}
%
%  TODO: cut?
  
%  \printbibliography[heading=subbibliography]
  
%\end{refsection}
