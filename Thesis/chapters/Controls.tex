
%\begin{refsection}

  \chapter{Control System} \label{Control}

  Figure \ref{fig:controlDiagram} shows the conceptual optical layout of our
  amplifier demonstration, and the control system discussed in the text. This
  chapter describes the critical sensors, actuators, and controllers for
  operating the amplifier in its current configuration. We also discuss
  a scheme for reducing input-referred pump frequency noise by controlling Mach-Zehnder length.

  \section{Sensors}

  \subsection{Photodiodes for Relative Intensity Sensing}

  The DC-coupled port of a photodiode monitors intensity fluctuations of the
  total field incident on the diode. Though we can get a rough sense of pump and
  probe laser powers with photodiodes, to reduce calibration error we typically
  compare relative intensity fluctuations on these monitors.

  For example, the cavity transmission photodiode (TRANS) is a good sensor for
  relative intensity fluctuations of the cavity's circulating field. The
  photodiode monitoring pump pickoff (PMON) just before the vacuum chamber senses
  relative intensity fluctuations of the pump laser. The cavity reflection
  photodiode (REFL) monitors the sum of pump and probe fields reflected from the
  cavity.

  Higher order transverse (spatial) modes can influence intensity
  measurements. For example, both 00 and higher order Hermite-Gaussian modes can
  coresonate in the cavity and transmit to TRANS PD. Any scatter or clipping on
  the transmission beam path will cause these orthogonal modes to interfere on
  TRANS PD, leading to intensity fluctuations that do not correspond to amplitude
  fluctuations of the cavity's resonating 00 mode. This effect is even more
  pronounced on REFL PD, which can see significant power from higher order
  Laguierre-Gaussian modes due to mode mismatch of either pump or probe beams
  relative to the cavity.
  
  \subsection{Pound-Drever-Hall Error Signal}
  Pound-Drever-Hall locking, the technique of dithering a laser frequency far
  outside the cavity bandwidth to derive an error signal linear in the
  laser-cavity frequency difference, is widely used in laser stabilization
  including LIGO's arm length stabilization \cite{ALS_thesis}. A good,
  pedagogical explanation of PDH is from Black \cite{PDH_Black}, who shows
  that the PDH frequency discriminant on-resonance is
  
  \begin{equation}
    D\equiv -\frac{8\sqrt{P_cP_s}}{\delta \nu}
  \end{equation}

  where $P_c$ is the carrier field power, $P_s$ is the sideband field power, and
  $\delta\nu\equiv \Delta\nu_{FSR}/\mathcal{F}$ is the cavity linewidth
  determined by the cavity's free spectral range $\nu_{FSR}$ and finesse
  $\mathcal{F}$.
  
  For a given pump laser power setting the shot noise level of the PDH
  measurement, sensitivity of the PDH sensor is determined in part by the PDH sideband
  modulation depth. By beating the modulated pump with a second probe laser, we
  can read off the modulation depth from an RF spectrum analyzer as

  \begin{equation}
    \frac{P_\mathrm{carrier-probe}}{P_\mathrm{sideband-probe}} = \frac{J_0(m)^2}{J_1(m)^2}.
  \end{equation}

  for $J_\nu(m)$ the $\nu$ order Bessel function of the 1st kind at modulation
  index $m$ and $P_\mathrm{field A - field B}$ the optical power in the beat
  note between field A and field B. The optimal SNR for the PDH error signal
  occurs at $m=1.08$, but we choose a lower modulation depth around $0.15$ to
  avoid unwanted polarization rotation at the fiber EOM.

  We can directly measure the slope of the PDH error signal on-resonance ($D$)
  by sweeping the carrier laser frequency and both sidebands through resonance
  much faster than the cavity linewidth or any dominant frequency noises. The
  sideband fields are a known frequency away from the main carrier, so it is
  straightforward to plot the swept error signal against cavity frequency. The
  form of the total error signal can be found in \cite{PDH_Black}, and is
  plotted in \ref{fig:PDH_fit}. Instead of fitting the full error signal, we can
  also use the analytic form of the PDH error signal near resonance to read off
  the PDH slope only from the peak-to-peak error signal voltage and frequency
  separation of the main carrier field as

  \begin{equation}
    \frac{\partial \epsilon}{\partial f} = 2\frac{\Delta V_\mathrm{pkpk}}{\Delta t_\mathrm{pkpk}}
    \frac{\Delta t_\mathrm{sideband}}{\Delta f_\mathrm{sideband}}
  \end{equation}

  \begin{figure}[h!]
    \centering
    \includegraphics[width=\textwidth]{Figures/PDH_fit.pdf}
    \caption[PDH Error Sweep]{PDH error signal fitted to find a frequency discriminant of $2.2$
      MHz/V.}
    \label{fig:PDH_fit}
    \index{figures}
  \end{figure}

  %TODO: add information from elog 3154
  
  \subsection{Delay-line Frequency Discriminator}

  Though we ultimately need to phaselock the pump and probe lasers, the
  experiment presents some challenges. With the pump laser PDH locked to the
  cavity, free-running pump-probe beat note frequency noise can exceed 200 MHz-pp at the high Q
  cantilever's resonance (typically 20-70 Hz, depending on choice of
  cantilever). Even with loop suppression, the resulting phase deviations can
  approach or exceed the linear range of our PLL sensor.
  We therefore require a beat note frequency sensor with a relatively
  wide dynamic range in order to linearize the phaselock response for large
  frequency deviations. A wide band frequency sensor can also prestabilize the beat note frequency in a
  low-bandwidth (100s Hz) loop to aid in PLL lock acquisition.

  \begin{figure}[h!]
    \centering
    \includegraphics[width=\textwidth]{Figures/DFD_diagram.pdf}
    \caption[Delay Line Frequency Discriminator]{Electronics diagram for the
      delay line frequency discriminator. In our application, the signal is the
      beat note between pump and probe lasers measured at REFL PD.}
    \label{fig:DFD_diagram}
    \index{figures}
  \end{figure}

  We use an RF delay-line frequency discriminator (DFD) to sense beat note frequency fluctuations. The DFD is based on an unbalanced Mach-Zehnder
  interferometer as in Fig. \ref{fig:DFD_diagram}. The beat note is split into two paths (using ZFRSC-42-S+) then recombined at
  an RF mixer (ZFM-3-S+). To see how this works, consider the total electric
  field due to pump and probe carriers mixing on a photodiode, ignoring an
  overall phase:

  \begin{equation}
    E(t) = E_\mathrm{pump}e^{i\omega_\mathrm{pump} t} + E_\mathrm{pump}e^{i\omega_\mathrm{probe} t}.
  \end{equation}

  Typical commercial InGaAs photodiodes have a sensitivity $\approx 1$ A/W at
  1550 nm. The current is transduced to a voltage by a low noise transimpedance
  amplifier such that the overall responsivity is $\mathcal{R}(\Omega)$ V/W. Let
  us assume for now the photodiode has a flat frequency response at its DC and
  AC coupled outputs, $\mathcal{R}(\Omega)\equiv \mathcal{R}_\mathrm{DC,AC}$.
  The AC-coupled output voltage then highpasses $\mathcal{R}(\Omega)|E(t)|$:

  \begin{equation}
    V_\mathrm{PD}(t) = 2\mathcal{R}_\mathrm{AC}
    E_\mathrm{pump}E_\mathrm{probe}\cos((\omega_\mathrm{pump} - \omega_\mathrm{probe})t).
  \end{equation}

  We want to stabilize the beat note frequency $\omega_\mathrm{beat}(t) \equiv
  \omega_\mathrm{pump}(t) - \omega_\mathrm{probe}(t)$. After the RF splitter,
  the beat note propagates at velocity $c_\mathrm{cable}\approx 2\times 10^8 \mathrm{m/s}$ in cables of length $L_\mathrm{L,R}$ such that the
  signals entering the mixer's LO and RF ports are:

  \begin{equation}
    V_\mathrm{i}(t) = \frac{1}{2}\mathcal{R}_\mathrm{AC}E_\mathrm{pump}E_\mathrm{probe}
    \cos(\omega_\mathrm{beat} (t - \frac{L_i}{c_\mathrm{cable}})).
  \end{equation}

  The mixer output is then lowpassed, such that the DFD output varies
  sinusoidally with $\Delta L\equiv L_\mathrm{R} - L_\mathrm{L}$ and
  $\omega_\mathrm{beat}(t)$. 
  
  \begin{equation}
    V_\mathrm{IF} \propto V_\mathrm{L}(t)V_\mathrm{R}(t) \\
    V_\mathrm{out} = V_0 \cos(\frac{\omega_\mathrm{beat}(t)\Delta L}{c_\mathrm{cable}})
  \end{equation}

  When the beat note frequency satisfies $\frac{\omega_\mathrm{beat}(t)\Delta
    L}{c_\mathrm{cable}}=(N+\frac{1}{2})\pi$ for $N\in \mathbb{Z}$,

  \begin{equation} \label{eq:DFD_out}
    V_\mathrm{out}\approx V_0 \frac{\omega_\mathrm{beat}(t)\Delta L}{c_\mathrm{cable}}.
  \end{equation}

  To operate the double-balanced mixer as a phase detector, we use a combination
  of RF amplifiers and attenuators to ensure the beat note saturates the mixer
  diodes. This reduces the coupling of beat note amplitude variations to the
  mixer IF port. In practice the proportionality factor in
  Eq.~\ref{eq:DFD_out} for a level 7 mixer is $V_0 \approx 0.25
  \frac{\mathrm{V}}{\mathrm{rad}}$. If we read out the DFD output with an SR560 preamplifier with $\approx 4\mathrm{nV}/\sqrt{\mathrm{Hz}}$ voltage noise, the
  input referred noise of the DFD is inversely proportional to the path length
  difference along the two paths:

  \begin{equation} \label{DFD_noise}
    \hat{n}_\mathrm{freq} = \hat{n}_\mathrm{V} \frac{\mathrm{d}f_\mathrm{beat}}{V_\mathrm{out}}
    = \hat{n}_\mathrm{V} \frac{c_\mathrm{cable}}{2\pi V_0\Delta L} \approx
    1.3 (\frac{0.1 \mathrm{m}}{\Delta L} )(\frac{0.25 \mathrm{V}}{V_0})\frac{\mathrm{Hz}}{\sqrt{\mathrm{Hz}}}.
  \end{equation}

  %The above estimates are in good agreement with the measured DFD transfer function
  %\begin{figure}[hbt!]
  %  \centering
  %  % \includegraphics[width=\textwidth]{}
  %  \caption[Delay-line Frequency Discriminator Transfer Functions]{Transfer function of DFD}
  %  \label{fig:DFD_TF}
  %  \index{figures}
  %\end{figure}
  
  \subsection{Phase Locked Loop Error Signals}

  We ultimately need a sensor for pump-probe phase error, not just frequency
  error. We can measure the phase of the pump-probe beat note relative to a
  reference oscillator (like a digital or analog voltage controlled oscillator
  (VCO)) by homodyne demodulation. As we will see later, we would like to
  consider contributions to the demodulated signal not just of the carrier
  fields and their frequency fluctuations, but of sidebands of the carriers.

  Consider the electric field incident on a
  photodiode due to the combination of a pump laser at $\omega_\mathrm{pump}$
  and probe laser at $\omega_\mathrm{probe}$. The subscripted 0 or 1 indicates
  carrier or first order sideband, respectively. $\phi_\mathrm{mod}$ controls
  whether the probe's modulation sideband is AM ($\phi_\mathrm{mod}=0$) or PM
  ($\phi_\mathrm{mod}=\frac{\pi}{2}$).

  \begin{equation}
    \begin{split}
      E_\mathrm{beat} = & E_\mathrm{pump}e^{i\omega_\mathrm{pump,0} t}
                          [1 + \frac{E_\mathrm{pump,1}}{E_\mathrm{pump,0}}
                          \frac{e^{i\omega_\mathrm{pump,1}t} - e^{-i\omega_\mathrm{pump,1}t}}{2}] + \\
                        & e^{i\phi_\mathrm{probe}} E_\mathrm{probe,0}e^{i\omega_\mathrm{probe,0}t}
                          [1+\frac{E_\mathrm{probe,1}}{E_\mathrm{probe,0}} (\\
                        & \qquad \qquad \cos{\phi_\mathrm{mod}}
                          \frac{e^{i\omega_\mathrm{probe,1}t}
                          + e^{-i\omega_\mathrm{probe,1}t}}{2} + \\
                        & \qquad \qquad \sin{\phi_\mathrm{mod}}
                            \frac{e^{i\omega_\mathrm{probe,1}t}
                            - e^{-i\omega_\mathrm{probe,1}t}}{2}
                            )]
    \end{split}
  \end{equation}

  The voltage from the photodiode will then be proportional to optical power.
  We can focus only on terms rotating at the beat frequency $\omega_\mathrm{beat}\equiv
  \omega_\mathrm{pump} - \omega_\mathrm{probe}$, but let us also allow
  $\omega_\mathrm{probe,1}\approx \omega_\mathrm{beat} + \phi_\mathrm{probe,1}(t)$ while
  $\omega_\mathrm{pump,1}$ is far from 0 Hz and far from $\omega_\mathrm{beat}$

  \begin{equation}
    \begin{split}
      P_\mathrm{beat} = & e^{i\phi_\mathrm{probe}} E_\mathrm{probe,0}E_\mathrm{pump,0}
                          e^{-i\omega_\mathrm{beat}t} + \\
                        & E_\mathrm{probe,0}E_\mathrm{probe,1} \cos{\phi_\mathrm{mod}}
                          \frac{e^{-i\omega_\mathrm{probe,1}t} + e^{i\omega_\mathrm{probe,1}t}}{2} + \\
                        & E_\mathrm{probe,0}E_\mathrm{probe,1} \sin{\phi_\mathrm{mod}}
                          \frac{e^{-i\omega_\mathrm{probe,1}t} - e^{i\omega_\mathrm{probe,1}t}}{2} + \\
                        & c.c. \\
      = & 2 E_\mathrm{probe,0}E_\mathrm{pump,0}\cos(\omega_\mathrm{beat} t + \phi_\mathrm{probe}(t)) + \\
                        & 2 E_\mathrm{probe,0}E_\mathrm{probe,0}\cos\phi_\mathrm{mod}
                          \cos(\omega_\mathrm{beat}t + \phi_\mathrm{probe,1}(t)).
    \end{split}
  \end{equation}
  
  Due to the complex conjugation, terms involving
  $\sin\phi_\mathrm{mod}$ ideally will be cancelled. However, if
  there is some sideband asymmetry the cancellation will be imperfect. Terms
  with $\cos\phi_\mathrm{mod}\neq 0$ (probe amplitude modulation at $\omega_\mathrm{beat}$) are not cancelled.

  We then demodulate $P_\mathrm{beat}$ by demodulating against a VCO at
  $\omega_\mathrm{VCO}\equiv \omega_\mathrm{beat}$, this time keeping only low frequency terms. The
  demodulated and lowpassed voltage is the PLL error signal, $e_\mathrm{PLL}$.  

  \begin{equation}
    e_\mathrm{PLL} = K_\mathrm{PLL,0}\sin(\phi_\mathrm{VCO}(t) - \phi_\mathrm{probe,0}(t)) + K_\mathrm{PLL,1}\cos(\phi_\mathrm{mod})\sin(\phi_\mathrm{VCO}(t) - \phi_\mathrm{probe,1}(t))
  \end{equation}

  We have introduced $K$ as overall gains combining the various field
  amplitudes above, as well as losses associated with electronics and
  photodetection. For small noises associated with the probe's modulation
  sidebands, $e_\mathrm{PLL}$ is an error signal $\propto \phi_\mathrm{VCO}(t)-
  \phi_\mathrm{probe,0}(t)$, the phase of the pump-probe beat note relative to
  the VCO phase. A more detailed derivation and noise analysis of the PLL error
  signal without probe modulation sidebands is in the technical note from Gupta
  \cite{Anchal_PLL}. 
  
%  \subsection{Accelerometers}

%  To damp table motio

  \section{Actuators}\label{actuators}

  We want to actuate on optical field frequencies and amplitude, as well as
  mechanical positions. We mostly use rigid optical mounts, so pitch and yaw
  alignment do
  not change significantly during a single measurement.

  \subsection{Acoustic-Optic Modulator}

  We use an acoustic-optic modulator (AOM) to amplitude modulate our pump's
  carrier field and its PDH sidebands. With the AOM's 0th order transmitted beam
  aligned to the cavity and its 1st order transmitted beam dumped, we set the
  AOM's drive amplitude to reduce intensity on PMON by about 10\%. Amplitude
  modulating the AOM's drive amplitude then imparts AM sidebands on the 0th
  order transmitted beam. Because the AOM acts as a diffraction grating, any
  unwanted phase modulation on the AOM drive does not significantly phase
  modulate the 0th order transmitted beam.

  % TODO: add plot of AOM AM to PMON transfer function

  \subsection{Laser current}

  Actuating laser current directly modulates laser frequency. For AlGaAS diode
  lasers, the frequency modulation is due to carrier modulation at high
  frequency and temperature modulation at low frequencies \cite{Diode_FM}. Laser
  current modulation also has a typically undesired coupling to laser amplitude.

  \begin{figure}[h!]
    \centering
    \includegraphics[width=\textwidth]{Figures/laser_response.pdf}
    \caption[Laser Frequency Response]{Frequency response of TeraXion and Rio (N
      and S) lasers to voltage modulations at the
      input of their respective current drivers.}
    \label{fig:laser_resp}
    \index{figures}
  \end{figure}

  \subsection{Piezoelectric Actuator}

  % TODO: describe piezo, resonance, etc
  
  We use a piezoelectric actuator, shown in Fig. \ref{fig:cantileverPhotos}, in
  vacuum to push on the base of the cantilever mount. The piezo drives MC2
  relative to MC1 and MC3, filtered by the cantilever's mechanical response.
  
%  \begin{figure}[hbt!]
%    \centering
%    \includegraphics[width=\textwidth]{Figures/cantileverPiezo.jpg}
%    \caption[Cantilever Photograph]{Photograph showing silicon cantilever holding MC2. The stacked
%      piezoelectric actuator pushes on the cantilever's clamping block, driving
%      the cavity length. The shadow sensor beam and optics do not appear in this
%      photo.}                   %136;31M
%    \label{cantPiezo}
%    \index{figures}
%  \end{figure}

  % TODO: crop and group this with other cantilever photos
  
  % note: measurement from elog 3145

%  \subsection{Piezos}

%  TODO: add TF from elog 3197
  
  \section{Controllers}

  \subsection{Pound-Drever-Hall Loop}

  We lock the pump laser to the cavity resonant frequency with a PDH loop. The
  control filter has a zero at the cavity pole frequency to compensate phase
  delay introduced by the cavity. At low frequency, the pump current controller
  saturates. However, we feed back the integrated PDH control signal to pump
  diode temperature to stabilize the PDH loop on long (<1 Hz) timescales.

  

  \begin{figure}[h!]
    \centering
    \includegraphics[width=\textwidth]{Figures/PDH_TFs_mod.pdf}
    \caption[Pound-Drever-Hall Transfer Functions]{Transfer functions of the
      Pound-Drever-Hall loop. The gray regions are where the measurement had low
      coherence. The excitation was summed into the loop at the the PDH error
      point, where the residual noise has a mostly flat spectral density. The
      yellow controller transfer function derived from the measurement agrees
      well with the digital controller set in Moku. We cannot see the cavity
      pole in the plant transfer function magnitude because the cavity is
      pumped with p-polarized light (low cavity finesse) for this measurement.
      The unity gain frequency of the open loop transfer function is about 10
      kHz.}
    \label{fig:PDH_TFs}
    \index{figures}
  \end{figure}

  As shown in Fig. \ref{fig:controlDiagram}, we also feed back the PDH error
  signal to cantilever mirror position by piezoelectric actuator. We resonantly
  filter the piezo signal at the cantilever's fundamental resonance to damp the
  cantilever but minimally affect signals away from this eigenfrequency.

  % TODO: we also have transfer function measurements from elog 3046, with
  % s-polarized light but different cavity mirrors

  % TODO: wouldn't be hard to redo this measurement with the cavity in-air...
  

  % TODO: add piezo feedback from elog 3182

  
  \subsection{Ponderomotive rigidity}

  An optical cavity held close to resonance should not have a very rigid optical
  spring, because the cavity's circulating power has a maximum near resonance
  (the derivative of optical power with respect to frequency detuning is zero).
  However, by design PSOMA's light mirror has a low mechanical resonance, such
  that pump intensity fluctuations dominate the cantilever's motion. To
  understand the effect of the optical spring on mirror motion in PSOMA,
  consider the optical rigidity $K(\Omega)$ given by \cite{Corbitt_rigidity}

  \begin{equation}
    K(\Omega) = K_0\frac{1+(\delta/\gamma)^2}{(1+i\Omega \gamma)^2+(\delta/\gamma)^2}\\
    K_0 = \frac{128\pi I_0(\delta/\gamma)}{T_I^2 c\lambda_0}(\frac{1}{1+(\delta/\gamma)^2})^2
  \end{equation}

  where $\delta$ is the cavity detuning, $\Omega$ is signal frequency, $\gamma$
  is cavity linewidth, $I_0$ is the power incident on the cavity, $T_I$ is input
  mirror power transmissivity, $c$ is the speed of light in the cavity, and
  $\lambda_0$ is the laser wavelength. The condition for ponderomotive rigidity
  to dominate the mechanical restoring force is $K>>\mu \Omega_0^2$, where our
  mechanical mass is $\mu\approx 1 g$ and cantilever resonance is $\Omega_0
  \approx 25 \mathrm{Hz}$. For $I_0\approx 15$ mW, $T_I\approx 330$ ppm, and
  $\gamma\approx 100$ kHz, the optical spring starts to dominate mechanical
  restoring force when $\delta > 23 Hz$, or about $0.02\%$ of the full range of
  the PDH error signal for the unlocked cavity sweeping across resonance.

  Marquardt et al have studied analytically the dynamical behavior of a resonant,
  high-finesse optical cavity with one compliant mirror, which suggests
  an interesting direction for future experimental work
  \cite{CantileverAttractors}.


  
  \subsection{Pump-Probe Phaselock}

  We can feed back the DFD error signal to the probe laser to stabilize the pump-probe
  beat frequency, as in Fig. \ref{fig:controlDiagram}. The frequency locked loop
  (FLL) alone (PLL gain of 0) stabilizes the beat note with $\approx 356
  \mathrm{Hz}$ unity gain frequency.

  % TODO: add UGF or loop TF for PLL-FLL

  % TODO: what limits FLL noise?

  % TODO: add TFs from elog 3159 -- these use EOAM and EOM, not sure if
  % relevant?  Does have loop diagram and filters.

  % \begin{figure}[hbt!]
  %   \centering
  %  %   \includegraphics[width=\textwidth]{}
  %   \caption[Pump-Probe FLL Transfer Functions]{Open loop transfer function for pump-probe beat note FLL}
  %   \label{fig:FLL_OLTF}
  %   \index{figure}
  % \end{figure}

  To further stabilize the beat note phase, we also lock the FLL-stabilized
  pump-probe beat note to a digital oscillator generated by a Moku:Pro via
  phase-locked loop (PLL). We choose the digital LO frequency to be close to the
  DFD null frequency, but higher than the PDH sideband frequency to avoid
  contaminating the cavity lock. Because the PLL mixer is driven by an
  independent LO, its demodulated output is proportional to beat note phase
  rather than frequency. The phase is related to frequency by a single pole at
  $0$ Hz in the Laplace domain, and the PLL sensor has the transfer function

  \begin{equation}
    P_\mathrm{PLL}(s) = \frac{P_\mathrm{PLL,0}}{s}.
  \end{equation}

  We sum the pump-probe PLL and FLL control signals to simplify control filter
  design. The overall loop has up to $\approx 200$ kHz UGF, though with
  the cavity locked we usually operate with closer to $75$ kHz UGF. The loop's
  response is dominated by the PLL sensor throughout the control band. The FLL only
  prevents the loop from losing lock during phase deviations that exceed the
  linear range of the PLL plant. The open loop transfer function is

  \begin{equation}
    G_\mathrm{beat} = A(H_\mathrm{FLL}P_\mathrm{FLL} + \frac{H_\mathrm{PLL}P_\mathrm{PLL}}{s}).
  \end{equation}
  

%  \begin{figure}[hbt!]
%    \centering
%    % \includegraphics[width=\textwidth]{}
%    \caption[Pump-Probe Phaselock Loop]{Control loop for pump-probe beat note phase lock}
%    \label{fig:beat_loop}
%    \index{figure}
%  \end{figure}


%  \begin{figure}[hbt!]
%    \centering
%    % \includegraphics[width=\textwidth]{}
%    \caption[Pump-Probe Phaselock Transfer Functions]{Open loop transfer function of pump-probe beat note PLL+FLL}
%    \label{fig:PLL_OLTF}
%    \index{figure}
%  \end{figure}

  \begin{figure}[h!]
    \centering
    \includegraphics[width=\textwidth]{Figures/PLL_OLTF_mod.pdf}
    \caption[PLL Transfer Functions]{Open loop transfer function (OLTF) of the
      pump-probe PLL, with cavity unlocked and no FLL engaged
      ($H_\mathrm{FLL}=0$).}
    \label{fig:PLL_OLTF}
    \index{figures}
  \end{figure}

  %TODO: add PLL transfer functions from elog 3005. 

  \section{Quadrature Sensing}\label{quads}

  To fully characterize the cavity's 2-quadrature transfer function, we need to
  independently sense the pump's AM and PM sidebands both before and after the
  cavity. The pump-probe beat note provides an independent measure of pump frequency
  modulations, so stabilizing the beat note by PLL lets us sense pump PM as long
  as we record the beat note either immediately before or immediately
  after the cavity. In particular, we cannot use the beat note on BEAT PD to
  sense pump PM entering the cavity in conjunction with the probe laser-based
  signal injection scheme described in Section \ref{sigInj}, since BEAT PD mixes pump and probe
  with a different phase than that set on BS1. On the other hand, for PM signals
  injected by directly modulating pump laser current, BEAT PD is a good sensor
  of pump PM entering the cavity, since the phase relation of the pump with the
  probe's resonant sideband is irrelevant to the injected signal.

  % TODO: add alternative quadrature sensing schemes
  
  \section{Coherent Signal Injection}\label{sigInj}

  \begin{figure}[h!]
    \centering
    \includegraphics[width=\textwidth]{Figures/PSOMA_layout.pdf}
    \caption[PSOMA Control System Diagram]{Control System Diagram}
    \label{fig:controlDiagram}
    \index{figures}
  \end{figure}

%  \begin{figure}[hbt!]
%    \centering
%    % \includegraphics[width=.3\textwidth]{}
%    \caption[Probe Laser Driver Transfer Functions]{Transfer functions from excitation voltage applied to the probe
 %     laser to frequency or amplitude noise relative to the pump laser}
 %   \label{fig:signalInjectionTF}
 %   \index{figures}
 % \end{figure}

  PSOMA is designed to amplify quantum noise limited signals on squeezed vacuum
  fields by taking advantage of the Mach-Zehnder interferometer's common mode
  rejection of fields entering PSOMA's pump port. We therefore need a strategy for phaselocking the probe field entering
  PSOMA's input port to the PSOMA pump, and generating signal sidebands around
  that probe field. The basic problem is analogous in many ways to the control of LIGO's squeezed
  injection field \cite{quantum_GWIFO} \cite{CLF_squeezing} \cite{CLF_broadband}
  \cite{CLF_twoLasers} and filter cavity
  for frequency-depending squeezing \cite{RLF_squeezing}. We use a coherent
  locking field (CLF) copropagating with the pump but outside PSOMA's amplified
  band as a surrogate for the amplified quadrature angle (the phase of signal
  sidebands around the probe carrier relative to the pump). For design
  flexibility, we use an auxiliary laser to generate a CLF. Future work should
  consider generating the CLF with two AOMs, to eliminate noise associated with
  the PLL.

  % TODO: add description of why choosing to stabilize beat on REFL instead of
  %BEAT, see elog 3165

  % TODO: this worked in elog 3153, 3027 has trnasfer functions?

  % TODO: lock-in setup in elog 3094
  % TODO: other schemes in elog 3011
 
%  \section{Seismic Noise Suppression}
%  
%  \begin{figure}[hbt!]
%    \centering
%    % \includegraphics[width=.3\textwidth]{}
%    \caption[Mechanical Control Diagram]{Mechanical isolation and control system}
%    \label{fig:seismicLayout}   %
%    \index{figure}
%  \end{figure}
  

%  \begin{figure}[hbt!]
%    \centering
%    % \includegraphics[width=.3\textwidth]{}
%    \caption[Seismic Noise PSD]{Seismic noise spectral density measured at the optical table, with and
%      without feedback suppression.
%      table}\label{PSD_seismic}
%    \index{figure}
%  \end{figure}

%  \subsection{Shadow sensor}

%  To damp the cantilever motion while the cavity is unlocked, we require an
%  additional sensor. Due to the low transmissivity of cavity optics and
%  cantilever's position at MC2, we decided to use an auxiliary beam for this
%  sensor. We routed a HeNe laser in and out of the chamber such that about half
%  the HeNe beam is clipped by the edge of the silicon cantilever. As the
%  cantilever vibrates, the fraction of HeNe beam occluded by the cantilever
%  (the size of the cantilever's ``shadow'') changes,
%  modulating the power of the HeNe beam exiting the chamber. 

%  By feeding back the shadow sensor signal to a piezo pushing the cantilever's
%  clamping block, we damped the residual cantilever motion sensed by the shadow
%  sensor by almost a factor of 10 (to a noise floor likely due to the vibration
%  of other optics along the HeNe beam path).

%  \begin{figure}[hbt!]
%    \centering
%    \includegraphics[width=\textwidth]{Figures/shadow_damp.png}
%    \caption[Seismic Feedback Performance]{Noise reduction on the shadow sensor (X1:OMA-OPLEV\_PIT\_OUT) output
%      with (blue) and without (black) feedback to the piezo.
%      X1:OMA-OPLEV\_SEIS\_OUT shows the seismic noise measured by a Wilcoxon
%      accelerometer, not normalized to the same units as the shadow sensor.}
%    \label{shadowSensor}
%    \index{figure}
%  \end{figure}
% TODO:  We measured the transfer function of the shadow sensor damping loop.

%  \begin{figure}[hbt!]
%    \centering
%    %\includegraphics[width=.3\textwidth]{}
%    \caption[Shadow Sensor Transfer Function]{Shadow sensing damping loop transfer functions}
%    \index{figure}
%  \end{figure}

  \section{Closed System Model}\label{controlModel}

  % TODO: series of elogs on the analytic analysis around elog 3065

  In order to understand our measurements of the multiple-input multiple-output
  transfer functions, we made a Finesse3 model of our experiment as it exists on
  the tabletop.

  The model implements all core optics, including ring cavity, Mach-Zehnder
  input and output beamsplitters, pickoffs, fiber beat note monitor, and
  REFL/TRANS/PMON photodiodes. The key parameters are realistic, including all
  transmissivities, cavity and Mach-Zehnder arm lengths, and cavity losses. We
  implement realistic and dimensionful control filters for the PDH and PLL
  control loops and manually ``lock'' the model by maximizing signals that
  indicate lock (such as cavity transmission and beat note demodulation
  Q-quadrature). We use the model the calculate the closed loop transfer
  functions that are sufficient to estimate open loop transfer function of the
  cavity (including both amplitude and phase quadratures).

  The model does not contemplate several auxiliary control loops, including slow
  laser temperature control and mechanical damping of the cantilever. Cantilever
  damping should be considered in future versions of the numerical model. We
  also leave to future work modeling of the coherent signal injection scheme
  described above.

  \begin{figure}[h!]
    \centering
    \includegraphics[width=.8\textwidth]{Figures/kat_PDHsweep.pdf}
    \caption[Finesse3 Model of PDH Sweep]{PDH sweep simulated in Finesse 3. Top: DC signals on transmission,
      reflection, and pump monitor photodiodes while sweeping pump frequency
      relative to cavity resonance. Bottom: PDH error signal in I and Q
      quadratures while sweeping pump frequency relative to cavity resonance.}
    \label{fig:kat_PDHsweep}
    \index{figures}
  \end{figure}

  \begin{figure}[h!]
    \centering
    \includegraphics[width=.8\textwidth]{Figures/kat_PDH_FR.pdf}
    \caption[Finesse3 Model of PDH Loop Bode Plot]{Bode plots of PDH loop in
      Finesse3 model. Excitation is summed at the PDH error point, with In1
      just before and In2 just after the summation point. OLTF, Controller, and
      Plant are the open loop transfer functions.}
    \label{fig:kat_PDHbode}
    \index{figures}
  \end{figure}

  \begin{figure}[h!]
    \centering
    \includegraphics[width=.8\textwidth]{Figures/kat_PLL_FR.pdf}
    \caption[Finesse3 Model of PLL Bode Plot]{Bode plots of the phase-locked
      loop stabilizing pump-probe beat frequency on REFL photodiode. Excitation
      is summed at the PLL error point, with In1 just before and In2 just after
      the summation point. OLTF, Controller, and Plant are open loop transfer
      function.}
    \label{fig:kat_PLLbode}
    \index{figures}
  \end{figure}

  \begin{figure}[h!]
    \centering
    \includegraphics[width=0.8\textwidth]{Figures/cavTFs_newData.pdf}
    %\includegraphics[width=0.45\textwidth]{Figures/kat_Cavity_FM_to_FM.pdf}
    %\hfill
    %\includegraphics[width=0.45\textwidth]{Figures/kat_Cavity_AM_to_FM.pdf}
    \\[\smallskipamount]
    %\includegraphics[width=0.45\textwidth]{Figures/kat_Cavity_FM_to_AM.pdf}
    %\hfill
    %\includegraphics[width=0.45\textwidth]{Figures/kat_Cavity_AM_to_AM.pdf}
    \caption[Cavity Transfer Function]{Finesse3 model and measured tansfer functions
      of optical amplitude and frequency across the cavity, derived from the PDH and
      PLL transfer functions in \ref{fig:kat_PLLbode} and
      \ref{fig:kat_PDHbode}. Note that there is some disagreement due to
      unmodeled FM-to-AM coupling, and possibly due to lack of measurement
      coherence (transparent points have less coherence) especially near the
      mechanical resonance. FM input is taken as the pump-probe PLL at BEAT PD;
      FM output is taken as the PDH control signal referred to frequency; AM
      input is measured at Pump MON PD; AM output is measured at TRANS MON.
      Note, too, that the VCO frequency on TRANS independently monitors FM out,
      and after correcting for PLL-suppression would not turn upwards as in the
      rightmost plot.}
    \label{fig:kat_cavBode}
    \index{figures}
  \end{figure}

  % TODO: add signal transfer functions, eg in 3095? I think these are in-air
  % newer measurements elog 3228

  \subsection{Improving model fidelity}

  The model is currently sufficient to validate the amplitude- and phase- cavity
  transfer functions to lowest order. However, it could be improved in several
  ways.

  Finesse3 defaults to using single-sided mirrors, rather than mirrors with both
  HR and AR surfaces. Implementing two-sided mirrors would be useful especially
  as the model is used for studying scatter and the effects of higher order
  modes.

  We should also implement realistic path lengths, mode matching optics, and
  optical losses outside of the cavity. The numerical model should be useful for
  understanding how higher order modes affect our measurements, for example by
  biasing the PDH error point offset.

  % TODO: finish summarizing elog 3222

  % TODO: mention autolock loops? elog 3102, 3101

%  \printbibliography[heading=subbibliography]
  

%\end{refsection}