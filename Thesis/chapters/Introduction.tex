%\begin{refsection}
  
\chapter{Introduction}

\section{What We Measure}

%TODO: this is supposed to be about measuring fundamental properties of
%spacetime, towards quantum mechanical systems that gravitate

Measurement is the process by which physicists test the ability of a physical
model to predict the behavior of the universe, or at least the system or
laboratory under study. We typically want to reduce our uncertainty about a
model parameter or reject a hypothesis using as few resources (time, energy,
money) as possible.

Optomechanical measurement uses photons to measure the distance between atoms,
and sometimes the rate of change of that distance. Optomechanics advances
our understanding of physics across disparate length scales.

At large scales, the direct detection of gravitational waves \cite{LIGO_news}
confirmed one of the major experimental
predictions of Einstein's General Theory of Relativity \cite{GW_physics}. The next generation of
gravitational wave (GW \nomenclature{GW}{Gravitational Wave. Traveling-wave
  solutions of the field equations of General Relativity with amplitude decaying
with distance $r$ from the source as $1/r$}) detectors \cite{GWIFO_nextGen} aims to deepen our understanding of GW signals
from compact binary systems, as well as expand our access to the remaining GW
spectrum at low and high frequency \cite{HFGW_Aggarwal_review}
\cite{HFGW_TobarBAW}.

At small scales, measurements to date imply that all physical systems behave
quantum mechanically \cite{Bell_eSpinMeasure} \cite{Bell_optomech}. One burning question of modern physics is how the apparently locally real behavior of
gravitationally dominated systems arises in a fundamentally quantum universe.
Increasingly, increasingly macroscopic optomechanical experiments aim to probe the boundary
between our quantum and classical descriptions \cite{Yanbei_review}
\cite{Haixing_thesis}.

%Whether
%quantum measurement is defined by postulate \cite{QM_postules_withMeasure} or
%derived from unitarity \cite{QM_postulates_noMeasure}, it is clear that. 

\section{Limits to Measurement}

%TODO: This is supposed to be about measurement theory and similarities and
%differences between classical and quantum measurement

\subsection{Q and Quantum}
Braginsky \emph{et al.} describe why linear oscillators with small dissipation
make excellent force sensors \cite{Braginsky_smallDissipation}
\cite{Braginsky_weakForces}. They consider an oscillator with mass $M$,
frequency $\omega$, and quality factor $Q$. Treated classically, the noise limiting the
measurement of oscillator amplitude due to a force acting at $\omega$ for duration
$\hat{\tau}$ is a thermally induced amplitude change that decreases with
oscillator quality factor $Q$.

\begin{equation}
  \Delta x_T\approx \sqrt{\frac{kT\hat{\tau}}{M\omega Q}}
\end{equation}

With sufficiently high Q, the thermal noise of an oscillator in a coherent state
may be reduced below the size of the oscillator's wavepacket
\cite{QND_harmonicOscillators}.

\subsection{Gravitational Wave Interferometers}

The quantum limits of measurements made with oscillators in coherent states are
now being reached and exceeded by gravitational wave interferometers
\cite{LIGO_FDsqueeze}. For traditional interferometry, our null measurement is
of a particular photon vacuum state, and we devise our system such that
classical information can be continuously imparted on the null state. We can
improve our measurements by choosing a more optimal null state, and by
protecting the null state from optical and mechanical losses along the entire
measurement chain. The possibility of reducing quantum noise by an appropriate
choice of observables or even with quantum error correction
\cite{QN_Heisenberg_QEC} demonstrates that gravitational wave interferometers are
still fundamentally limited by Q rather than quantum mechanics.
% TODO: consider including others cited on page 7 of systems with small
% dissipation
% also Caves, BnC, etc

To reduce the noise in gravitational wave detectors \cite{Rana_review}, it is beneficial to have many photons all in the
same state, as in a high power laser stabilized against some
conveniently chosen quantum state; and to measure the average position of
many atoms that are not moving much, as in heavy mirrors of high quality. The inverse relationship between quantum noise spectral density $S_\mathrm{PP}$
and measurement sensitivity $S_\mathrm{FQL}$ is capture in the so-called fundamental or energetic quantum limit, also
known as the quantum Cramer-Rao
bound, which can be formulated for laser interferometric GW detectors as \cite{aIFO_review}

\begin{equation}
  S_\mathrm{FQL}^h(\Omega) = \frac{\hbar^2c^2}{S_{PP}(\Omega)L^2}=\frac{
    4 \hbar^2
  }{S_{\mathcal{E}\mathcal{E}}(\Omega)}.
\end{equation}

Stated simply, precise measurements of spacetime require tight control of a
system in the face of large energy fluctuations $S_{\mathcal{E}\mathcal{E}}$. A
more thorough exploration of measurement precision limits in the language of
control theory is provided in \cite{control_FDT}. Braginsky, Mitrofanov, and
Panov \cite{Braginsky_smallDissipation} formulated this limit as a maximum
observable quality factor due to the effect of the measurement process.




%\section{Types of Loss}

%TODO: This I'll probably cut combine with the previous section

%\subsection{optical}

%Photons absorption, scatter, mode mismatch

%\subsection{mechanical}

%Nonlinearity in phonon system leads to thermal noise. Akhiezer, thermoelastic, etc

%\subsection{Quantum Noise}

%(Caves, BnC, etc)

\section{Contributions}

This work is divided into three parts covering the theory of optomechanical
interferometry in Part \ref{Ponder}; experimental and analytical techniques in
silicon optomechanics in Part \ref{SiMech}; and an early stage tabletop
interferometry experiment, motivated and enabled by the previous parts, in Part
\ref{Preamp}. The appendices discuss some
measurement techniques and topics of interest that were only briefly explored,
but may be of use to someone beginning a deeper study. I apologize for the many pedagogical
deficiencies, and encourage the reader attempting to apply or replicate this work to request
clarification if required.

Chapter \ref{optomech}, on ponderomotive interferometry, is largely a review. If I have
a novel contribution, it is the explicit expression for the input-output relation
of a mechanically responsive
beamsplitter pumped by counterpropagating lasers incident on the same side of the optic
(which surely others have derived but not seen fit to publish, to my knowledge).

The work in Chapter \ref{PSOMA} is a summary of our group's proposal for a new quantum
measurement widget, the phase-sensitive optomechanical amplifier (PSOMA
\nomenclature{PSOMA}{Phase-Sensitive OptoMechanical Amplifier. A Mach-Zehnder
  interferometer modified with identical ring cavities in each arm and
  parameters chosen to enhance optomechanical gain for signals transmitted
  across the differential ports in one direction.}) \cite{PSOMA}.
I contributed to conceptual development and performed numerical simulations and optimization in
Optickle and Finesse to validate our analytic results.

I developed a strong interest in generalizing PSOMA, and describe the outcome in Chapter
\ref{QIFO}. I propose and simulate signal- and power-recycled Mach-Zehnder interferometers operated in transmission and/or reflection, as an internal
quantum filter-squeezer for Michelson-based gravitational wave interferometers,
or as independent resonant strain sensors. To my knowledge this is the first
discussion in the literature of dual recycled Mach-Zehnder interferometers and
the utility of the interferometer's microscopic position \textit{within} the
otherwise fixed recycling cavities. I have not exhaustively explored this
device, but attempt to connect it to existing work in quantum coherent
interferometry and optomechanical trapping and cooling such that natural
extensions become apparent. I am grateful to many colleagues for letting me
distract them with this idea and helping me understand the system, including
Shruti Maliakal, Kevin Kuns, Xiang Li, James Gardner, Chris Wipf, Yanbei Chen,
and Rana Adhikari.

The central technologies enabling quantum coherent laser
interferometry in the lab are high stability light and high quality mechanics,
and as much of it as desired. The experimenter should clamp down their mechanics
before turning on the laser,
so Chapter \ref{SiFab} describes my experimental work fabricating cm- and gram-scale Si mechanical
oscillators for use as optomechanics. I implemented but did not invent the
fabrication techniques therein. I also carried out some small experiments,
including with Disha Kapasi on surface roughness following KOH etching, to
advance our group's understanding and application.

I used my Si mechanical oscillators in the cryogenic thin film
mechanical loss testbed described in Chapter \ref{cryoQ}. I was the primary
student who, along with postdoc Brittany Kamai, designed, operated, and analyzed the testbed and
our results. We applied the prior art of gentle nodal suspension to a liquid
nitrogen cryostat, demonstrating experimental limitations of some cryogenic
design choices for this mechanical isolation technique. I also used Nic Smith's
clever method for constant-amplitude quality factor measurement
\cite{Moderinger} to explore a new
technique for non-contact temperature sensing and control of a mechanical
oscillator.

Our cryogenic mechanical loss testbed inspired Anna Roche, Rana Adhikari, and me
to develop a novel analysis for material parameter estimation we call loss
tomography, described in Chapter \ref{tomo}. Loss tomography contributes to the
field of high Q mechanical loss measurement and material development by
eliminating systematic errors associated with sequential mechanical Q
measurement before and after test film deposition. I made the FEA models used for the analysis, and developed
the loss budget with Brittany Kamai. I independently completed the quick-and-dirty
demonstration at the end of the chapter.

In the future, we anticipate utilizing Si mechanical oscillators in our tabletop
PSOMA demonstration. Chapter \ref{Design} covers the detailed design of the early stage
experiment, and our targeted late-stage performance. Shruti Maliakal, Rana Adhikari, and Chris Wipf contributed to all
aspects of the design. I commissioned a new vacuum system, which I designed with
Stephen Appert. I used Shruti's Finesse-based mode matching optimization code to choose
an appropriate cavity geometry for the optics on hand. I constructed and
characterized the fiber
and free-space optical layout, with sanity-preserving assistance at various
times from Shruti Maliakal, Mayank Chaturvedi, Yuta Michimura, Francisco (Paco) Salces
Carcoba, Chris Whittle, Jeff Wack, and Jancarlo Sanchez.

Mayank Chaturvedi, Shruti Maliakal, Chris Wipf, Rana Adhikari, and I developed
the control system characterized in Chapter \ref{Control}, which has some novelty even if mostly due to the inherent
specificity of control systems. Our signal injection strategy is reminiscent though not
identical to the coherent locking technique used in GW and related
interferometers to implement frequency-dependent optical
squeezing. We also develop a new application of multiple phase-locked loops
to measure the full 2-quadrature transfer function of a
Pound-Drever-Hall locked cavity. I briefly discuss my proposal for using a
Mach-Zehnder interferometer to reduce pump phase noise coupling in this
scheme.

Finally, Chapter \ref{Budget} presents a characterization and noise budget of
the PSOMA demonstration in its current form. Shruti Maliakal and I developed the
noise budget. I contributed the most sensitive measurement to date of the low
frequency phase noise of TeraXion's PureSpectrum laser module. Mayank
Chaturvedi, Chris Whittle, and Jeff Wack were invaluable collaborators on several of the transfer function
measurements presented in this chapter.

%\printbibliography[heading=subbibliography]

%\end{refsection}

%%% Local Variables:
%%% mode: latex
%%% TeX-master: "../main"
%%% End: