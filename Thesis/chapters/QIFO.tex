
%\begin{refsection}

  \chapter{Quantum Coherent Interferometry} \label{QIFO}

  Careful and fully phase-sensitive design can improve the performance of
  optomechanical sensors. This chapter explores how the additional
  interferometric degrees of freedom afforded by Mach-Zehnder interferometers
  can be used to benefit gravitational wave detection. We apply some ideas from
  advanced GWIFO design to generalize the device from Chapter \ref{PSOMA}, and
  explore some new ways the generalized traveling-wave optomechanical amplifiers
  can be used for quantum coherent interferometric sensing.
  
  Advanced interferometers and optomechanical configurations for optimal
  quantum noise performance are discussed in \cite{TsangCaves_QNcancellation} \cite{quantum_FCBWoptomech}
  \cite{QN_squeezedVariational} \cite{QN_Haixing_unstable}
  \cite{CoherentCoupling}. PT symmetric interferometers are discussed in a
  series of papers including \cite{QN_fullPTsym} \cite{PTsymmetry_coherentQFB}.
 
  Using quantum mechanical systems for measurement is intimitely related to the
  field of quantum computing and quantum information processing
  \cite{QN_Heisenberg_QEC} \cite{QN_entangledCombs} \cite{QN_CVcompute_combs}
  \cite{QN_CVQEC} \cite{QN_encodingOscillators} \cite{QN_generateGKP}
  \cite{QN_entangledGraphComb} \cite{QN_quadripartiteEntangleComb}
  \cite{QEC_gridQubit} \cite{QN_singleModeDispSensor} \cite{QN_stroboscopicOptomech}, where linear
  Gaussian optics are powerful tools for Gaussian quantum information processing
  \cite{quantum_Gaussian}.

  \section{Power- and Arm-Recycled Mach-Zehnder Interferometers (PARMZI)}

  Power recycling is well understood for Michelson interferometers, where for
  example GWIFOs introduce a normal-incidence mirror to the Michelson's bright
  port to enhance the buildup of optical power in the interferometer. In the
  Mach-Zehnder interferometer (MZI), power recycling requires normal incidence mirrors
  on each of the two ``bright'' ports of the MZI. As in Michelson
  interferometers, power recycling leads to resonant gain of the pump laser
  field, since we have essentially placed the MZI inside a resonant Fabry-Perot
  cavity (PRC). However, we have also
  introduced an additional degree of freedom characterizing the microscopic
  position of the MZI inside the PRC. From the perspective of the MZI input BS,
  the pump phase exiting the MZI can be chosen independent of the pump phase
  entering the MZI while holding the PRC on resonance.

  The configuration for
  PARMZI is close to that in Figure \ref{fig:DRrings}, but without the
  normal-incidence signal (differential) port mirrors.
  
  \subsection{Input-Output Relations for the Light Mirror}

  The input-output relations of PARMZI are similar to PSOMA for the
  forward-propagating signal field, but modifying $P_\mathrm{circ}\to g_P P_\mathrm{circ}$ to
  account for optical gain $g_P$ in the PRC. However, the reverse propagating
  pump allows reverse propagating probe fields to drive the cantilever, and all
  cantilever motion will generate sidebands on the reverse propagating pump
  field.

  First, consider the input-output relations of just the movable mirrors, which
  are beamsplitters with near-zero transmissivity $\tau\to 0$ and mass $m$. 
  Adapting Eq. \ref{eq:BS_IO} and Fig. \ref{fig:basicPorts}, we can
  reduce to two pumping fields with power $I_1,I_2$ and phases $\theta_1,
  \theta_2$ incident at $a_1$ and $a_2$, respectively. It will turn out that
  even when solvable there is no clean analytic expression for the general
  input-output relations, so for clarity we will ignore optical loss and set
  $\rho=0$. In some of the numerical analysis presented later, optical losses and finite
  mirror transmissivity will be included to demonstrate their limited effect on
  the conceptual results.
  
  \begin{equation}\label{eq:cantBS_IO}
    \begin{split}
      & [\mathbb{I} - \frac{\Pi}{2}
      \begin{pmatrix}
        D_{\hat{a}_1}^* \\ D_{\hat{a}_2}^*
      \end{pmatrix}
      \begin{pmatrix}
        D_{\hat{a}_1}^T & D_{\hat{a}_2}^T
      \end{pmatrix}
      ]
      \begin{pmatrix}
        \hat{b}_1 \\ \hat{b}_2
      \end{pmatrix} \\
      & =
      -[\mathbb{I} + \frac{\Pi}{2}
      \begin{pmatrix}
        D_{\hat{a}_1}^* \\ D_{\hat{a}_2}^*
      \end{pmatrix}
      \begin{pmatrix}
        D_{\hat{a}_1}^T & D_{\hat{a}_2}^T
      \end{pmatrix}
      ]
      \begin{pmatrix}
        \hat{a}_1 \\ \hat{a}_2
      \end{pmatrix}
    \end{split}
  \end{equation}

  To solve for the output fields, we need to invert the matrix on the left-hand
  side (LHS).

  \begin{equation}
    \Xi\equiv
    \mathbb{I} - \frac{\Pi}{2}
    \begin{pmatrix}
      D_{\hat{a}_1}^* \\ D_{\hat{a}_2}^*
    \end{pmatrix}
    \begin{pmatrix}
      D_{\hat{a}_1}^T & D_{\hat{a}_2}^T
    \end{pmatrix}
  \end{equation}

  $\Xi$ has four eigenvectors with eigenvalue 1. Note that unlike the case
  well studied in interferometric speedmeters
  which pump the beamsplitter from both sides of the HR surface \cite{IFO_math}, no additional
  optomechanical resonance arises in our case. One set of eigenvectors is

  \begin{equation}
    \{\xi_i\} = 
    \{
    \begin{pmatrix}
      D_{\hat{a}_1}^* \\ 0
    \end{pmatrix},
    \begin{pmatrix}
      0 \\ D_{\hat{a}_2}^*
    \end{pmatrix},
    \begin{pmatrix}
      D_\mathrm{\hat{a}_2} \\ - D_{\mathrm{\hat{a}_1}}
    \end{pmatrix}
    \begin{pmatrix}
      D_{\hat{a}_1} \\ -D_{\hat{a}_2}
    \end{pmatrix}
    \}.
  \end{equation}

  One can find

  \begin{equation}
    \Xi^{-1} =
    \mathbb{I} + \frac{\Pi}{2}
    \begin{pmatrix}
      D_{\hat{a}_1}^* \\ D_{\hat{a}_2}^*
    \end{pmatrix}
    \begin{pmatrix}
      D_{\hat{a}_1}^T & D_{\hat{a}_2}^T
    \end{pmatrix}.
  \end{equation}

  Conveniently, this leads to a simple expression for the input-output relations
  for the cantilever mirror

  \begin{equation}\label{eq:MC2_IO}
    \begin{split}
      \begin{pmatrix} \hat{b}_1 \\ \hat{b}_2 \end{pmatrix}
      & =
      -(\Xi^{-1})^2\begin{pmatrix} \hat{a}_1 \\ \hat{a}_2 \end{pmatrix} \\
      & =  - (\mathbb{I} + \Pi
        \begin{pmatrix}
          M_{11} & M_{21} \\
          M_{12} & M_{22}
        \end{pmatrix}
        )
        \begin{pmatrix} \hat{a}_1 \\ \hat{a}_2 \end{pmatrix}.
    \end{split}
  \end{equation}

  The matrices $M_{ij}$ indicate the ponderomotive part of the transfer matrix
  from $\hat{a}_i$ to $\hat{b}_j$.

  \begin{equation}\label{eq:Mij}
    M_{ij} \equiv 2 \sqrt{I_iI_j}
    \begin{pmatrix}
      \cos\theta_i \sin\theta_j & \sin\theta_i \sin\theta_j \\
      -\cos\theta_i\cos\theta_j & -\sin\theta_i\cos\theta_j
    \end{pmatrix}
  \end{equation}

  \subsection{Input-Output Relations for the Full System}\label{PARMZI_IO}

  \begin{figure}[hbt!]
    \centering
    \includegraphics[width=0.5\textwidth]{Figures/cavityNodes.pdf}
    \caption[Triangle Cavity Input-Output]{Triangular cavity labelled with input and output ports propagating
      in both directions.}
    \label{fig:MCnodes}
    \index{figures}
  \end{figure}
  
  The input-output relations for lossless ring cavity assuming the additional two mirrors
  are fixed (or very massive) can now be expressed based on \ref{fig:MCnodes}
  and \ref{eq:MC2_IO} as

  \begin{equation}
    \begin{pmatrix}
      \hat{b}_1 \\ \hat{b}_2 \\ \hat{b}_5 \\ \hat{b}_6 \\ \hat{b}_7 \\ \hat{b}_8 \\
      \hat{a}_1 \\ \hat{a}_2 \\ \hat{a}_7 \\ \hat{a}_8
    \end{pmatrix}
    =
    \begin{pmatrix}
      -\mathbb{I} - \Pi M_{11} & -\Pi M_{21}  & 0 & 0 & 0 & 0 & 0 & 0 & 0 & 0 \\
      -\Pi M_{12} & -\mathbb{I} - \Pi M_{22} & 0 & 0 & 0 & 0 & 0 & 0 & 0 & 0 \\
      0 & 0 & \rho & 0 & 0 & \tau & 0 & 0 & 0 & 0 \\
      0 & 0 & 0 & \rho & \tau & 0 & 0 & 0 & 0 & 0 \\
      0 & 0 & 0 & \tau & -\rho & 0 & 0 & 0 & 0 & 0 \\
      0 & 0 & \tau & 0 & 0 & -\rho & 0 & 0 & 0 & 0 \\
      0 & 0 & 0 & 0 & 0 & 0 & 0 & 0 & 0 & e^{i \Omega l_1 / c} \\
      0 & 0 & 0 & 0 & 0 & 0 & 0 & 0 & e^{i \Omega l_2 / c} & 0 \\
      0 & 0 & 0 & 0 & 0 & 0 & 0 & e^{i \Omega l_1 / c} & 0 & 0 \\
      0 & 0 & 0 & 0 & 0 & 0 & e^{i \Omega l_2 / c} & 0 & 0 & 0 
    \end{pmatrix}
    \begin{pmatrix}
      \hat{a}_1 \\ \hat{a}_2 \\ \hat{a}_5 \\ \hat{a}_6 \\ \hat{a}_7 \\ \hat{a}_8 \\
      \hat{b}_1 \\ \hat{b}_2 \\ \hat{b}_7 \\ \hat{b}_8
    \end{pmatrix}.
  \end{equation}

  We can directly invert the above adjacency matrix, or reduce it to

  \begin{equation}
    \begin{split}
    \begin{pmatrix}
      \hat{b}_5 \\ \hat{b}_6
    \end{pmatrix}
    & =
    \begin{pmatrix}
      \rho & 0 \\
      0 & \rho
    \end{pmatrix}
    \begin{pmatrix} \hat{a}_5 \\ \hat{a}_6 \end{pmatrix} +
    \begin{pmatrix}
      0 & \tau \\
      \tau & 0
    \end{pmatrix}
    \begin{pmatrix}
      0 & e^{i \Omega l_1 /c} \\
      e^{i \Omega l_2 / c} & 0
    \end{pmatrix}
    [-\mathbb{I}-\Pi M]
    \begin{pmatrix}
      0 & e^{i \Omega l_1 / c} \\
      e^{i \Omega l_2 / c} & 0
    \end{pmatrix} \\
    & * [\begin{pmatrix}
       0 & \tau \\
       \tau & 0
     \end{pmatrix}
     \begin{pmatrix} \hat{a}_5 \\ \hat{a}_6 \end{pmatrix} +
     \begin{pmatrix}
       -\rho & 0 \\
       0 & -\rho
     \end{pmatrix}
     \begin{pmatrix}
       \hat{a}_7 \\ \hat{a}_8
     \end{pmatrix}
      ].
    \end{split}
  \end{equation}

  Using the relations at the the input beamsplitter and simplifying, we can
  recast this as

  \begin{equation}
    [\mathbb{I} - \rho e^{i \Omega L / c}(\mathbb{I}+\Pi M)]
    \begin{pmatrix}
      \hat{b}_5 \\
      \hat{b}_6
    \end{pmatrix}
    =
    [\rho \mathbb{I} -
    \tau^2 e^{i\Omega L / c}(\mathbb{I}+\Pi M) -
    \rho^2 e^{i \Omega L / c} (\mathbb{I} + \Pi M)]
    \begin{pmatrix}
      \hat{a}_5 \\
      \hat{a}_6
    \end{pmatrix}.
  \end{equation}

  In the lossless case, we can express the input-output relations explicitly as

  \begin{equation}
    \begin{pmatrix}
      \hat{b}_5 \\ \hat{b}_6
    \end{pmatrix}
    =
    \Upsilon
    \begin{pmatrix} \hat{a}_5 \\ \hat{a}_6 \end{pmatrix}
  \end{equation}

  \begin{equation}\label{eq:QIFOcav_IO}
    \begin{split}
      \Upsilon\equiv
      & -e^{i2\eta}
        \begin{pmatrix}
          \mathbb{I} + \mathcal{K}_{11} & \mathcal{K}_{21} \\
          \mathcal{K}_{12} & \mathbb{I} + \mathcal{K}_{22}
        \end{pmatrix} \\
      e^{i2\eta}\equiv
      & \frac{e^{i\Omega L/c}-\rho}{1-\rho e^{i\Omega L /c}} \\
      \mathcal{K}_{ij} \equiv
      & \frac{\tau^2}{1-2\rho \cos(\frac{\Omega L}{c}) + \rho^2} \Pi M_{ij}.
    \end{split}
  \end{equation}

  As usual, the Mach-Zehnder lets us treat the differential arm mode coupled
  to the signal (``dark'') ports separately from the common arm mode coupled
  to the pumped (``bright'') ports, and the expression in \ref{eq:QIFOcav_IO}
  holds for the differential and common modes separately.

  The expressions above
  will be modified slightly if we allow the other two cavity mirrors to be
  ponderomotively active (relaxing the assumption of large mass compared to the
  third mirror). We will explore this to some extent in numerical simulations, but
  will not derive the explicit expressions.

  \section{Signal-, Power-, and Arm-Recycled Mach-Zehnder Interferometers}

  To complete the analogy of a Mach-Zehnder interferometer to a dual-recycled
  Fabry-Perot Michelson interferometer, we can add normal incidence mirrors to
  both signal ports of PARMZI (as in Fig. \ref{fig:DRrings}). As was the case for power recycling, the signal recycling cavity has an
  additional degree of freedom relative to DRFPMI. In addition to overall signal
  cavity length detuning, which takes the interferometer from signal recycling
  to resonant sideband extraction, the relative phase of forward- and reverse-
  propagating signal fields is tunable.

  One choice of Mach-Zehnder and recycling cavity tunings establishes a system
  completely analogous to the Sagnac speedmeter with ring cavities considered by Chen
  \cite{Yanbei_SagnacSpeedmeter}. However, we will discover that the extra degrees
  of freedom can lead to an optical spring in the resonant differential arm
  cavity.

  Tuned to its dark fringe, the Mach-Zehnder interferometer lets us treat separately the signal recycling
  cavity coupled to differential arm motion and the power recycling cavity
  coupled to common arm motion. The simplified common and differential mode
  systems are represented in Fig. \ref{fig:DRrings}.

  \begin{figure}[h!]
    \centering
    \includegraphics[width=0.5\textwidth]{Figures/DRPSOMA_layout.pdf}
    \\[\smallskipamount]
    \includegraphics[width=0.5\textwidth]{Figures/recycled_rings.pdf}
    \caption[Recycled Mach-Zehnder Layout]{Recycled Mach-Zehnder layout. Top to bottom: SPARMZI
      layout, showing MZI tuning for distinct signal- and power- recycling (or
      extraction) cavities; decomposition into differential and common mode
      recycled ring cavities of SPARMZI. SM1 is the ``signal extraction mirror''
      with amplitude reflectivity and transmissivity $\rho_1, \tau_1$; SM2 is the
      ``signal recycling mirror'' with amplitude reflectivity and transmissivity
      $\rho_2, \tau_2$; ITM is the ``intermediate test mass'' with amplitude refectivity
      and transmissivity $\rho, \tau$; ETM is the ``end test mass'' with mass
      $m$.}
    \label{fig:DRrings}
    \index{figures}
  \end{figure}

  In a procedure nearly identical to section \ref{PARMZI_IO}, the input-output
  relations for SPARMZI tuned to the MZI dark fringe can be written as

  \begin{equation}\label{eq:SPARMZI_IO}
    \begin{split}
    [\mathbb{I} +
      \tau_e\tau_r e^{i\Omega L_\mathrm{SRC}/c}\Upsilon\begin{pmatrix} 0 & \rho_e \\ \rho_r & 0 \end{pmatrix}]
    & \begin{pmatrix} b_{fwd} \\ b_{rev} \end{pmatrix} = \\
    [\begin{pmatrix} 0 & \rho_2 \\ \rho_1 & 0 \end{pmatrix} +
      \tau_1\tau_2 e^{i\Omega L_\mathrm{SRC}/c}\Upsilon +
      e^{i\Omega L_\mathrm{SRC}/c}\begin{pmatrix}\rho_2^2 & 0 \\ 0 & \rho_1^2 \end{pmatrix} \Upsilon ]
      & \begin{pmatrix} a_{fwd} \\ a_{rev} \end{pmatrix}.
    \end{split}
  \end{equation}

  The LHS of Eq \ref{eq:SPARMZI_IO} has two unique eigenvalues, each with
  twofold degeneracy. Using the expressions from Eq \ref{eq:QIFOcav_IO} and
  \ref{eq:Mij}, these eigenvalues are

  \begin{equation}\label{eq:SPARMZI_lambda}
    \lambda_\pm = 1 \pm
    i \Pi \frac{\tau^2\sqrt{I_1I_2\rho_1\rho_2\sin 2\theta_1\sin 2\theta_2}}
    {(\rho e^{i L \Omega / c} - 1)^2}
    e^{i\Omega (L+L_\mathrm{SRC})/c}.
  \end{equation}

  Eq \ref{eq:SPARMZI_lambda} points to the existence of several optomechanical
  resonances. One is an optical spring associated with detuning the main arm
  cavities, represented by $L$. Even with the arm cavities on resonance, another
  optical spring arises from detuning the signal recycling cavity with
  $L_\mathrm{SRC}$. Yet even when $L_\mathrm{SRC}$ does not produce a resonance (for
  example, $e^{i\Omega(L+L_\mathrm{SRC})/c} = i$), the phases $\theta_1,
  \theta_2$ can establish an optomechanical resonance. The resonance set by
  $\theta_1,\theta_2$ is due to relative phase accumulation of the pump and signal
  fields on multiple passes of the mechanically susceptible optic (in this case,
  clockwise and counter-clockwise trips around the ring cavities). While
  finalizing this manuscript, the author learned that Khalili
  has independently derived and generalized the mechanism of the
  double-pass optical spring, and studied a system equivalent to Eq. \ref{eq:SPARMZI_IO} with
  $T_\mathrm{SEM}=1$ and $R_\mathrm{SRM}=0$, in
  \cite{Khalili_spring}.

  Though it is straightforward to write down the adjacency matrix for SPARMZI
  when the MZI is tuned to a dark fringe,
  the most general closed-form input-output relations are complicated. We
  explore this configuration numerically below.

  \section{Quantum Coherent GW Interferometers}

  Nonlinearity can be used to exceed the standard quantum limit
  for linear measurement of classical signals \cite{CramerRao_Belinda}
  \cite{quantumExpander}.

  Phase sensitive filtering and amplification, which treats both optical
  quadratures equally, can be useful for compensating the phase delay of
  resonant sensors without increasing sensitivity to intra-sensor optical losses
  \cite{optomech_frequencyConversion} \cite{Haixing_highFrequencyGWIFO} \cite{PhaseSensitive_tabletop}. A
  specific case of interferometers with phase-insensitive filtering has
  received significant attention for loss tolerant quantum noise cancellation by
  operating at an exceptional point or with a PT-symmetric Hamiltonian 
  \cite{PT_quantumCorrelations} \cite{PT_review} \cite{PT_OMIT}
  \cite{antiPT_sensing}. EPR entangled intererometers are discussed in
  \cite{GWIFO_EPR}.
  
  Non-reciprocal quantum sensing relies on an asymmetry between signal- and
  noise- coupling or forward- and reverse- propagation through the device to
  escape the typical tradeoff between signal gain and sensor isolation \cite{nonreciprocal_limits}
  \cite{nonHermitian_limits} \cite{nonrecip_qubit} \cite{nonrecip_exceptional}
  \cite{exceptional_ringGyro} \cite{nonrecip_reservoir}. $SU(1,1)$
  interferometry is discussed in
  \cite{Caves2019ReframingSI} \cite{SU11_adaptive}.

  \subsection{Phase-Sensitive Quantum Filters}\label{phaseGWIFO}

  In Fig. \ref{fig:QIFOcontrol}, we adapt the control system schematic from
  \cite{PhaseInsensitiveFilters} for the case of a phase sensitive filter (PSF). We
  can optimize $Y$ to maximize the signal to noise ratio of %38M
  $\hat{a}_\mathrm{out}$ for sensing $h$ subject to noise $\hat{n}_q$ entering
  only at the detector port. As long as the PSF obeys
  unitarity (as does PARMZI above), we do not
  need to add additional quantum noise at the amplifier as in the case of generic
  linear amplifiers.
  
  \begin{figure}[h!]
    \centering
    \includegraphics[width=\textwidth]{Figures/QIFO_controls.pdf}
    \caption[Control System Picture of Amplifier-Filter Applied to GWIFO]{Control system diagram of homodyne phase sensitive linear
      amplifier, in our example PARMZI, in the filter cavity typical of
      gravitational wave interferometers. Inspiration drawn from the figure in \cite{PhaseInsensitiveFilters}.}
    \label{fig:QIFOcontrol}
    \index{figures}
  \end{figure}

  To solve for the contribution at $\hat{a}_\mathrm{out}$ due to fields entering
  at $\hat{n}_\mathrm{q}$ and $\hat{h}$, one can write down the adjacency matrix
  $A$ for the MIMO system and invert $1-A$ to find the closed-loop transfer
  functions.

  Unitarity imposes
  conditions on the matrices $Y$ in Fig.
  \ref{fig:QIFOcontrol}, which collectively have 4 degrees of freedom (two
  amplitudes and two phases) at each frequency. 
  These are sufficient to maximize signal coupling
  while minimizing noise coupling to the output. The
  phase-sensitive amplifier behaves nonreciprocally to create the following
  coupling (with some abritrary choice of quadratures for concreteness)
  
  \begin{itemize}
    \item Noise entering at $\hat{q}_{\cos}$ is resonantly transmitted and
      amplified to $\hat{h}_{\cos}$
    \item Noise entering at $\hat{q}_{\sin}$ is resonantly transmitted and
      de-amplified to $\hat{h}_{\sin}$
    \item Noise entering at $\hat{h}_{\cos}$ is resonantly transmitted and
      de-amplified to $\hat{a}_{\mathrm{out}, \sin}$ 
    \item Signal entering at $\hat{h}_{\sin}$ is resonantly transmitted and amplified
      to $\hat{a}_{\mathrm{out}, \sin}$.
  \end{itemize}

  One can already see that the effect of optical losses and amplifier noises will become relevant due to the round-trip deamplification-amplification
  for noise entering at the readout port. The quantity and distribution of optical
  losses and displacement noises throughout the various subsystems will determine an optimal amplifier gain and
  level of squeezing injected at the dark port. However, as long as the system
  properties determine an optimal amplifier gain $|G_\mathrm{fwd}(\Omega)||G_\mathrm{rev}|>1$, the input-referred
  noise due to dark port vacuum fields is roughly $|G_\mathrm{fwd}||G_\mathrm{rev}|$ lower than for the IFO
  with phase-insensitive filtering described in \cite{PhaseInsensitiveFilters}.

  Another way of viewing the behavior of the filter cavity is as a
  circulator-squeezer. The phase-sensitive amplifier antisqueezes (amplifies)
  the signal-carrying quadrature on the way from the sensing cavity to the
  device output. And it instead squeezes (de-amplifies) the vacuum field quadrature that must
  inevitably reach the signal input due to the photodetection process. The
  filter cavity around the phase-sensitive amplifier acts to set the impedance
  matching condition such that any reflected field must pass through the
  amplifier in both directions before reaching the photodetection port.

  In Figures \ref{fig:Qfilter_ASD} and \ref{fig:Qfilter_gain}, we compare the
  performance of one phase-sensitive filter to the cases considered in
  \cite{PhaseInsensitiveFilters}. ``Resonant Sideband Extraction'' (RSE) refers to the
  cavity formed by IM and CM being $\pi/2$ off-resonance with no filtering ($Y_{ij}=1$), such that signal
  sidebands are resonantly transmitted through the input-filter cavity. ``Signal
  Recycling'' (SR) refers to the cavity formed by IM and CM being on-resonance with
  no filtering ($Y_{ij}=1$), enhancing signal buildup at DC. The Optimal
  Phase-Insensitive Filter is described in \cite{PhaseInsensitiveFilters} as RSE
  with a unity gain phase-insensitive filter defined by

  \begin{equation}
    \begin{split}
      Y_{11}^\mathrm{PIF} = Y_{22}^\mathrm{PIF} &= \sqrt{\frac{i\Omega + \gamma_s}{i\Omega - \gamma_s}}\\
      Y_{12}^\mathrm{PIF} = Y_{21}^\mathrm{PIF} &= 0 \\ %2
      \gamma_s & \equiv \frac{c\tau_{CM}^2}{4L_s}.
    \end{split}
  \end{equation}

  The ``Phase-Sensitive Filter+Amplifier'' starts with the same overall phase as
  the Phase-Insensitive Filter case, but allows $Y_{ij}$ to take a form inspired
  by $M_{ij}$ in Eq \ref{eq:Mij}. Eq \ref{eq:PSF} implements the same form of
  phase-sensitive filtering as PARMZI, but with the optimal phase-insensitive
  phase replacing the phase (and frequency-dependence) imposed by the PARMZI ring
  cavity and mechanical susceptibility. 

  \begin{equation}\label{eq:PSF}
    \begin{split}
      M_{ij}^\mathrm{PSF}
      & =
        2 G_iG_j
        \begin{pmatrix}
          \cos\theta_i\sin\theta_j & \sin\theta_i\sin\theta_j \\
          -\cos\theta_i\cos\theta_j & -\sin\theta_i \cos\theta_j
        \end{pmatrix} \\
      Y_{ij}^{PSF}
      & =
        (\delta_{ij}\mathbb{I} + M_{ij}) \sqrt{\frac{i\Omega + \gamma_s}{i\Omega - \gamma_s}}
    \end{split}
  \end{equation}

  
  
%  TO DO -- add the numerical results, and if time the analytic derivation.
%  What's the difference between just having
%  phase-insensitive filter inside RSE cavity + FD squeezed
%  injection + PSOMA on the output? I guess it might depend on where your optical
%  losses are. Does it let you not use the SEM (I
%  think no)? Can it
%  do backaction evasion (maybe)? Can we reject displacement noise from the
%  IM and CM (I'm pretty sure yes)?

  \begin{figure}[h!]
    \centering
    \includegraphics[width=\textwidth]{Figures/Qfilter_ASD.pdf}
    \caption[Sensitivity of GWIFOs with Quantum Filtering]{Input-referred
      quantum noise for lossless resonant sensors as in Fig. \ref{fig:QIFOcontrol} with various filters
      inside signal cavity.}
    \label{fig:Qfilter_ASD}
    \index{figures}
  \end{figure}

  \begin{figure}[h!]
    \centering
    \includegraphics[width=\textwidth]{Figures/Qfilter_gain.pdf}
    \caption[Gain of GWIFOs with Quantum Filtering]{Signal
      gain for lossless resonant sensors as in Fig. \ref{fig:QIFOcontrol} with various filters inside
      signal cavity.}
    \label{fig:Qfilter_gain}
    \index{figures}
  \end{figure}

  Figs. \ref{fig:Qfilter_ASD} and \ref{fig:Qfilter_gain} show that the
  phase-sensitive filter suppresses input-referred quantum noise, but at the expense of some signal gain. Furthermore, the noise
  suppression is affected by the fine tuning of the input and filter cavities,
  as suggested by the resonances visible in the input-referred quantum noise
  between 10 Hz and 1 kHz, and the noise is actually amplified at high frequency.
  This should not be a fundamental limitation of internal phase sensitive
  filtering, but a more general expression for $Y_{ij}^\mathrm{PSF}$ and further
  optimization is required to realize the full benefits
  and identify the true limitations. In particular, one may expect that the
  optimal overall phase for PSF is not the same as that for PIF.

  There are some benefits to placing the PSA inside the filter cavity, rather than using
  the combination of a directional amplifier like PSOMA, a circulator, and frequency-dependent squeezed light
  injection. First, the benefits of phase-sensitive preamplification for
  downstream optical loss immunity are moved closer to the signal port. Further, because signals encounter the amplifier from one direction only ($h$), but
  amplifier noises are coherently injected towards both the IM and CM, it would
  be interesting to search for amplifier noise free
  configurations. For example, if the PSA is a ponderomotive amplifier like
  PARMZI, one may want to choose for displacement noises at PARMZI cavity
  mirrors to appear in the quadrature orthogonal to signals injected with $h$.

%  Phase-sensitive amplification also offers benefits compared to quantum noise
%  evasion by interferometric speedmeters. While speedmeters improve
%  signal-to-noise ratio by cancelling the effect of both noise and signal,
%  phase-sensitive amplification de-amplifies dark port noise while
%  amplifying signal. (TODO: not sure this is true?)

% TODO: add parameters for PSF configuration plotted
  
  \subsection{PARMZI in the Signal Recycling Cavity}\label{PARMZI_SEC}

  If we replace the generic phase-sensitive filter in section \ref{phaseGWIFO} above with the
  input-output relations of PARMZI, we can understand one specific instance of
  phase-sensitive filtering applied to GWIFO. In Figs. \ref{fig:PARMZI_SEC_ASD}
  and \ref{fig:PARMZI_SEC_gain}, we considered a lossless dual recycled
  Fabry-Perot Michelson Interferometer (DRFPMI) and numerically minimized differential arm length
  strain (DARM)-referred
  quantum noise over SEC detuning, forward- and reverse- PARMZI pump power and
  phase, and foward- and reverse- PARMZI signal phase.
  
  \begin{figure}[h!]
    \centering
    \includegraphics[width=.75\textwidth]{Figures/PARMZI_ASD.pdf}
    \\[\smallskipamount]
    \includegraphics[width=.75\textwidth]{Figures/PARMZI_chaos.pdf}
    \caption[PARMZI in SEC Strain Sensitivity]{Strain sensitivity for PARMZI
      inside the SEC. Top to bottom: Input-referred strain sensitivity
      for Voyager-like GWIFO with addition of optimal PARMZI inside the signal
      extraction cavity; many local optima for PARMZI inside SEC parameter choices.}
    \label{fig:PARMZI_SEC_ASD}
    \index{figures}
  \end{figure}

  \begin{figure}[h!]
    \centering
    \includegraphics[width=.75\textwidth]{Figures/PARMZI_gain.pdf}
    \caption[PARMZI in SEC Signal Gain]{Differential strain signal gain for Voyager-like GWIFO
      with addition of PARMZI inside the signal extraction cavity.}
    \label{fig:PARMZI_SEC_gain}
    \index{figures}
  \end{figure}

  Fig. \ref{fig:PARMZI_SEC_ASD} shows the quantum noise ASDs for DRFPMI with
  Voyager-like parameters tuned for resonant sideband extraction, along with the
  curves for infinite-mass Fabry-Perot test masses (TMs) and for PARMZI optimized for 200
  kg TMs and no displacement noises or optical loss. The quantum
  noise curve with PARMZI in the SEC has a modest improvement from 1-6 kHz due
  to compensating the DARM cavity pole over these frequencies. Below 300 Hz,
  PARMZI implements backaction evasion essentially analogous to variational
  readout \cite{QN_squeezedVariational}, but with some resonant gain due to an
  optical spring formed by the ring cavity mirrors and the counterpropagating
  PARMZI pumps. With no displacement noise, the optimal spring resonance is at
  several 10s Hz, but considering Brownian noise in DARM and suspension thermal
  noise of PARMZI would likely push the optimal resonance to higher frequency as
  in the second plot of \ref{fig:PARMZI_SEC_ASD}. We expect the quantum noise
  improvement to be resilient against intracavity optical losses because the
  improvement is due to gain enhancement rather than noise suppression, but this
  should be shown explicitly. 

  \section{Dual Recycled Mach-Zehnder Interferometers for Gravitational Waves}

  As discussed above, SPARMZI is one type of phase-sensitive
  amplifier for optical fields. SPARMZI has several unique features relative to
  similar, previously explored filtering and amplification schemes.

  The ponderomotive interaction is a third order
  nonlinearity, similar to materials with $\chi^3$ optical nonlinearities (Kerr
  media). \cite{KerrOpticalSpring} explores some of the
  tradeoffs for $\chi^2$ and $\chi^3$ optical nonlinearities.
  However, purely optical nonlinearities build up gain along a
  propagation length of many laser wavelengths, and therefore do not amplify on
  both transmission and reflection as in PARMZI or SPARMZI. The benefit of
  optomechanical over all-optical amplifiers is lower optical loss, with a tradeoff in
  displacement noise at the ponderomotively active mirror and less gain at high frequency.

  % TODO: describe tradeoffs of chi^2 and chi^3 nonlinearity

  \subsection{SPARMZI as Resonant Strain Sensor}

  This subsection uses a Finesse3 model to investigate SPARMZI with Voyager-like parameters. We consider
  ring cavities with 4 km arm lengths (8 km cavity round trip) and 200 kg test
  masses. To comply with the anticipated thermal budget of the cryogenically
  cooled TMs, the laser power is 1.5 MW circulating in each direction of each
  ring cavity such that the total laser power incident on each mirror is 3 MW. The model includes optical loss, but no displacement noise. Note that
  one could minimize optical loss per unit arm strain by using bowtie cavities in
  place of ring cavities in each arm, but this does not affect the major
  features. For simplicity, one power-recycling and one signal-extraction mirror
  are set to 0 transmissivity, such that two of the four ports of the
  Mach-Zehnder are closed except for optical loss. Fig. \ref{fig:SPARMZI_ASD} shows the DARM-referred quantum noise for SEC
  and PRC tunings analogous to RSE, for numerically optimized tunings, and for
  the same numerically optimized tuning but with infinitely massive mirrors.

  \begin{figure}[h!]
    \centering
    \includegraphics[width=.75\textwidth]{Figures/SPARMZI_ASD.pdf}
    \caption[SPARMZI Strain Sensitivity]{DARM-referred strain sensitivity for
      SPARMZI with Voyager-like parameters.}
    \label{fig:SPARMZI_ASD}
    \index{figures}
  \end{figure}

  
  \begin{figure}[h!]
    \centering
    \includegraphics[width=.75\textwidth]{Figures/SPARMZI_gain.pdf}
    \caption[SPARMZI Strain Sensitivity]{Differential strain signal gain for
      SPARMZI with Voyager-like parameters}
    \label{fig:SPARMZI_gain}
    \index{figures}
  \end{figure}

  As shown in Fig. \ref{fig:SPARMZI_gain}, the quantum noise improvement afforded
  by the extra degrees of freedom in SPARMZI relative to RSE is due to signal
  enhancement, not noise cancellation. The resonance around 8 Hz is due to an
  optical spring whereby differential strain signals generate phase sidebands in the SEC,
  which then recombine at input/output beamsplitters in-phase with the pump to
  ponderomotively drive test-mass position. As in section \ref{PARMZI_SEC}, this
  is a novel mechanism for generating an optical spring, since all pumped
  cavities are on-resonance.

  \subsection{Some Other Schemes}

  SPARMZI is an exciting new building block for optomechanical sensors, which
  immediately suggests plethora reconfigurations, and a few are drawn in Fig. \ref{fig:future_configs}.

  The obvious drawback of SPARMZI is that half of the strain signal enters the
  SEC in the ``wrong'' direction, and must encounter the arm cavities before
  SEM in the first cavity round trip. If the SRM were instead transmissive, one
  could consider coherently sensing at both signal-coupled ports.

  In analogy with \cite{Yanbei_SagnacSpeedmeter}, one could also detune the
  Mach-Zehnder such that the pump encounters three of the four recycling
  mirrors. This still leaves one interferometric degree of freedom more than the
  MZI speedmeter in \cite{Yanbei_SagnacSpeedmeter}.

  Some have suggested placing a $\chi^3$ material in the arm cavities of a FPMI,
  and one could study the MZI in this context.

  One could also consider coherently combining the two signal ports before
  introducing signal recycling mirrors. This leads to a configuration analogous
  to the L-shaped resonator considered in \cite{Lresonator} \cite{Lresonator_control}, which
  exhibits excellent and loss-tolerant high frequency sensitivity due to its
  common and differential modes having different resonant frequencies.

  Those interested in tabletop tests of gravitationally coupled quantum systems
  may consider configurations with bowtie traveling wave cavities in a folded
  MZI. This permits the test masses to be brought close together.

  Finally, one may consider whether traveling wave cavities with mechanically
  coupled test masses could be used for displacement noise free interferometry,
  as in \cite{DFI_MZI} \cite{DFI_general}.

  \begin{figure}[h!]
    \centering
    \includegraphics[width=\textwidth]{Figures/future_configs.pdf}
    \caption[Future SPARMZI Configurations]{Some configurations involving
      SPARMZI-like interferometers for future study.}
    \label{fig:future_configs}
    \index{figures}
  \end{figure}

  
%  \printbibliography[heading=subbibliography]

%\end{refsection}