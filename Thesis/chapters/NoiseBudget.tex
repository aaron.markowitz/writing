%\begin{refsection}

  \chapter{Optomechanical Amplifier Characterization} \label{Budget}

  We characterized the current status of the PSOMA demonstration by
  estimating the frequency noise budget of the PDH sensor, and by making a first
  pass at measuring the two-quadrature transfer functions of the cavity.

  \section{System Identification}\label{sysID}

  The system parameters derived from measurements in this section are used to
  construct the amplifier noise budget in Section \ref{budget}, and summarized
  in \ref{table:demoParams}. A good reference on frequency domain system
  identification is \cite{SysID}.


  \subsection{Cavity Parameters}

  We measured the FSR of the PSOMA cavity to be $\Delta\nu_{FSR}\approx 675 MHz$
  by observing peaks in the cavity transmission profile while sweeping a
  p-polarized pump through resonance. We used p-polarized light to increase
  cavity transmission (the cavity mirrors are coated for s-polarized light
  to reduce their sensitivity to angle of incidence) during FSR measurements.

  \begin{figure}[h!]
    \centering
    \includegraphics[width=\textwidth]{Figures/FSR_sweep.png}
    \caption[Cavity Free Spectral Range Measurement]{Transmission profile (blue) while driving the laser current (red)
      with a triangle wave to sweep laser frequency across cavity resonances.
      The PDH error signal (green) and known sideband modulation frequency lets
      us convert the x-axis to MHz. The yellow trace is the DC monitor on
      REFL PD.}
    \label{fig:FSR_sweep}
    \index{figures}
  \end{figure}

  \subsection{Most recent cavity transfer functions}

  The latest measurements towards the cavity transfer function are in Fig.
  \ref{fig:kat_cavBode}. The data are noise dominated except in a few cases. We also note
  that the observed amplitude-to-frequency transfer function has good coherence
  but is not consistent with ponderomotive amplification (which would have a
  $1/\Omega^2$ response as in \ref{fig:kat_PLLbode}). We have previously noted
  some at least $0.03\%/MHz$ coupling of PDH control frequency to relative
  intensity noise on PMON, but there could be electronic or other crosstalk
  responsible.

  We use the coherence $\gamma^2$ at each frequency $\Omega_k$ of the swept sine
  to estimate the variance of the transfer functions as \cite{SysID}

  \begin{equation}\label{coh_var}
    \sigma^2(\Omega_k) = |G(i\Omega_k)|^2\frac{1-\gamma^2(\Omega_k)}{\gamma^2(\Omega_k)}.
  \end{equation}
  
  Ultimately we would like to use our transfer function
  measurements to refine the parameters of
  a physical model like that in Section \ref{controlModel}. We leave this to a
  future with a quieter cavity and more accurate model.
  
  %\begin{figure}[h!]
  %  \centering
  %  \includegraphics[width=0.49\textwidth]{Figures/cavTF_FM_FM.pdf}
  %  \hfill
  %  \includegraphics[width=0.49\textwidth]{Figures/cavTF_AM_FM.pdf}
  %  \\[\smallskipamount]
  %  \includegraphics[width=0.49\textwidth]{Figures/cavTF_FM_AM.pdf}
  %  \hfill
  %  \includegraphics[width=0.49\textwidth]{Figures/cavTF_AM_AM.pdf}
  %  \caption[Measured Cavity Transfer Functions]{Transfer functions across the
  %    optical cavity. Cavity Frequency In is estimated as the reading on a Moku
  %    Phasemeter (digital PLL) measuring the frequency of the pump-probe beat
  %    note on BEAT PD. Cavity Frequency Out is estimated as the control signal
  %    on the FLL locking pump-probe beat frequency on REFL PD. Cavity Power In
  %    is estimated as the laser intensity on PMON relative to mean intensity.
  %    Cavity Power Out is estimated as the laser intensity on TRANS PD relative
  %    to mean intensity. Coherence is between the numerator and denominator
  %    channels.}
  %  \label{fig:cavTFs}
  %  \index{figures}
  %\end{figure}


  %\subsection{Laser cross coupling}

  %TODO: add measurement from elog 3160. Possibly should go in 'design' section
  %on laser drivers.

  %TODO: Add cavity transfer function measurements? elog 3156

  \begin{table}[h!]
    \centering
    \begin{tabular}{||c c c c||}
      \hline
      Symbol & Description & Measured Value & Future Value \\ [0.5ex]
      \hline\hline
      $\Delta \nu_{FSR}$ & Ring Free Spectral Range & 675 MHz & 675 MHz \\
      $P_\mathrm{pump, launch}$ & Pump Power from Collimator & 33 mW & -- \\
      $P_\mathrm{circ}$ & Cavity Circulating Power & $\approx 1$ W & 560 W \\
      $\mathcal{F}$ & Cavity Finesse & $\approx$ 9,000 & 19,000 \\
      $L_\mathrm{cav}$ & Cavity Length & 44.4 cm & 37 cm \\
      $r_\mathrm{beam}$ & Beam waist radius & $\approx 350$ $\mu$m & 215 $\mu$m \\
      $f_\mathrm{mech}$ & Cantilever Frequency & 66 Hz & 43 Hz \\
      $Q_\mathrm{mech}$ & Cantilever Q & $\approx 3e5$ & 1e6 \\
      $m_\mathrm{mech}$ & Cantilever mirror mass & $\approx 1$ g & 0.5 g \\
      $S_{\mathrm{RIN},0}$ & Pump RIN floor & -- & $10^{-8}/\sqrt{\mathrm{Hz}}$ \\
      $f_\mathrm{corner}$ & RIN corner frequency & -- & 100 Hz \\
      $S_\mathrm{f}$ & Pump frequency noise & -- & $10^{-10} \mathrm{Hz}/\sqrt{\mathrm{Hz}}$ \\
      \hline
    \end{tabular}
    \caption[PSOMA Demonstration Experimental Parameters]{Table of amplifier parameters. Measured values are characterized in
      \ref{sysID} and used to derive the noise budget in Sec. \ref{budget}. Future
      values are those used to project the performance of future upgrades in Fig.
      \ref{fig:futureNoise}.}
    \label{table:demoParams}
  \end{table}

  \section{Noise Budget} \label{budget}

  The noise budget compares various noises due to PSOMA and measured at its
  output as if they could all be attributed to sources at input of a noiseless
  amplifier with gain $G(\Omega)$. PSOMA's input-output relation in Eq
  \ref{eq:PSOMA_IO} can be reduced to a single gain $G(\Omega)$ for signals
  $h(\Omega)$ entering along $\hat{b}_{in,1}$ and transmitted along $\hat{b}_\mathrm{out,2}$,
  along with total noises $n_i(\Omega)$.

  \begin{equation}
    b_\mathrm{out,2}(\Omega) = G(\Omega) (h(\Omega) + n_i(\Omega))
  \end{equation}

  Any noise appearing at the output $\hat{b}_\mathrm{out,2}$ with spectral
  density $S_\mathrm{out,i}(\Omega)$, even those coupled in from $\hat{b}_\mathrm{in,2}$, can
  be input-referred and contribute to the total measured input-referred noise
  $S_\mathrm{in}$.

  \begin{equation}
    S_\mathrm{in} = \sum_i \frac{S_\mathrm{out}(\Omega)}{G(\Omega)}
  \end{equation}

  \subsection{Quantum}

  An optical vacuum field $\hat{v}$ with unit amplitude replaces the circulating field
  everywhere there is optical loss, as described in Eqs. \ref{eq:FS}
  or \ref{eq:BS}. Quadrature detection of $\hat{v}$, for example by balanced
  homodyne detection with a local oscillator field intensity $I_\mathrm{LO}$ at
  $\omega_\mathrm{LO}$, would find a flat noise spectral density due to shot
  noise of

  \begin{equation}
    S_\mathrm{shot}=\frac{\hbar c^2}{I_\mathrm{LO}\omega_\mathrm{LO}}.
  \end{equation}

  The mirrors of the optomechanical system also experience a force noise due to
  shot noise from $\hat{v}$, and their dynamics are affected according to \ref{eq:BS_IO}.

  We include quantum noise in the projected future noise budget
  (\ref{fig:futureNoise}), but do not expect it to limit amplifier performance.
  Instead, quantum noise excluding vacuum fluctuations carried with the probe
  field is relevant as a figure of merit for PSOMA's utility to
  photodetection loss limited measurement devices. When we use an unsqueezed
  probe to characterize PSOMA, we can write the
  input-referred noise attributable to the amplifier as

  \begin{equation}
    S_\mathrm{amp} = S_\mathrm{in} - S_\mathrm{shot}.
  \end{equation}

  At frequencies where $S_\mathrm{in} < S_\mathrm{shot}$, we would expect
  sub-SQL signals to benefit from PSOMA's preamplification assuming sufficient
  amplifier gain.
  
  %The input-referred noise and gain of the phase-sensitive optical preamplifier can be
  %summarized in analogy with the electronic amplifier's noise figure, which we
  %adapt from \cite{optical_noiseFigure} as

  %\begin{equation}
  %  F = 1+\frac{S_\mathrm{amp}}{S_\mathrm{source}}(1-G)
  %\end{equation}

  %Where here $S_\mathrm{source}$ is the input-quadrature noise carried along
  %with the signal of interest.

  % TODO: add noise figure
  
  \subsection{Suspension Thermal}

  Suspension thermal noise is due to mechanical loss in the cantilever mirror
  suspension. Note that increasing mirror mass decreases suspension thermal
  noise, which can sometimes offer a favorable tradeoff with ponderomotive gain.
  For the calculation in \ref{fig:futureNoise}, the displacement noise spectral
  density on the
  $i$th cantilever mirror
  $x_{ST,i}(\Omega)$ can be derived from
  the fluctuation-dissipation theorem for a mechanical oscillator with loss
  $\phi_\mathrm{mech}(\Omega)$, resonant frequency $\omega_\mathrm{mech}$, and mass $m$ held
  at temperature T \cite{SusTherm_FDT}.
  
  \begin{equation}
    x_{ST,i}(\Omega) = \sqrt{\frac{4 k_BT}{m \Omega}
      (\frac{\omega_\mathrm{mech}^2\phi_\mathrm{mech}(\Omega)}{\omega_\mathrm{mech}^4\phi_\mathrm{mech}^2(\Omega)
        + (\omega_\mathrm{mech}^2 - \Omega^2)^2})}
  \end{equation}

  \subsection{Coating Brownian}

  Coating Brownian noise is another force noise arising from the
  fluctuation-dissipation theorem, this time applied to multiple stacked layers
  in a dielectric mirror coating. We can treat each unit area of the stacked
  coating somewhat independently, such that the phase imparted on a width $w$ Gaussian
  beam profile reflected from the mirror surface falls off like $1/w^2$. In Fig.
  \ref{fig:futureNoise}, we use the expression from Eq. 21 in \cite{TN_coatBrown}
  for the Brownian noise contribution just due to coatings. The $i$th mirror
  experiences displacement noise $x_{CBN,i}$ due to a mirror coating of
  thickness $d$, mechanical loss angle $\phi_c$, Young's modulus $Y_c$, and
  Poisson ratio $\nu_c$ on a substrate with Young's modulus $Y_s$ and
  Poisson ratio $\nu_s$. 

  \begin{equation}
    x_{CBN,i}^2 = \frac{2k_BT}{\pi^2 \Omega}
    \frac{\phi_c d}{w_B^2 Y_s^2 Y_c(1-\nu_c)^2}
    (Y_c^2(1+\nu_s)^2(1-2\nu_s)^2 + Y_s^2(1+\nu_c)^2(1-2\nu_c)^2)
  \end{equation}

  \subsection{Laser Frequency Noise}

  It is possible to prestabilize PSOMA's pump laser to reduce its amplitude and
  frequency noise to at or near its shot noise level. However, in our tabletop
  demonstration we simply stabilize the pump's frequency to the PSOMA cavity.
  For the single-cavity experiment, the pump's frequency noise sums with any amplified signal at the output of the
  amplifier.

  At low frequency, the laser's shot noise is overcome by 1/f noise due to a
  combination of effects including Brownian thermal noise inside the fiber cavity substrate
  \cite{Numata_diode1f} and carrier density
  fluctuation at the diode interfaces \cite{1f_laserNoise}.

  For the projection in Fig. \ref{fig:futureNoise}, we assume a flat pump
  frequency noise $S_{f}=10^{-10}$ Hz/$\sqrt{\mathrm{Hz}}$. 

  Noise in semiconductor lasers is further discussed in \cite{laserNoise_PNsemi},
  \cite{laserNoise_LW}, \cite{laserNoise_GaAlAs},
  \cite{laserNoise_gratings}, \cite{semiconductor_book},
  \cite{laserNoise_interferenceECDL}, \cite{laserNoise_degradation}. Some other
  frequency noise measurements are discussed in
  \cite{laserNoise_characterization}, \cite{laserNoise_1fHeterodyne}.

  To estimate the pump laser's frequency noise, we use a three corner hat
  measurement as described in Section \ref{TCH}. The inferred laser frequency
  noise is represented in Fig. \ref{fig:TeraX3CH}. It should be noted that
  because the TeraXion laser is much quieter than the two Rio lasers used as
  reference clocks in the three corner hat, uncertainty on the inferred TeraXion
  frequency is skewed to lower amplitude spectral density.

  \begin{figure}[h!]
    \centering
    \includegraphics[width=0.8\textwidth]{Figures/Teraxion_noise_smokey.pdf}
    \caption[TeraXion Frequency Noise]{Frequency noise of the TeraXion (pump) laser
      inferred from three corner hat measurement. Scatter point opacity is
      inversely proportional to relative uncertainty in the ASD estimate.}
    \label{fig:TeraX3CH}
    \index{figures}
  \end{figure}
  
  % TODO: add frequency noise from 3CH, elog 3154
  % also note on error propagation a la 2802

  \subsection{Laser Amplitude Noise}

  The current configuration uses only one ring cavity rather than a
  balanced Mach-Zehnder with two ring cavities. We do not yet subtract amplified
  pump relative intensity noise (RIN), which means pump RIN contributes directly
  to the input referred noise of the amplifier.

  For the projection in Fig. \ref{fig:futureNoise}, we assume pump RIN has the values in Table
  \ref{fig:futureNoise} and the form

  \begin{equation}
    S_\mathrm{RIN}(f) = \frac{f + f_\mathrm{corner}}{f} S_\mathrm{RIN,0}.
  \end{equation}

  \subsection{Current Noise}

  The frequency of light emitted by the laser depends on the drive current applied
  across the diode, as described in Sec. \ref{actuators}. Therefore, current noise across the laser diode directly
  sources frequency noise of the laser frequency.

  % TODO: add figure from elog -- where?
  
  \subsection{Seismic Noise}

  We characterized the seismic noise with a Wilcoxon accelerometer. The accelerometer contains a built-in current
  preamplifier, which we power using a low noise DC supply driving a JFET
  operated as a constant current source. We estimate the contribution of seismic noise to
  cavity frequency noise by using the nominal accelerometer calibration to put
  the accelerometer signal into m/rtHz at the cantilever mirror, which is then
  filtered by the cantilever mechanical response with two zeros at 0 Hz.

  % TODO: add accelerometer plots??

  We observe a seismic noise peak between 20-30 Hz, which creates significant
  noise when the cantilever resonance falls in that range. For example, when
  operated with a ~25 Hz cantilever, the cavity exhibits ~100 nm/rtHz frequency
  noise at the cantilever resonance. For cantilevers with ~25 Hz resonances, we
  found that seismic noise saturates our fast pump current actuation. We can
  still lock the cavity by damping this seismic noise by feeding either the
  cavity error signal or an auxiliary sensor of cantilever motion back to a
  piezo driving the cantilever base. We also switched to a
  cantilever with higher resonant frequency (~66 Hz) in part to reduce seismic noise
  coupling.
  
  \subsection{Electronics}

  To diagnose the noise in Fig. \ref{fig:cavNoise}, we observed the noise at the PDH error
  monitor point with PDH modulation off, probe laser blocked, and cavity
  off-resonance. The noise measured at PDH error is then referred to frequency
  noise at the cavity input and shown in \ref{fig:cavNoise} as the ``electronics
  noise'' curve, which agrees well with the total observed noise. The demodulated
  voltage at the PDH error point is most directly attributable to a combination of (1)
  voltage noise below ~kHz between the PDH mixer output and the Moku ADC; (2)
  voltage noise around $f_\mathrm{PDH}=33.59$ MHz between REFL PD's
  transimpedance amplifier and the PDH mixer; (3) current noise around
  $f_\mathrm{PDH}=33.59$ MHz across the REFL
  photodiode; (4) pump intensity noise around $f_\mathrm{PDH}=33.59$ MHz.

  There is no reason for pump intensity noise to peak around $f_\mathrm{PDH}$
  when no PDH modulation is applied to the pump laser. Moreover, we do not
  observe such intensity noise using other photodiodes and a spectrum analyzer.
  But, we do observe that the noise is linearly related to pump power as shown
  in Fig. \ref{fig:electronicsNoise}. This suggests the noise is sources before
  PDH demodulation.

  \begin{figure}[h!]
    \centering
    \includegraphics[width=\textwidth]{Figures/PDHerr_vs_power.pdf}
    \caption[Electronics Noise Diagnostic]{Noise at PDH error monitor point with
      no phase modulation applied to the pump laser is shown to be linearly
      proportional to pump power below 200 Hz.}
    \label{fig:electronicsNoise}
    \index{figures}
  \end{figure}
 
  Since measuring Fig. \ref{fig:cavNoise}, we have electrically isolated TRANS and
  REFL PDs from the optics table to avoid potential ground loops affecting REFL PD. We also note
  that Fig. \ref{fig:cavNoise} is measured using p-polarized pump light,
  resulting in a relatively low cavity Finesse (under 1000). We expect
  measurements on the s-polarized cavity would see less electronics noise due to
  increased plant gain.
  
  % TODO? \cite{Hobbs, Building Electro-Optical Systems}
  % TODO: finish, add equations, add sus therm
  
  \subsection{Pound-Drever-Hall Noise Budget}

  Though we ultimately anticipate using an independent sensor of frequency
  fluctuations at PSOMA's input, the PDH loop provides our most sensitive
  measurement of frequency noise at the cavity input.
  
  The total noise in Fig. \ref{fig:cavNoise} is dominated at low frequency by
  likely electronics noise. We also observe total noise close to laser frequency noise at
  high frequency.

  \begin{figure}[h!]
    \centering
    \includegraphics[width=\textwidth]{Figures/CavityNoiseBudget_Hz_mod.pdf}
    \caption[PSOMA Measured Noise Budget, p-Polarized]{Frequency noise budget of the PSOMA cavity referred to frequency
      fluctuations at the cavity input. Measured with p-polarized pump, so the
      cavity gain is unusually low.}
    \label{fig:cavNoise_p}
    \index{figures}
  \end{figure}

  \begin{figure}[h!]
    \centering
    \includegraphics[width=\textwidth]{Figures/CavityNoiseBudget_Hz_fin.pdf}
    \caption[PSOMA Measured Noise Budget, s-Polarized]{Frequency noise budget of
    the PSOMA cavity referred to frequency fluctuations at the cavity input.
    Measured with s-polarized pump. Note that the pump-probe beat note frequency
  noise curve contains both pump frequency noise (which contributes to total
  amplifier noise) and probe frequency noise (which does not contribute
  to total amplifier noise).}
    \label{fig:cavNoise}
    \index{figures}
  \end{figure}
  

  %TODO: is this the latest budget? See elog 3046, 3000, 2998

  %TODO: section on gain? first attempt was elog 3053

  \subsection{Quadrature Sensor Noise Budget}

  We can also estimate the noise referred to each of our quadrature sensors (see
  Sec. \ref{quads}), rather than to the cavity input.

  \section{Outlook for Future Upgrades}

  \begin{figure}[h!]
    \centering
    \includegraphics[width=\textwidth]{Figures/PSOMA_timeline.pdf}
    \caption[PSOMA Experiment Outlook]{Outlook for the next several stages of the
      PSOMA experiment.}
    \label{fig:timeline}
    \index{figures}
  \end{figure}

  \subsection{Asymmetric Ring Cavity Mach-Zehnder}

  We already have optics for a Mach-Zehnder interferometer in place in the PSOMA
  vacuum chamber, but have not instrumented Mach-Zehnder length control or
  locked the interferometric degrees of freedom. The MZI can sense pump frequency
  fluctuations independent of cavity length fluctuations appearing in the PDH
  control signal. We anticipate installing length actuation on the MZ steering
  mirror opposite the existing ring cavity and locking the MZI would take 3
  months.

  \subsection{Symmetric Ring Cavity Mach-Zehnder}

  Because pump intensity noise is amplified in only one path of the asymmetric
  ring cavity MZI, we have no way to distinguish cantilever motion driven by
  signals carried with the probe field from motion driven by pump amplitude
  noise. Therefore, pump RIN puts a floor in input-referred amplifier noise for
  the asymmetric configuration. By balancing the gain with identical ring
  cavities in each arm, amplified pump intensity noise is rejected out the
  bright port of the MZI.
  % TODO: estimate gain balance requirement?

  \subsection{Cryogenic PSOMA}
  
  With two balanced ring cavities, we expect the limiting noise differentially
  driving the two cavities to be thermal noise of the cavity mechanics. In
  particular, we expect suspension thermal noise due to the finite Q of the
  light cantilever mirrors to limit PSOMA's noise performance. By cooling the
  cantilevers to $123$ K, we can take advantage of the low intrinsic mechanical
  loss of crystalline Si at this temperature, as well as a lower overall thermal
  bath temperature, to significantly increase the quality factor of PSOMA's
  ponderomotively active mirror suspensions.


  To achieve sub-SQL input-referred noise, we must increase the Q of PSOMA's
  mechanically compliant optics. We can achieve this by cooling its Si
  cantilevers to 123 K, where we anticipate lower suspension thermal noise due
  to the reduced thermoelastic loss in Si at this temperature.

  % TODO: add noise budget for cryo full PSOMA

  \begin{figure}[h!]
    \centering
    \includegraphics[width=.8\textwidth]{Figures/futurePSOMA_noise.pdf}
    \caption[Projected Noise Budget with Future
    Upgrades]{Projected noise budget of tabletop demonstration given upgrades to
      cryogenic PSOMA.}
    \label{fig:futureNoise}
    \index{figures}
  \end{figure}
    

  \subsection{Future work}
  
  The successful cryogenic demonstration would allow PSOMA to directly benefit
  future GWIFO with high levels of readout loss limited squeezing. Future work
  on the tabletop could explore some of the modified optical configurations
  considered in Chapter \ref{QIFO}, or applications like mechanical feedback
  cooling or laser intensity stabilization.

  %\printbibliography[heading=subbibliography]

%\end{refsection}