%\begin{refsection}

  \chapter{Experimental Layout and Design} \label{Design}

  We designed and built an initial demonstration towards the phase-sensitive
  optomechanical amplifier described in Chapter \ref{PSOMA} from the ground up.
  The purpose is to work through the important challenges that would need to be
  resolved before applying PSOMA to a Voyager-like interferometer, including
  controls, cryogenics, optical design, and mechanical design.

  \section{Vacuum System}

  The core optics of the PSOMA experiment must be under vacuum to avoid acoustic
  noise coupling from the environment and maintain clean optics of the high finesse
  cavity. The radius of the chamber was chosen to accommodate a breadboard
  containing all core optics, and two
  layers of radiative shielding for cryogenic operation. The height was chosen to
  allow for potential future installation of seismic isolation systems. The
  chamber has four DN125CF flanges for optical feedthrough and thermal feedthrough
  from a cryogenic cooler. There are also four DN35CF flanges for electrical
  feedthrough and vacuum pumping. A hinged lid allows a single user easy access
  to the chamber interior without the use of a hoist. With no installed
  equipment, the vacuum chamber was capable of reaching below $10^{-7}$ torr.

  \begin{figure}[h!]
    \centering
    \includegraphics[width=.8\textwidth]{Figures/cryostat_toplevel_view0.PDF}
    \caption[Vacuum Chamber]{PSOMA vacuum chamber showing breadboard and
      fit-check of optics for DRPSOMA configuration.}\label{fig:chamber}
    \index{figures}
  \end{figure}

  The vacuum system layout (\ref{fig:vacLayout}) is designed such that the chamber can be vented with
  clean, dry compressed nitrogen gas. Venting with $N_2$ reduces the amount of
  water adsorbed onto metal surfaces while vented, which significantly reduces
  pumpdown time.

  \begin{figure}[h!]
    \centering
    \includegraphics[width=.8\textwidth]{Figures/vacuum_system_v2.pdf}
    \caption[Vacuum Layout]{Vacuum layout as designed. Not all DB9 electrical feedthroughs are
      necessary or installed at this stage of experiment. The 4-way cross
      providing cryocooler access is also not yet installed.}
    \label{fig:vacLayout}
    \index{figures}
  \end{figure}

  % TODO: possibly update vacuum diagram from elog 3060

  \section{Mechanics}

  \subsection{Mirror Mounts}

  Most of the kinematic mirror mounts are U100 models from Newport optics. To
  allow alignment and beam clearance of the Mach-Zehnder optics, the input and
  output beamsplitter and MC1 are in Newport 9774 top-adjusting mounts. All
  mounts are on 3/4'' diameter posts to set a 4'' beam height.

  \subsection{Breadboard}

  The in-vacuum breadboard is 1/2'' thick unadonized aluminum with 1'' hole
  spacing. The breadboard is rigidly clamped to 3/4'' diameter posts, and the
  posts are fork clamped to the bottom of the vacuum chamber. Future iterations
  of the experiment can avoid stress-induced misalignment during pumpdown by
  kinematically mounting the breadboard to the chamber. 
  
  \section{Optics}

  The latest version of the optical layout for our experiment is in Fig. \ref{fig:PSOMALayout}
  
  \begin{figure}[h!]
    \centering
    \includegraphics[width=.75\textwidth]{Figures/PSOMA_table.pdf}
    \\[\smallskipamount]
    \includegraphics[width=.75\textwidth]{Figures/PSOMA_chamber.pdf}
    \caption[Optical Layout Photograph]{Optical layout for single-cavity optomechanical amplifier
      demonstration. For a more conceptual diagram, see
      \ref{fig:controlDiagram}.}
    \label{fig:PSOMALayout}
    \index{figures}
  \end{figure}

  
  \subsection{Lasers}
  
  The amplifier is pumped by a single laser on-resonance with PSOMA's ring
  cavities. We also phase-lock an auxiliary probe laser to the pump to
  characterize the system's transfer functions and understand the challenges of
  adapting PSOMA to work with an existing optomechanical sensor with its own
  frequency reference.

  For the PSOMA pump, we use a PureSpectrum Narrow Linewidth Laser from
  TeraXion. This 1550 nm, 80 mW semiconductor laser uses a laser driver module to lock
  the laser frequency to an integrated fiber Bragg grating, achieving narrower linewidth
  and single mode operation over its full temperature range. Fast analog
  modulation is achieved by driving the error point of the module's internal
  feedback loop.
  
  The probe laser is a planar external cavity laser from Rio Laser. The AR
  surface of an InP gain chip is coupled to the AR surface of a planar Bragg
  grating on a silica-on-silicon planar
  lightwave circuit \cite{Rio_laser}.

  % TODO: cite references on laser noise, actuation mechanism, mode hopping, etc

  \subsection{Mirrors}

  Most of the mirrors are off-the-shelf high reflectors or beamsplitters from
  Thorlabs or Newport. However, the cavity mirrors must be superpolished, wedged
  optics with low optical loss high-reflecting and anti-reflecting coatings.

  Because our amplified signal will be imprinted on the phase of light reflected
  from the ring cavities and is proportional to circulating optical power, we
  want the cavity to be overcoupled. To reduce astigmatism in the cavity, we
  should choose a low angle of incidence for the cavity's curved mirror. And,
  using a flat mirror as the cavity's input coupler simplifies mode matching in
  the Mach-Zehnder interferometer.

  Based on these constraints, we want to choose MC1 to be the superpolished flat
  mirror with the highest transmissivity at $45^\circ$. For ease of alignment,
  MC2 should be a curved mirror mounted on the Si cantilever. And MC3 should be
  the superpolished mirror with the lowest transmissivity near $45^\circ$.
  
  \begin{figure}[h!]
    \centering
    \includegraphics[width=.75\textwidth]{Figures/transmission_setup.pdf}
    \\
    \includegraphics[width=.75\textwidth]{Figures/transmissivity_setup_photo.pdf}
    \caption[Transmissivity Measurement Setup]{Optical layout for mirror transmissivity measurement}
    \label{T_layout}
    \index{figures}
  \end{figure}

  We used the apparatus in Fig. \ref{T_layout} to measure the transmissivity
  of several superpolished optics we had on hand. The test laser was passed
  through three polarizing beamsplitters to reject all p-polarized light. A
  two-frequency chopping wheel was used to
  perform separate lock-in measurements of the reference and test beams to
  reduce photodiode electronics noise.

  To reject laser intensity noise, each measurement on the ``Transmission''
  photodiode was calibrated against a ``Reference'' photodiode that monitored a
  fixed fraction of the total laser power and maintained a fixed gain throughout
  the measurement. Due to the extremely low transmissivity of the optics, the
  difference in laser power with and without the test optic in the beam path
  could exceed the dynamic range of the ``Transmission'' photodiode. Therefore,
  an additional pickoff beam was aligned onto the ``Transmission'' photodiode to
  calibrate the photodiode gain when it was changed.

  The test optic was mounted
  in a flip mount to easily add and remove it from the beam path, as well as a
  rotating kinematic mount to change the test beam's angle of incidence on the
  optic. To ensure the beam was entirely on the photodiode with or without any
  deflection due to the test optic, we placed a sharp lens just before the
  transmission photodiode. And, to avoid interference at the transmission PD,
  either the reference or test beam is blocked during each measurement (and we
  assume the photodiode gain is constant for a giving gain setting). 

  Each transmissivity measurement requires, for each of three optical configurations, simultaneous lock-in measurements
  of the transmission and reference photodiode signals:
  \begin{enumerate}
  \item Test optic out of path, test beam incident on transmission PD \\
    ($K_P \equiv P_\mathrm{TRANS}(f_\mathrm{test})/ P_\mathrm{REF}(f_\mathrm{ref})$)
  \item Reference beam incident on transmission PD \\
    ($K_G \equiv P_\mathrm{TRANS}(f_\mathrm{ref}) /
    P_\mathrm{REF}(f_\mathrm{ref})$)
  \item Test optic in path, test beam incident on transmission PD \\
    ($X\equiv P_\mathrm{TRANS}(f_\mathrm{test}) / P_\mathrm{REF}(f_\mathrm{ref})$).
  \end{enumerate}

  In practice, configuration (1) is used to calibrate the fraction of laser
  power incident on the reference PD, and need only be performed once.
  Configuration (2) is used to calibrate transmission PD gain, and need only be
  performed once per gain setting. For example, with the test mirror in the beam
  path the test PD gain may be at $G_1$, while with the test mirror out of the
  beam path the test PD gain may be at $G_2$. Configuration (3) is the actual measurement
  of transmitted power, and can be repeated at several angles of incidence
  for each optic.

  The mirror transmissivity at a particular angle of incidence is then:
  \begin{equation}
    T(\theta_\mathrm{aoi}) = \frac{X / K_{G_1}}{K_P / K_{G_2}}.
  \end{equation}
  
  \begin{figure}[h!]
    \centering
    \includegraphics[width=.75\textwidth]{Figures/trans_laseroptik_13997_2.pdf}
    \\
    \includegraphics[width=.75\textwidth]{Figures/trans_coastline_4902.pdf}
    \caption[Cavity Mirror Transmissivities]{Transmissivity of cavity mirrors
      (top is MC1, bottom MC2) as a function of incident angle. Top is MC1, a
      mirror from Laseroptik. Bottom is MC2, a mirror from Coastline.}
    \label{transmissivity}
    \index{figures}
  \end{figure}

  Fig. \ref{transmissivity} shows the results for the two mirrors used in our
  cavity. We also measured 168 ppm transmissivity near $0^\circ$ on a 1/2'' Si mirror
  with 0.5 m radius of curvature, which we use as MC2.

  Future iterations of PSOMA involving two ring cavities on either
  side of a Mach-Zehnder interferometer will require new custom coating runs to
  achieve nearly identical parameters for both cavities. The mirror
  transmissivities, curvatures, and coating design should be optimized to
  minimize projected amplifier noise at that time.
  
  \subsection{Fiber Components}
  
  The lasers used in PSOMA are fiber-coupled. We also chose to use a fiber-based
  electro-optic modulators (EOM), which offer a wider modulation bandwidth and
  lower required operating voltage for a given modulation depth relative to
  typical free-space EOMs due to the small mode volume inside the nonlinear crystal. The MPX-LN-0.1 EOMs from iXBlue
  Photonics uses a $\mathrm{LiNbO}_3$ waveguide and has a usable bandwidth of
  $300$ MHz with typical $V_\pi$ at $50$ kHz of $3.5$ V \cite{EOMdatasheet}. We
  also used fiber-coupled Faraday isolators (IO-G-1550-APC from Thorlabs) and several
  fiber-coupled beamsplitters (PN1550R1A1, PN1550R2A1 and PN1550R5A2 from
  Thorlabs). 

  The ease of alignment and compact size of fiber optic components come with
  tradeoffs in polarization selectivity, acoustic noise coupling, and optical
  loss. To mitigate these effects, we secure all optical fibers to the breadboard
  to stiffen each length of optical fiber and prevent acoustic coupling. We also
  clean the fiber tips with commercial fiber cleaning tissue and solution to
  minimize excess optical loss or scatter at fiber interfaces. The measured loss
  in fiber components from our pump diode to launch is $\approx -3.8 \mathrm{dB}$,
  close to the sum of nominal losses for the components in series. Using fibers with
  angled APC connectors reduces backreflection of light at fiber interfaces. 
  
  Still, fiber components introduce noise or undesired
  cross-coupling through several routes:
  
  \begin{itemize}
  \item Environmental acoustic noise stresses optical fiber, which can directly
    change the optical propagation length of the fiber and introduce phase noise
    in the launched laser beam.
  \item The EOM's waveguide is designed for TE mode propagation. Misalignment
    of the laser entering the waveguide leads to excess loss for the laser's TM
    component. 
  \item Acoustic noise-induced stress of the optical fiber also induces
    polarization noise in the propagating light. This allows acoustic noise to
    generate polarization noise, which becomes laser amplitude noise when the
    laser passes through components with polarization-dependent losses
    including most fiber components and the free-space PBS.
  \item Due to the birefringence of the EOM's crystal, the drive voltage applied
    across the crystal generates not only phase modulation but also
    polarization modulation of the laser. Since the laser typically passes
    through additional fiber and free space components with polarization
    dependent losses after the EOM, the laser acquires an amplitude modulation
    at the drive frequency. 
  \end{itemize}

  The final effect is especially pernicious when RF control sidebands used
  for example to stabilize the laser frequency to the optical cavity with
  Pound-Drever-Hall locking are applied at the fiber EOM. Any AM component of the
  PDH sidebands generate an offset on the PDH error signal and allows first order
  coupling of amplitude noise (including due to temperature drift of the EOM
  itself) into the laser frequency stability \cite{EOM_AM} \cite{EOM_fiberCav}.
  PM-to-AM coupling due to EOM birefringence can be actively nulled by applying a
  DC offset to the EOM control voltage \cite{EOM_RAMstable_gas} \cite{EOM_RAMstable_shot}. 

  \section{Cavity Geometry}

  We designed PSOMA with a triangular (three mirror) cavity geometry. Triangular
  cavities have nondegenerate s- and p-polarized eigenmodes, which reduces
  resonance of the undesired polarization mode. We also placed the single cavity
  curved mirror at MC2 to reduce the effect of astigmatism at the 45\textdegree
  incidence optics and simplify mode matching in the Mach-Zehnder interferometer. To determine the optimal cavity geometry under the constraint
  that MC1 have a 45\textdegree angle of incidence, we simulated the higher order
  transverse electromagnetic (TEM) mode spectrum of the ring cavity in Finesse 2.
  We minimized coresonance of higher order TEM modes and RF sidebands due to PDH modulation at
  33.6 MHz. The simulated transmission spectrum for the optimal cavity geometry is in Fig.
  \ref{fig:modeScan_theory}.
  
  \begin{figure}[h!]
    \centering
    \includegraphics[width=.75\textwidth]{Figures/CavityGeometry.pdf}
    \caption[Mode Scan Simulation]{Transmissivity of low order Hermite-Gaussian cavity modes from
      Finesse 2 calculation.}
    \label{fig:modeScan_theory}
    \index{figures}
  \end{figure}

  % TODO: include words and/or diagram about higher order mode scans... though these never
  % worked out
  
  \section{Mode Matching}
  
  We measured the transverse beam profile of the pump and probe lasers from the
  fiber-to-free space collimators using a Data Ray Beam'R2 profiler.
  
  \begin{figure}[h!]
    \centering
    \includegraphics[width=.75\textwidth]{Figures/probe_profile.pdf}
    \caption[Beam Profile]{Beam profile of probe laser showing good agreement (ellipticity close
      to 1) along two transverse axes.}
    \label{fig:probeProfile}
    \index{figures}
  \end{figure}
  
  Then, we used the open source mode matching and beam profiling software ``a la
  mode'' to select a pair of lenses to match the beam waist and position to the
  cavity's fundamental mode that minimizes mode mismatch error due to lens
  position error.
  \begin{figure}[h!]
    \centering
    \includegraphics[width=.75\textwidth]{Figures/alaPSOMA_pump.png}
    \caption[Mode Matching Telescope Solution]{Mode matching lens placement solution for the pump laser generated
      by geometrical optics calculation in a la mode.}
    \label{fig:probeMatchingSolution}
    \index{figures}
  \end{figure}

  Experimental methods for optimizing mode matching are discussed in
  \cite{MM_heterodyne}. 

  % TODO: mode matching measured around elog 3146, 3035, 3031
  \section{Electronics}

  \subsection{Photodiodes}

  All photodiodes had InGaAs sensors with about 1 A/W responsivity at 1550 nm.

  Where we only needed to measure signals at acoustic or lower frequencies, we
  used the gain-adjustable PDA20CS from Thorlabs. For higher frequencies, we
  used a Newport 1611 or 1811 receiver.

  All photodiodes are mounted on electrically insulating Teflon bases to prevent
  ground loops and coupling of noise between the optical table and low noise
  transimpedance amplifiers of the photodiodes.

  Note that fiber and free space photodiodes can have slight different
  responsivities, as shown in Fig. \ref{fig:fiberFree_resp}.

  \begin{figure}[h!]
    \centering
    \includegraphics[width=.75\textwidth]{Figures/PD_fiberFree_resp.pdf}
    \caption[Fiber vs Free Space Photodiode Responsivity]{Relative responsivity
      of fiber coupled and free space NewFocus 1611 photodiodes. The time delay
      is due to an optical path length difference in the experimental setup, so
      the magnitude is the main result of interest.}
    \label{fig:fiberFree_resp}
    \index{figures}
  \end{figure}

  \subsection{Moku}

  We used the FPGA-based Moku:Pro from Liquid Instruments in a variety of test
  applications, for example as a network analyzer, spectrum analyzer, digital
  synthesizer, or RF phase sensor.

  We also used Moku:Pro as the controller for our fast (up to 150 kHz bandwidth) feedback loops, including
  for demodulation and filtering.

  \subsection{Laser Drivers}

  The temperature and current of the TeraXion laser are controlled by a
  proprietary laser driver.

  The Rio laser temperature is controlled by a commercial controller from
  Thorlabs, such as ITC502. Rio laser current is controlled by a low noise current
  driver described in \cite{laserDriver}. 

  % TODO: add summing circuit? elog 3179
  
%  \begin{figure}[hbt!]
%    \centering
%    \includegraphics[width=.35\textwidth]{Figures/PD_mount.jpeg} \hfill
%    \includegraphics[width=.35\textwidth]{Figures/pump_fiber_layout.jpeg}
%    \\[\smallskipamount]
%    \includegraphics[width=.35\textwidth]{Figures/pump_fiber_layout.jpeg}
%    \caption[Photographs of Experiment]{From left to right, top to bottom: Photodiode mounted on electrically%
%      insulating mounts; Optical fiber components for pump laser,%
%      showing TeraXion laser driver and module, EOM, faraday isolator, and 99-1
%      pickoff beamsplitter; }
%    \label{fig:labPhotos}
%    \index{figures}
%  \end{figure}

  \begin{figure}[h!]
    \centering
    \includegraphics[width=.75\textwidth]{Figures/cantilever_photos.pdf}
    \caption[Photographs of Si Cantilever with Mirror]{Photos of PSOMA
      cantilever. From top left clockwise:
      Cantilever in jig before being clamped; cantilever mounted at MC2 viewed
      from side; cantilever mounted at MC2 viewed from front (HR surface); piezoelectric
      actuator clamped against cantilever mount.}
    \label{fig:cantileverPhotos}
    \index{figures}
  \end{figure}
 
 
%  \printbibliography[heading=subbibliography]
  
%\end{refsection}