
%\begin{refsection}

  \chapter{Loss Tomography} \label{tomo}

  \section{Statistical Inference for Mechanical Q Measurements}

\subsection{Purpose}
We describe a general procedure to use Q measurements at various
eigenfrequencies and temperatures (or arbitrary other experimentally controlled parameters), plus our knowledge of the physics of our system, to estimate key
material parameters. For the testbed described in Chapter \ref{cryoQ}, the parameters of
interest may be the mechanical loss, Young's modulus, and Poisson's ratio of a
thin film deposited on a Si wafer. One may also be interested in
distinguishing bulk and shear mechanical loss contributions
\cite{Gabriele_bulkShear} \cite{TiTa_bulkShear}. The purpose of this technique
is to avoid the assumptions underlying the analysis in Chapter \ref{cryoQ} and
similar thin film loss measurements, which
require the mechanical loss of the substrate to remain unchanged before
and after the deposition process. A good review of Bayesian parameter estimation
based on experimental results and an approximately linear model can be found in
\cite{Hogg_bayesian}, and a more extensive reference including of MCMC methods
is in \cite{gregory_2005}. 

\subsection{Metropolis-Hastings Markov chain Monte Carlo}

Our experiment measures a set of quality factors and eigenfrequencies for a
mechanical system, which for concreteness could be a silicon wafer
with thin film coating. As described in Chapter \ref{cryoQ}, our Q measurements are
made at constant mode amplitude using a moderinger technique, and the
eigenfrequency can be inferred from zero crossing counting or a phase-locked
loop. We can measure the $Q$s and frequencies of $N$ eigenmodes at a fixed temperature, then vary
the system temperature to measure a new set of $Q$s and frequencies for those
eigenmodes. If we measure the $Q$s at $M$ different temperatures, our
measurements are a set of $\{Q_{ij}, f_{ij}\}$ for $i\leq N$ and $j\leq M$. One
could easily extend this problem. For example, one could search for
amplitude-dependent effects by performing the measurements at various mode
amplitudes, and simply include mode amplitude in the list of measurements. In
any case, let us collect the experimental measurements in a vector $\vec{y}_0$.
The measurement uncertainties are captured in a joint probability
distribution $P_\mathrm{cov}(\vec{y})$, which for the common case of Gaussian distributed
uncertainties can be well represented with the measured covariance matrix $\mathbf{C}_{\vec{y}}$.

We believe that some set of unknown physical parameters determine our measurements
$\vec{y}$. These include the Young's modulus and Poisson's ratio of the coating. Extrinsic sources of loss, for example from the
surrounding air and clamping point, may be relevant. And if the experiment is
well designed, the Qs are largely determined by the (generally anisotropic)
mechanical losses in the Si wafer and coating. Collect all of the poorly constrained physical
parameters in a vector $\vec{x}$, which has $Q$ elements. Suppose we also have a
probability distribution $P_\mathrm{prior}(\vec{x})$ which describes our prior knowledge of the
possible parameter values.

% TODO: Endnote about priors?

Finally, we describe below how to develop a physical model to predict mechanical
Q measurement outcomes. The model may contain material properties tightly constrained by other experiments,
such as the Young's modulus, Poisson's ratio, and coefficient of thermal
expansion of crystalline Si. Let us collect the well constrained parameters in a
vector $\vec{\mu}$, and keep in mind that if we decide later that we would like
to constrain or account for uncertainty of some member of $\vec{\mu}$ we could move it to $\vec{x}$. For now,
we simply define our physical model as a map

\begin{equation}
  \mathbf{A}_{\vec{\mu}}:\mathbb{R}^{MN} \to \mathbb{R}^Q.
\end{equation}

We want to solve for $\vec{x}$ (with some uncertainties) in the expression

\begin{equation}\label{eq:model}
  \vec{y}=\mathbf{A}_{\vec{\mu}}(\vec{x}).
\end{equation}

Because $\mathbf{A}_{\vec{\mu}}$ is some complicated nonlinear function, we cannot
easily invert to solve for $\vec{x}$. However, Eq \ref{eq:model} and Bayes' theorem
lets us write a convenient expression for the posterior probability distribution
of the model parameters based on our measurements:

\begin{equation}
  P_\mathrm{post}(\vec{x}|\vec{y}) = \frac{P(\mathbb{A}_{\vec{\mu}}(\vec{x})|\vec{x})P_\mathrm{prior}(\vec{x})}{P_\mathrm{cov}(\mathbb{A}_{\vec{\mu}}(\vec{x}))}.
\end{equation}

Since by construction all of our model uncertainty is captured in the uncertainty in
$\vec{x}$, $P(\mathbb{A}_{\vec{\mu}}(\vec{x})|\vec{x})=1$. We can therefore use
a MCMC algorithm such as Metropolis-Hastings to efficiently sample
$P_\mathrm{post}$ simply by drawing sample values of $\vec{x}$ from ratio of the
known distributions $P_\mathrm{prior}$ and $P_\mathrm{cov}$.

\section{Structural Mechanics Model}

We developed a finite element analysis (FEA) model in COMSOL to analyze
parameter-dependent mechanical properties of the wafer and coating.

\subsection{Meshing Analysis}

FEA models struggle to accurately represent systems with vastly different length
scales along different dimensions, such as a large, wide wafer with several 10s
cm diameter and 100s um thick whose mechanical properties depend strongly on a
thin film only 10s or 100s nm thick. The model must balance the requirement to capture physics with
a mesh that is finer than the smallest dimension against the computational cost
of a mesh that spans the largest dimension. One effective strategy is reducing the
model to a smaller subdomain that is related to the full domain by some symmetry
(for example, meshing only 1/4 of a pie slice of a system with azimuthal symmetry
reduces the number of elements by 4). One can also employ adaptive mesh
refinement to start with a coarse mesh that is inexpensive to compute, then
successively adding elements to create a finer mesh until the simulation results
converge to a single value. Adaptive mesh refinement ensures the mesh is only as
fine as necessary to achieve reliable results.

% TODO: dig up plot of adaptive mesh refinement

\subsection{Eigenfrequency Analysis}

Given the dimensions and material properties of a mechanical system, COMSOL's
structural mechanics module can perform an eigenfrequency analysis to compute
the mechanical eigenmodes and eigenfrequencies of the system.

% TODO: add eigenfrequency results

\subsection{Strain-energy density}

According to Eq \ref{eq:dilution}, the Q of a mechanical eigenmode depends on the sum of
mechanical losses in each subdomain of the system, weighted by the fractional
strain-energy density contained in that subdomain. COMSOL's eigenfrequency
analysis provides a strain-energy density field defined at each mesh element,
which we can integrate to obtain the fractional strain energy for each
subdomain.

% For simplicity,

% TODO: add strain energy density plots and eigenfrequency-dependent strain-energy density

\section{Loss Budget for a Si Wafer}

The parameter estimation problem relies on having an accurate model for the
frequency- and temperature-dependent mechanical losses present in the
experiment. This section describes the physical mechanisms behind various mechanical losses that may limit Q measurements for the
cryogenic Si Qs experiment described in \ref{cryoQ}.
 
\subsection{Thermoelastic}

For materials with finite thermal expansion, the strain profile of
mechanical eigenmodes creates a spatially varying temperature distribution along
which heat can flow. Eigenmode energy lost to such heat flow is called
thermoelastic loss.

One reason silicon is an important material for mechanical sensing applications,
including next generation gravitational wave interferometers, is that Si has
zero thermal expansion at 123 K due to phonon anharmonicity
\cite{Si_anharmonic}.


\begin{figure}[h!]
  \centering
  \includegraphics[width=.8\textwidth]{Figures/TE_vsT.pdf}
  \caption[Thermoelastic Loss vs Temperature]{Mechanical loss at 1 kHz for a 2'' silicon wafer due to
    thermoelastic damping, limited to the worst mechanical loss within $\Delta
    T$ K of the nominal system temperature. One can see that near 123 K where
    thermoelastic loss sharply decreases, temperature stability can limit the
    lowest achievable thermoelastic loss. However, $\pm 1$ K is not a terribly
    stringent requirement.}
  \label{fig:TE_vsT}
  \index{figures}
\end{figure}


\begin{figure}[h!]
  \centering
  \includegraphics[width=.8\textwidth]{Figures/TE_vsF.pdf}
  \caption[Thermoelastic Loss vs Frequency]{Frequency dependence of thermoelastic loss for a 2'' silicon wafer with mean temperature near
    123 K, at
    Si's coefficient of thermal expansion zero crossing. Thermoelastic loss is conservatively
    assumed to equal the maximum mechanical loss within $\Delta T$ of the
    nominal temperature.}
  \label{fig:TE_vsF}
  \index{figures}
\end{figure}

% TODO: add equation for thermoelastic loss

\subsection{Gas Damping}

Stochastic collisions between the sample and the surrounding gas molecules can
damp mechanical motion. There have been extensive studies on accurately modeling
gas damping, % TODO: add \cite{Ian_damping}
but the static model developed in 1966
\cite{static_gas_damp} has been widely applied to experiments with macroscopic
oscillators.

Gas damping is typically the negative tradeoff when considering buffer gas-based
cooling \cite{heatTransfer_cryo_lowP} \cite{heatTransfer_convection_P} for sample or test mass thermal control.

\begin{equation}
  \phi_\text{gas} =\left(\frac{2}{\pi}\right)^{2/3}\frac{1}{\rho h \nu_m}\sqrt{\frac{M_g}{RT}}P\approx\frac{P}{f\sqrt{T}}
\end{equation}


\begin{figure}[h!]
  \centering
  \includegraphics[width=.8\textwidth]{Figures/GasDamping.pdf}
  \caption[Gas Damping Loss]{Mechanical loss for a 2'' silicon resonator due to gas damping, shown
  at several ambient pressures and temperatures.}
  \label{fig:gasDamp}
  \index{figures}
\end{figure}



\subsection{Squeezed Film Damping}

When two solid surfaces are separated by a thin layer of fluid, the fluid
molecules can interact multiple times with each surface before fully
thermalizing with the ambient fluid. This leads to a geometry-dependent
enhancement of the ordinary gas damping effect given by \cite{squeeze_damping}


\begin{figure}[h!]
  \centering
  \includegraphics[width=.8\textwidth]{Figures/SqueezeFilmDamping.pdf}
  \caption[Squeeze Film Damping Loss]{Mechanical loss for a 2'' silicon resonator due to squeeze film
    damping, shown at several ambient pressures, temperatures, and overlapping
    surface area geometries.}
  \label{fig:squeezeDamp}
  \index{figures}
\end{figure}


\begin{equation} \phi_\text{squeezed} = \phi_\text{gas}\frac{L}{16\pi d}.  \end{equation}

\subsection{Slide Film Damping}

Another type of gas damping is slide film damping, which occurs between nearby
surfaces due to lateral motion (rather than motion that changes the distance
between the surfaces, as in squeezed film damping). We do not anticipate a
significant contribution from slide film damping in our mechanical Q
measurements, but the interested reader can learn more in \cite{Q_slideFilm}.

\subsection{Akhiezer Damping}

Eigenmodes of a mechanical system are standing waves solutions
of collections of phonons that carry the solid's stress energy. Due to the material's
stress-strain relationship, the phonon density of states varies as the eigenmode
evolves. When the equilibrium density of states varies relative to the
instantaneous phonon distribution, phonons participating in the excited mode
lose energy through phonon-phonon interactions as they rethermalize. When the
mode frequency is comparable to the characteristic rate of thermal diffusion,
phonon scatter leads to thermoelastic loss; when the mode frequency is much
smaller than the thermal phonon decay rate, the dissipation is called Akhiezer
damping; and the Landau-Rumer regime is relevant when the mode frequency is much
larger than the thermal phonon decay rate \cite{Dykman_short}.

Until recently, the mode- and direction-dependent picture was
approximated with an average Gruneisen parameter $\gamma_\mathrm{avg}$. Akhiezer
damping on average leads to an upper limit on Q given by

\begin{equation}
  Q\cdot f=\frac{\rho c^4}{2\pi\gamma_{\text{avg}}^2\kappa T\tau}
\end{equation}

where $\rho$ is the material density, $c$ is the speed of sound in the material,
$\tau$ is the thermal relaxation time,
and $\kappa$ is the thermal conductivity.

% TODO: cite source for average Akhiezer

Iyer and Candler \cite{full_Akhiezer} show how to compute the cross section for
leading order phonon-phonon interactions due to a mode- and
direction- dependent Gruneisen parameter $\gamma_\mathrm{i,j}$. A more detailed
derivation of Akhiezer and other loss
mechanisms relevant to nanomechanical systems are in \cite{Dykman_short}
\cite{Dykman_long}.

Cubic crystals like Si can have anharmonicity in $i$ of 39 phonon branches
due to strain in $j$ of 6 strain directions. In an orthonormal basis, there are
only two independent anharmonicities

\begin{equation}
  \vec{\gamma}_i\equiv
  \begin{pmatrix}
    \gamma_{i,1} \\ \gamma_{i,1} \\ \gamma_{i,1} \\
    \gamma_{i,5} \\ \gamma_{i,5} \\ \gamma_{i,5}
  \end{pmatrix}.
\end{equation}

The second order stress-strain relation for a cubic crystal can be
written with just 3 elasticity coefficients as

\begin{equation}
  \vec{\sigma} =
  \begin{pmatrix}
    c_{11} & c_{12} & c_{12} & 0 & 0 & 0 \\
    c_{12} & c_{11} & c_{12} & 0 & 0 & 0 \\
    c_{12} & c_{12} & c_{11} & 0 & 0 & 0 \\
    0 & 0 & 0 & c_{44} & 0 & 0 \\
    0 & 0 & 0 & 0 & c_{44} & 0 \\
    0 & 0 & 0 & 0 & 0 & c_{44}
  \end{pmatrix}\vec{\epsilon} \equiv C\vec{\epsilon}.
\end{equation}

For a unit volume of the crystal experiencing stress tensor $\vec{\epsilon}$, the
effective, cycle-averaged elastic storage modulus is

\begin{equation}
  E_\mathrm{eff}(\vec{\epsilon}) = \frac{\vec{\epsilon}^\top \vec{\epsilon}}{\vec{\epsilon}^\top C^{-1}\vec{\epsilon}}.
\end{equation}

The full mode- and direction-
dependent mechanical loss is expressed in \cite{full_Akhiezer} as:

\begin{equation}
  Q=\frac{E_\mathrm{eff}}{\Gamma_a^2 C_v T}
  \frac{1 + \Omega^2 \tau^2}{\Omega \tau}.
\end{equation}

The strain-energy loss depends on the degree to which the applied strain
perturbs the eigenfrequency of each phonon branch \cite{Dykman_long}

\begin{equation}
  \delta \omega_i = -\omega_{i,0} \vec{\gamma}_i\vec{\epsilon}.
\end{equation}

This leads to the anharmonic Gruneisen parameter $\Gamma_a$ depending on the variance of the
strain energy-weighted $\vec{\gamma}_i$.

%%\begin{equation}
%  \Gamma_a^2 \equiv \frac{1}{C^2\vec{\epsilon}^2 (C^{-1})^2}
%  (\langle (\vec{\gamma}_i \vec{\epsilon})^2 \rangle -
%  \langle \vec{\gamma}_i\vec{\epsilon} \rangle^2)
%\end{equation}


%For underdamped oscillators near resonance, the quality factor is related to the
%damping factor by $Q\approx 1/\eta$. And, from the form of $E_\mathrm{eff}$ and
%$\Gamma_a^2$, we can see that $\eta$ depends only on the orientation of the
%strain, not on its magnitude. Therefore, 

%The expressions for $\Gamma_a$ for pure square extensional modes are given in
%\cite{full_Akhiezer} by

%\begin{equation}
%  \begin{split}
%    \Gamma_{a, \mathrm{bulk}}^2 &= \alpha^2(\langle \gamma_{i,1}^2 \rangle - \gamma_0^2),
%    \alpha_\mathrm{bulk}^2 &= \frac{2(c_{11} - c_{12})^2}{c_{11}^2 + 2c_{12}^2}
%  \end{split}
%\end{equation}

%and for pure shear modes as

%\begin{equation}
%  \begin{split}
%    \Gamma_{a, \mathrm{shear}}^2 = \alpha^2\langle \gamma_{i,5}^2 \rangle,
%    \alpha=1
%  \end{split}
%\end{equation}

Mason shows how to calculate Gruneisen parameters $\gamma_{i,1}$ and
$\gamma_{i,5}$ for Si using measured third-order strain moduli in
\cite{Mason_gruneisen}. He shows that $\gamma_{i,1}$ varies between $-0.83$ and
$1.43$ with an average value of $0.501$, while $\gamma_{i,5}$ varies between
$-0.92$ and $0.92$ with an average value of $0$ (shear modes are isovolumetric).
There are only 21 unique values of $\gamma_{i,1}$ and $\gamma_{i,5}$.

Akhiezer damping has been observed in Si \cite{Q_AkhiezerMeasurement}. However,
Akhiezer damping has not yet been definitively demonstrated near acoustic
frequencies ($\approx$ kHz) in Si resonators near 123 K.

It does not appear that anything inherently prevents an appropriate choice of mode shape from sending
$Q_\mathrm{phonon-phonon}\to\infty$, such that mechanical loss would be set by
fourth order phonon interactions. Of course, the condition would need to hold
across the entire volume of the oscillator.

\subsection{Total Loss Budget}

The mechanical loss budget in Fig. \ref{fig:SiLossBudget} captures the total loss due to dissipation
for a 1 kHz eigenmode of a 2'' Si wafer held at 123 K. To fully describe
experimental mechanical losses, we would also need to include clamping losses,
which depend on the curvature of the mechanical eigenmode near the suspension
point \cite{Q_lossBudget}. 

\begin{figure}[h!]
  \centering
  \includegraphics[width=\textwidth]{Figures/BareSi_lossBudget.pdf}
  \caption[Mechanical Loss Budget]{Mechanical loss budget for a bare Si wafer with 2'' diameter, held at $123\pm 1$
  K and mechanically isolated from the environment with a gentle nodal
  suspension.}
  \label{fig:SiLossBudget}
  \index{figures}
\end{figure}

\section{Demonstration with Simulated Data}

For technical experimental reasons, we do not have sufficient data to perform
the tomography analysis in a temperature-dependent Si system as envisioned.
Moreover, our COMSOL model is computationally expensive and the limiting step of
the MCMC analysis. Other groups have demonstrated with cryogenic Si Q
measurements and fast FEA that neither challenge is a fundamental barrier to performing loss
tomography on temperature dependent Si-and-film Q measurements. We hope this
demonstration encourages others to perform similar analyses, and proves useful
for characterizing material properties of Si and thin films.

\subsection{Generating data}

We will analyze a system with some assumed ``true'' properties, generate some
noisy data based on those nominal properties, and show that we can infer certain
material properties when the data follow the model.

We assume the system and material properties in Table \ref{table:tomo}.
Uncertainties are for the assumed prior distribution, and are the standard deviation of a Gaussian
centered on the nominal value and uncorrelated with the other parameters.
Parameters included in the parameter estimation problem were randomly assigned
``true'' values drawn from a uniform distribution within $10\%$ of one standard
deviation of their nominal values. Estimated measurement temperatures were
constrained with a uniform prior between $100 K$ and $200 K$. The only parameter
not explicitly assigned a prior distribution was
the coating loss angle $\phi_\mathrm{coat}$, which
instead had an unbounded log-uniform prior. Material properties were chosen to be similar
to a (100) single crystal Si wafer with an amorphous Si coating.

\begin{table}[h!]
  \centering
  \begin{tabular}{||p{2cm} c c p{2cm} c||}
    \hline
    Symbol & Description & Nominal Value & True Value & Reference \\ [0.5ex]
    \hline \hline
    $R_\mathrm{s,0}$ & Si Radius at $T_\mathrm{ref}$ & 75 mm & -- & -- \\
    $h_\mathrm{s,0}$ & Substrate Thickness at $T_\mathrm{ref}$ & $500\pm 50$ $\mu$m & 502.5 um & --\\
    $Y_s(T)$ & Si Young's modulus & $Y_s(T_\mathrm{ref})=166$ GPa & -- & \cite{Si_Youngs} \cite{temp_Si_cantilevers_Y} \\
    $\nu_s$ & Si Poisson's ratio & 0.167 & -- & -- \\
    $\rho_s$ & Si density at $T_\mathrm{ref}$ & 22220 kg/$m^3$ & -- & \cite{cryo_White} \\
    $t_\mathrm{coat}$ & Coating Thickness & $300\pm 75$ nm & 299.0 nm & -- \\
    $Y_\mathrm{coat}$ & Coating Young's Modulus & $80\pm 20$ GPa & 81.3 GPa & -- \\
    $\nu_\mathrm{coat}$ & Coating Poisson's ratio & $0.22\pm 0.055$ & 0.228 & -- \\
    $\rho_\mathrm{coat}$ & Coating density & 2175 kg/$m^3$ & -- & -- \\
    $\gamma_{0,s}$ & Si Gruneissen parameter & $0.4\pm 0.2$ & 0.395 & -- \\
    $\phi_\mathrm{coat}$ & Coating loss angle & -- & $10^{-4}$ & -- \\
    $T_1$, $T_2$, $T_3$, $T_4$, $T_5$, $T_6$ & Measurement temperatures
                         & -- & 110, 120, 123, 123.9, 125, 130, 150 K & -- \\ [2ex]
    \hline
  \end{tabular}
  \caption[Parameters for Loss Tomography Demonstration]{Parameters for loss
    tomography demonstration. For parameters inferred in the analysis presented
    in Fig. \ref{fig:tomo_corner}, ``Nominal Value'' refers to the mean of the
    Gaussian-distributed prior distributions, while ``True Value'' refers to the
    underlying parameter value used to generate simulated experimental data.
  }
  \label{table:tomo}
\end{table}

For the true parameter values and each set of values proposed by the MCMC, we use an analytical model \cite{Gabriele_Tnote} and code \cite{Gabriele_git}
developed by Vajente to compute to compute the eigenfrequencies of a coated and
uncoated disk. We use change in eigenfrequency and total mass with and without coating to
estimate the coating dilution factor, as proposed in \cite{LMA_dilution} and
proven with some conditions in
\cite{Gabriele_Tnote}.

\begin{equation}
  D_i = 1 - (\frac{f_i^{(\mathrm{uncoated})}}{f_i^{(\mathrm{coated})}})^2
  \frac{m^{(\mathrm{uncoated})}}{m^{(\mathrm{coated})}}
\end{equation}

We then compute each eigenmode's quality factor as the sum of thermoelastic and
Akhiezer contributions in the Si substrate, plus a frequency and temperature
independent contribution from $\phi_\mathrm{coat}$ in the coating. For a given
set of proposed parameter values, we limit ourselves to the first 34
eigenfrequencies below 16 kHz at each of the 7 measurement temperatures. 

For the ``true'' parameter values in Table \ref{table:tomo}, we generate some
experimental data by adding noise to the fiducial eigenfrequencies and quality
factors. We add a uniformly random offset drawn from $[-1,1]$ Hz to each eigenfrequency,
and multiply each quality factor by a uniformly random factor drawn from [0.99,
1.01].

Finally, we define a likelihood function proportional to the joint probability
density for Gaussian uncorrelated errors centered on each of the experimental
data points. For eigenfrequencies, we assume a 1 Hz standard deviation, and for
quality factors we assume a 1\% standard deviation. The objective function for
the MCMC is the sum of the log likelihood and log prior.


%TODO: cite Glasgow, Syracuse, other cryo Si Q groups; see if Gabriele or Chris
%recommend anyone else I miss. Cite numerical relativity
%FEA folks, Geoffrey, etc.

%I am grateful to Anna Roche for working on this analysis during her SURF
%project, and to Gabriele Vajente for permitting us to analyze his Q
%measurements.

\subsection{Results}

We performed the MCMC sampling using the python emcee module.

With 10,000 iterations of an ensemble of 512 samplers, the autocorrelation time
of the ensemble was only about 10x less than the number of iterations, and not
enough to ensure unbiased sampling of the true probability distribution.
Nonetheless, the results in Fig. \ref{fig:tomo_corner} suggest the MCMC would be able
to constrain $\phi_\mathrm{coating}$ given enough iterations.

Several interesting features suggest modifications to improve the analysis. The
bimodal distribution of $T_4$, the temperature closest to the zero crossing of
Si's thermal expansion coefficient, suggests thermoelastic loss and not
eigenfrequency is constraining the temperature estimates. It would be
interesting to perform this analysis to higher eigenfrequencies that would be
more sensitive to temperature changes. We could also include a measurement at a
known temperature (like room temperature) as a calibration point.

Due to the expensive computation of many eigenfrequencies, the likelihood took
seconds to compute. It would be preferable to first perform the expensive
computation over a grid or sampling around the expected parameter values, then perform a
polynomial fit to create a fast, if approximate, objective function. To improve
model fidelity, one could instead compute eigenfrequencies and dilution factors
with a numerical finite element analysis and then fit a polynomial model. Unless
a finite element model could be developed to run in 10s or 100s ms, it is
unlikely to be useful for directly computing eigenfrequencies during the MCMC.
One could also search for a sampling algorithm better suited for our problem,
which may more efficiently sample the parameter space with shorter
autocorrelation.

\begin{figure}[h!]
  \centering
  \includegraphics[width=.9\textwidth]{Figures/tomo_corner.pdf}
  \caption[Tomography MCMC Corner Plots]{Corner plots showing the joint
    posterior probability distributions of the model parameters.}
  \label{fig:tomo_corner}
  \index{figures}
\end{figure}


%\begin{figure}

%  \printbibliography[heading=subbibliography]

%\end{refsection}