#!/usr/bin/env python
# coding: utf-8

# In[699]:


import numpy as np
#import control as ct
import matplotlib.pyplot as plt
import scipy.signal as sig
import scipy.optimize as opt
import scipy.constants as scc
import pickle as pkl

# In[700]:


import pyswarms as ps
import multiprocessing as mp
from pyswarms.backend.operators import compute_pbest, compute_objective_function
from multiprocessing import Pool


# In[701]:


#from control.matlab import *
#import control.matlab as ctlml


# In[702]:


ff = np.logspace(0, 6, 100)


# In[929]:


c = scc.c
Ls = 4e3
L_src = 40
Li = .5 #20
Lf = 20
w0 = c/2000e-9 * 2 * np.pi
tau0 = np.sqrt(1e-4)
rho0 = np.sqrt(1-tau0**2)
m0 = 1e-2
L0=.37
tauCM = np.sqrt(2e-3)
rhoCM = np.sqrt(1-tauCM**2)
tauIM = np.sqrt(4.6e-2)
rhoIM = np.sqrt(1-tauIM**2)
z = np.zeros([2,2])
Is = 3e6
Ms = 200
hbar = scc.hbar #6.626e-34 / 2 / np.pi
phi_s = np.pi/2
I1 = 1e3
I2 = 1e3
theta1 = 0
theta2 = 0
phi_i = 0
phi_f = np.pi/2

best_cost = None

# In[952]:


x0 = [theta1, theta2, np.log10(I1), np.log10(I2),
      phi_i, phi_f, np.log10(m0), np.log10(tau0**2),
      np.log10(L0), np.log10(Li)]#, np.log10(Lf),
      #np.log10(tauIM**2)]
bounds = [
    (-np.pi, np.pi), # min, max for theta1
    (-np.pi, np.pi), # min, max for theta2
    (-3, 5), # min, max for I1
    (-3, 5), # min, max for I2
    (-np.pi, np.pi), # min, max for phi_i
    (-np.pi, np.pi),  # min, max for phi_f
    (-3, 2), # min, max for cantilever mass m
    (-4, -1), # min, max for ring cavity MC1 power transmissivity
    (-1, 2), # min, max for ring cavity round trip length
    (-2,  0) # min, max for fraction of SRC length for input cavity
    #(-1, 2), # min, max for filter cavity length
    #(-4, -1) # min, max for SRM power transmissivity
]
params = ['theta1', 'theta2', 'I1', 'I2', 'phi_i', 'phi_f', 'm', 'tau', 'L', 'Li', 'Lf', 'tauIM']
dim = len(bounds)
swarm = False
# Create bounds in pyswarms style
if swarm:
    mins = np.zeros(len(bounds))
    maxs = np.ones(len(bounds))
    for i in np.arange(len(bounds)):
        mins[i] = bounds[i][0]
        maxs[i] = bounds[i][1]
    bounds = (mins, maxs)


# In[934]:


def Pi(Omega, rho=rho0, m=m0):
    num = 2 * rho * w0
    den = m * Omega**2 * c**2
    return num / den


# In[935]:


def Z(Omega, L, phi):
    t = L / c
    #num, den = ct.pade(t, 2)
    #Z = ct.tf(
    #    [[num, [0]], [[0], num]],
    #    [[den, [1]], [[1], den]]
    #)
    
    return np.e**(1j * (Omega * t + phi))


# In[936]:


def eta(Omega, tau=tau0, L=L0):
    gamma = c * tau**2 / 2 / L
    return np.arctan(Omega / gamma)


# In[937]:


def M(theta1, theta2, I1, I2, i, j):
    a = ((np.sqrt(I1) * np.cos(theta1)*(i==1) + np.sqrt(I2) * np.cos(theta2)*(i==2)) * 
         (np.sqrt(I1) * np.sin(theta1)*(j==1) + np.sqrt(I2) * np.sin(theta2)*(j==2)))
    b = ((np.sqrt(I1) * np.sin(theta1)*(i==1) + np.sqrt(I2) * np.sin(theta2)*(i==2)) * 
         (np.sqrt(I1) * np.sin(theta1)*(j==1) + np.sqrt(I2) * np.sin(theta2)*(j==2)))
    c = ((np.sqrt(I1) * np.cos(theta1)*(i==1) + np.sqrt(I2) * np.cos(theta2)*(i==2)) * 
         (np.sqrt(I1) * np.cos(theta1)*(j==1) + np.sqrt(I2) * np.cos(theta2)*(j==2)))
    d = ((np.sqrt(I1) * np.sin(theta1)*(i==1) + np.sqrt(I2) * np.sin(theta2)*(i==2)) * 
         (np.sqrt(I1) * np.cos(theta1)*(j==1) + np.sqrt(I2) * np.cos(theta2)*(j==2)))
    return 2 * np.array([[a, b], [c, d]])


# In[938]:


def Kappa(Omega, L, theta1, theta2, I1, I2, i, j, tau=tau0, rho=rho0):
    MM = M(theta1, theta2, I1, I2, i, j)
    factor = tau**2 / (1 - 2 * rho * np.cos(Omega * L / c) + rho**2)
    return factor * MM


# In[939]:


def Y(Omega, theta1, theta2, I1, I2, i, j, tau=tau0, rho=rho0, L=L0, m=m0):
    ee = eta(Omega, tau, L)
    PP = Pi(Omega, rho, m)
    KK = PP * Kappa(Omega, L, theta1, theta2, I1, I2, i, j, tau, rho)
    if i == j:
        KK = np.identity(2) + KK
    #KK = np.block(
    #    [[np.identity(2) + Kappa(Omega, L, theta1, theta2, I1, I2, 1, 1, tau, rho), 
    #      Kappa(Omega, L, theta1, theta2, I1, I2, 2, 1, tau, rho)],
    #     [Kappa(Omega, L, theta1, theta2, I1, I2, 1, 2, tau, rho),
    #      np.identity(2) + Kappa(Omega, L, theta1, theta2, I1, I2, 2, 2, tau, rho)]]
    #)
    return -np.e**(1j * 2 * ee) * KK


# In[940]:


def A(Omega, theta1, theta2, I1, I2, phi_i, phi_f,
    Ls=Ls, Li=Li, Lf=Lf, L=L0, m=m0, phi_s=phi_s,
    tau=tau0, rho=rho0, tauIM=tauIM, rhoIM=rhoIM, tauCM=tauCM, rhoCM=rhoCM):
    ttIM = tauIM * np.identity(2)
    rrIM = rhoIM * np.identity(2)
    ttCM = tauCM * np.identity(2)
    rrCM = rhoCM * np.identity(2)
    Zi = Z(Omega, Li, phi_i) * np.identity(2)
    Zs = Z(Omega, Ls, phi_s) * np.identity(2)
    Zf = Z(Omega, Lf, phi_f) * np.identity(2)
    Y11 = Y(Omega, theta1, theta2, I1, I2, 1, 1, tau, rho, L, m)
    Y21 = Y(Omega, theta1, theta2, I1, I2, 2, 1, tau, rho, L, m)
    Y12 = Y(Omega, theta1, theta2, I1, I2, 1, 2, tau, rho, L, m)
    Y22 = Y(Omega, theta1, theta2, I1, I2, 2, 2, tau, rho, L, m)

    Ks = np.identity(2)
    Ks[1,0] -= 8 * Is * w0 / Ms / scc.c**2 / Omega**2

    out =  np.block([
        [z,    z,  z,   z,  z,    z,  z,  z,  z,     z,  z,   z,  z,     z],
        [ttIM, z,  z,   z,  z,    z,  z,  z,  z,     z,  z,   z,  -rrIM, z],
        [z,    Zi, z,   z,  z,    z,  z,  z,  z,     z,  z,   z,  z,     z],
        [z,    z,  Y22, z,  z,    z,  z,  z,  z,     z,  Y12, z,  z,     z],
        [z,    z,  z,   Zf, z,    z,  z,  z,  z,     z,  z,   z,  z,     z],
        [z,    z,  z,   z,  ttCM, z,  z,  z,  -rrCM, z,  z,   z,  z,     z],
        [z,    z,  z,   z,  z,    Zs, z,  z,  z,     z,  z,   z,  z,     z],
        [z,    z,  z,   z,  z,    z,  Ks, z,  z,     z,  z,   z,  z,     z],
        [z,    z,  z,   z,  z,    z,  z,  Zs, z,     z,  z,   z,  z,     z],
        [z,    z,  z,   z,  rrCM, z,  z,  z,  ttCM,  z,  z,   z,  z,     z],
        [z,    z,  z,   z,  z,    z,  z,  z,  z,     Zf, z,   z,  z,     z],
        [z,    z,  Y21, z,  z,    z,  z,  z,  z,     z,  Y11, z,  z,     z],
        [z,    z,  z,   z,  z,    z,  z,  z,  z,     z,  z,   Zi, z,     z],
        [rrIM, z,  z,   z,  z,    z,  z,  z,  z,     z,  z,   z,  ttIM,  z]
    ])
    return out


# In[941]:


def CLTF(A):
    return np.linalg.inv(np.identity(28) - A)


# In[942]:


def qNoise(
    IO
):
    # quantum noise at IFO output "signal" quadrature
    return np.sqrt(np.abs(IO[27,1])**2 + np.abs(IO[27,2])**2)


# In[943]:


def aNoise(
    Omega, IO, I1, I2, tau=tau0, phi0=1e-5, Omega0=1, m=m0
):
    # amplifier noise at IFO output "signal" quadrature
    # scale expression from psoma paper
    scale = np.sqrt(4 * scc.k * 123 / Omega / m * Omega0**2 * phi0 / 
                    (Omega0**4 * phi0**2 + (Omega0**2 - Omega**2)**2)
                   )
    scale *= np.sqrt(32 * w0 / hbar / c**2) / tau0**2 * 0 # NOTE: AMPLIFIER NOISE OFF
    return np.abs(scale * (np.sqrt(I2) * IO[27,7] + np.sqrt(I1) * IO[27,23]))


# In[944]:


def SNR(Omega, theta1, theta2, I1, I2, phi_i, phi_f,
    Ls=Ls, Li=Li, Lf=Lf, L=L0, m=m0, phi_s=phi_s,
    tau=tau0, rho=rho0, tauIM=tauIM, rhoIM=rhoIM, tauCM=tauCM, rhoCM=rhoCM, 
    Is=Is, Ms=Ms, IRnoise=False):
    A0 = A(
        Omega, theta1, theta2, I1, I2, phi_i, phi_f,
        Ls, Li, Lf, L, m, phi_s, tau, rho, tauIM, rhoIM, tauCM, rhoCM
    )
    IO = CLTF(A0)
    quantNoise = qNoise(IO)**2 # quantum noise
    ampNoise = aNoise(Omega, IO, I1, I2, tau=tau, m=m)**2 # amplifier displacement noise
    sig = (np.abs(IO[27,13]))**2
    if IRnoise:
        # see Haixing thesis 2.29
        kappa_s = 8 * Is * w0 / Ms / scc.c**2 / Omega**2
        hSQL = np.sqrt(8 * scc.hbar / Ms / Omega**2 / Ls**2)
        h_units = np.sqrt(2 * kappa_s) / hSQL
        totNoise = np.sqrt(quantNoise + ampNoise)
        return totNoise / np.sqrt(sig) / h_units
    return np.sqrt(sig / (quantNoise + ampNoise))

SNR_vec = np.vectorize(SNR)


# In[945]:


def cost(*args, #theta1, theta2, I1, I2, phi_i, phi_f,
         Omegas=np.logspace(1, 4, 32), tau=tau0, p=params,
         tauIM=tauIM, rhoIM=rhoIM, Lf=Lf, Li=Li, L=L0, rho=rho0, m=m0,
         I1=I1, I2=I2, theta1=theta1, theta2=theta2, phi_i=phi_i, phi_f=phi_f,
         IRnoise=False
        ):
    for i in np.arange(len(args[0])):
        #print(args)
        #print(params)
        p = params[i]
        if p == 'theta1':
            theta1 = args[0][i]
        elif p == 'theta2':
            theta2 = args[0][i]
        elif p == 'I1':
            I1 = 10**(args[0][i])
        elif p == 'I2':
            I2 = 10**(args[0][i])
        elif p == 'phi_i':
            phi_i = args[0][i]
        elif p == 'phi_f':
            phi_f = args[0][i]
        elif p == 'm':
            m = 10**(args[0][i])
        elif p == 'tau':
            tau = np.sqrt(10**(args[0][i]))
            rho = np.sqrt(1 - tau**2)
        elif p == 'L':
            L = 10**(args[0][i])
        elif p == 'Li':
            Li = 10**(args[0][i]) * L_src
            Lf = L_src - Li
        elif p == 'Lf':
            Lf = 10**(args[0][i])
        elif p == 'tauIM':
            tauIM = np.sqrt(10**(args[0][i]))
            rhoIM = np.sqrt(1 - tauIM**2)

    SNRs = SNR_vec(Omegas, theta1=theta1, theta2=theta2, I1=I1, I2=I2,
                   phi_i=phi_i, phi_f=phi_f, L=L, Li=Li, Lf=Lf,
                   tauIM=tauIM, rhoIM=rhoIM,
                   m=m, tau=tau, rho=rho, IRnoise=IRnoise)
    if IRnoise:
        return SNRs
    costs = -np.log10(SNRs)

    tot_cost = np.sum(costs)
    global best_cost
    if best_cost == None:
        best_cost = tot_cost
    if tot_cost < best_cost:
        best_cost = tot_cost
        print("new best cost: ", tot_cost)
        print("at: ", args)


    return np.sum(costs)

if __name__ == '__main__':

    #optimum = opt.basinhopping(cost, x0, minimizer_kwargs={'bounds':bounds})

    optimum = opt.shgo(
        cost, bounds, options={'disp':True}, iters=4
    )
    
    f_opt = open('optimum.pkl', 'wb')
    pkl.dump(optimum, f_opt)


    print("optimum is ")
    print(optimum)


