\begin{acknowledgements}

  In light of the ongoing automation of information processing, I am all the
  more grateful to those who taught me how to temporarily care for our
  collective knowledge, and perhaps contribute to our ability to reason and
  create.

  I thank my family for nurturing my curiosity. Thanks to my mom, Tracy
  Rhinehart; my dad, Randy Markowitz; my sister, Leah Markowitz; my stepparents
  Jill Markowitz and Bill Valentino; my aunts and
  uncles Scott Bruder, Lisa Bruder, Sheryl Markowitz, Bob Labes, Cindy
  Lanterman, Frank Lanterman, Ron Laino, Rich Rhinehart, Stephanie Rhinehart; my
  grandparents, Audrey Rhinehart, Richard Rhinehart, Martin Markowitz, and
  Norma Markowitz; and my
  cousins Jordan Cohen, Arielle Cohen, Hannah Leflein, Emma Bruder, Seth Laino,
  Eva Rothermel, Jared Laino, Anna Duda, Sarah Rhinehart, and Emma Rhinehart.
  
  I am grateful to Prof. Horst von Recum for letting me do chemistry in his lab, where I learned the value of performing the right experiment. I also thank the many grad students and
  postdocs who took a teenager under their wings during those summers, including
  Steven Vesole, Jeffrey Halperin, Julius Korley, and Edgardo
  River-Delgado.

  Prof. John Kovac introduced me to physics experiments, including some techniques like
  lock-in amplification that would shape my aesthetic preferences. In the CMB
  lab, Kirit Karkare, Chin Lin Wong, and Robert Kimberk taught me how to enjoy
  making measurements.

  Prof. Vasant Natarajan taught me how light can be simultaneously a sturdy tool
  and a delicate timepiece. I fondly recall conversations spanning physics and
  philosophy with him and Apurba Paul, Dipankar Kaundilya, Lal Muanzuala, Pushpander
  Kumar Singh, and Ketan Rathod, among others.

  Prof. John Doyle's encyclopedic knowledge of the dirty details behind
  experiments, from gaskets to transistors, inspired me to build my own
  apparatuses especially when I want to understand them well. Thanks especially to
  Elizabeth Petrik West for mentoring me during my research.

  I suspect my figures do not meet Prof. Wesley Smith's standards, but the
  reader can share my gratitude to him for much of any clarity they offer. Thanks
  to Isobel Ojalvo, Laura Dodd, and Tyler Ruggles for teaching me some particle
  physics and how to use a
  computer.

  Prof. Gary Feldman showed me how to make statistically rigorous statements,
  perhaps more than anything the art that defines science. Thanks to Gareth Kafka for connecting the decisions just before grad school to those at the end.

  For the last several years, I had the privilege of Prof. Rana Adhikari
  trying to teach me everything: the importance of Q and control systems and
  noise budgets; how to
  work fast and when to work slow; who to trust with your experiment; that everything
  is quantum; how to lock the cavity. Thank you for your insight, humor, and guidance. 
  
  I cannot say enough about the extraordinary scientists I've collaborated with
  during grad school. Thanks to Brittany Kamai for friendship, mentoring on mentorship, and many writing sessions
  in coffee shops and in nature; to Chris Wipf for sounding nearly every
  project I undertook and reviving my much abused cymac; to Koji Arai for
  telling me how he aligns the OMC, among other lessons; to Gabriele Vajente for helping me adapt his experiment and analyses to
  a cryostat, and for his careful measurements; to Eric Gustafson, who
  taught me how a laser works; to Johannes Eiccholz for thorough problem
  solving in the lab, especially Wednesday evenings; to Andrew Wade for
  learning Finesse with me; to Yuntao Bai for reminding us about
  Mach-Zehnder interferometers; to Aidan Brooks for many a coffee, donut, and
  chat; to Jamie Rollins for Debian tips; to Mayank Chaturdevi for vibing in the lab with me
  perfectly, and at a critical moment; to Steve Vass for the regular snow
  reports; to Jancarlo Sanchez for keeping the table floating; and to Francisco (Paco) Salces-Carcoba
  for entertaining the fringe ideas. 

  So much of the functioning of a lab survives by transfer between peers. Thanks
  to Sarah Gossan for inviting me to her third space during my first year; to Zach Korth for
  showing me how to use the moderinger, probably the first control loop I
  tried to understand as such; to Evan Hall for answering the questions
  that taught me how LIGO works, and for careful estimates; to Kevin Kuns for answering the
  questions that taught me how optomechanics works, and for endless open source
  support; to Gautam Venugopalan for showing me around the 40m; to Craig
  Cahillane for his excellent Fabry-Perot cavity widget on github; to Ching Pin
  for hacking together several of our best cryo Q measurements; to Shruti Maliakal, who
  shared in designing and building a beautiful experiment; to Anchal
  Gupta for setting our deadline and being a true friend; to Ian MacMillan for
  letting me distract him from the opposite cubicle; to Radhika Bhatt for
  sharing the drive to APS and asking good questions; and to Xiaoyue Ni for showing me the KNI and helping me take some incredible
  photos.
  
  % TODO Eric Quintero, Jon Richardson,  Hang Yu.

  I am grateful to Prof. Yanbei Chen for his cheerful disposition and serious consideration of even my
  naive ideas. My interest in ponderomotive interferometry was sparked by a
  workshop attended by Yanbei, Tom Corbitt, Nancy Aggarwal, Haixing Miao,
  Belinda Pang, and Denis Martynov, and I am grateful for their invaluable
  lessons during that short week. I also am grateful to James Gardner for giving
  me a Hamiltonian and for developing our practice of Social Interactions; to Su
  Direcki for illuminating conversation on entanglement; to Xiang Li for
  helping me sketch useful features of interferometers; and Bassam Helou for
  many illuminating conversations and holding the rope.

  Thanks to Adrienne Meier for countless conversations. Thanks to Larry Wallace,
  Mike Pedraza, and Michael Park for years of tech support and, along with Di-No
  Repairs, for setting up a
  loaner laptop when mine died a month before my defense.

  I'm grateful to have lived in community with many roommates and close friends over the years,
  many of whom were like siblings. Thanks to Brianna Beswick, Li Kewei, Kyle Franseen, Joey
  Slipka, Ryan Chow, Steve Barroquiero, Nini Ren, Andrew Sun, Grace Kim, Samantha Heinle, Sarah Sohn, Cyndia Yu, Ross Rheingans-Yoo, Lucian Wang, Jude
  Russo, Mark Arildsen, Karina
  Perez, Josh Douglas, Vinicius
  Ferreira, Giuliana Viglione, Sho Harvey, and all of E41.

  Finally, I must thank my comrades. Together, we advanced the organizational
  capacity of Pasadena's working class by enacting, through direct democracy, a
  state-leading municipal charter amendment including rent control, just cause eviction requirements, tenants'
  right to organize, and a permanent tenant-majority administrative board. We
  also made a home. Thanks to Arian Jadbabaie, Brigitte Rooney, Nathan Sagman, Ashay Patel, Jane Panangaden, Jae Fromm,
  Dustin Lagoy, Charles Xu, Shima Taj Bakhsh, Tarquin, John Brown, Sandwich, Artemis,
  Pinky, Mordechai, and Sassafras. Thanks especially to my incredibly patient
  partner Bobbi Ennis.

\end{acknowledgements}

\begin{abstract}

  Optomechanical sensors provide our most sensitive measurements of spacetime,
  including observations of gravitational waves by laser interferometric
  detectors. However, even state of the art detectors like the Advanced Laser
  Interferometric Gravitational-Wave Observatory (LIGO) are
  still tens of orders of magnitude away from the measurement limits
  imposed by Heisenberg uncertainty. This thesis maps out the contours of mechanical
  and optical losses limiting next generation graviational wave interferometers,
  and describes several experiments and analyses to improve those limitations.
  We review the theory of optomechanical force sensing to understand the
  influence of
  optical radiation pressure on the dynamics of mechanical oscillators.
  We analyze several modified Mach-Zehnder interferometers and show how radiation
  pressure can be a resource for quantum measurement, including by
  establishing a surprising optical spring effect in a cavity held
  on-resonance. The most developed proposal is for a phase-sensitive
  optomechanical amplifier to avoid the photodetection losses that may limit next-generation
  gravitational wave interferometers utilizing cryogenic silicon mirrors and
  $\approx 2000$ nm infrared lasers. The amplifier calls for high quality
  mechanical oscillators made of single crystal silicon, which we fabricate. We
  describe our efforts to develop a testbed for cryogenic mechanical loss
  measurements of silicon oscillators and thin film coatings. And, we show
  how Bayesian inference can be used to improve our understanding of the
  physical mechanisms limiting a system's mechanical loss. Finally, we describe
  the optical, mechanical, and electronic design of a prototype phase sensitive optomechanical
  amplifier. The prototype is useful for testing the control system required
  to implement the full amplifier, and we characterize the current control
  scheme and the scheme for near-term upgrades. Our latest measurements show a clear path to
  steadily improving the amplifier's noise figure with well understood
  technology.
  
  Updates to this work will be publicly available at LIGO document number
  \href{https://dcc.ligo.org/P2300422}{P2300422} and at
  \href{https://git.ligo.org/aaron.markowitz/writing}{https://git.ligo.org/aaron.markowitz/writing}
  along with source code and more.
	%\href{https://dcc.ligo.org/P#######}{P#######}.

\end{abstract}

\begin{publishedcontent}[iknowwhattodo]
  \nocite{PSOMA,Si_emissivity,Voyager,Q_blackNanotube}
%  \printbibliography[keyword=ownpubs,heading=publishedcontent]
\end{publishedcontent}

\tableofcontents
\listoffigures

\listoftables

\printnomenclature

%%% Local Variables:
%%% mode: latex
%%% Tex-master: "../main"
%%% End:
